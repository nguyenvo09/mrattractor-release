package no_memory_jaccard;

import java.io.BufferedReader;
import java.io.File;
import java.io.FileNotFoundException;
import java.io.FileReader;
import java.io.IOException;
import java.io.PrintWriter;
import java.util.ArrayList;
import java.util.Collections;
import java.util.LinkedList;
import java.util.Queue;
import java.util.Set;
import java.util.HashSet;
import java.util.HashMap;
import java.util.Map.Entry;

import nomemory.EdgeValue_v2;
import nomemory.Graph;
import nomemory.Helper;
import nomemory.Settings;
import nomemory.VertexValue_v2;
import junit.framework.Assert;

import com.google.common.collect.Sets;

/**
 * First component: computing Jaccard Distance in Single Machine. 
 * The output of this component will be used for Dynamic Interactions on MapReduce Hadoop.
 * @author ben
 *
 */
public class JaccardInit {

	public Graph m_cGraph;

	private int m_iCurrentStep = 0;

	private int cntVertices = 0;
	private int cntEdges = 0;
	private int current_loops = 0;
	private PrintWriter logSingle;
	private String graphFile = "";

	/**
	 * Adding edges to the graph, build dictionary of edges and dictionary of
	 * vertices.
	 * 
	 * @param strFileName
	 * @throws NumberFormatException
	 * @throws IOException
	 */
	private void SetupGraph(String strFileName) throws NumberFormatException,
			IOException {

		m_cGraph = new Graph();

		BufferedReader reader = new BufferedReader(new FileReader(new File(
				strFileName)));

		String line = "";
		int i = 0;

		while ((line = reader.readLine()) != null) {
			String[] parts = line.split("\\s+");
			int iBegin = Integer.parseInt(parts[0]);
			int iEnd = Integer.parseInt(parts[1]);
			/** Distance of the edge!!! */
			double dWeight = 0;
			// System.out.println(dWeight);
			m_cGraph.AddEdge(iBegin, iEnd, dWeight);
		}
		Assert.assertTrue("No Edges: " + m_cGraph.m_dictEdges.size(), m_cGraph.m_dictEdges.size() == cntEdges);
		Assert.assertTrue("No vertices: " + m_cGraph.m_dictVertices.size(), m_cGraph.m_dictVertices.size() == cntVertices);

		for (Entry<Integer, VertexValue_v2> vertex : m_cGraph.m_dictVertices.entrySet()) {
			Collections.sort(vertex.getValue().pNeighbours);
			int x = 0;
			x+=1;
		}
		// File f = new File(strFileName);
		// f.delete();

	}

	/**
	 * Pre-compute the common neighbors and exclusive neighbors of every
	 * non-converged edges. Pre-compute sumWeight of every node in G(V,E)
	 * 
	 * @throws Exception
	 */
	private void InitializeGraph() throws Exception {
		HashMap<String, EdgeValue_v2> pEdges = m_cGraph.GetAllEdges();

		int cntCheckSumWeight = 0;
		logSingle.println("Jaccard Distance Initialization ");
		logSingle.flush();
		for (Entry<String, EdgeValue_v2> iter : pEdges.entrySet()) {
			EdgeValue_v2 pEdgeValue = iter.getValue();

			String[] parts = iter.getKey().split("\\s+");
			int iBegin = Integer.parseInt(parts[0]);
			int iEnd = Integer.parseInt(parts[1]);

			ArrayList<Integer> starU = m_cGraph.m_dictVertices.get(iBegin).pNeighbours;
			ArrayList<Integer> starV = m_cGraph.m_dictVertices.get(iEnd).pNeighbours;

			int i = 0;
			int j = 0;
			int m = starU.size();
			int n = starV.size();

			int no_common_neighbor = 0;

			while (i < m && j < n) {
				int a = starU.get(i);
				int b = starV.get(j);
				if (a == b) {
					no_common_neighbor += 1;
					i += 1;
					j += 1;
				} else if (a > b) {
					j += 1;
				} else if (a < b) {
					i += 1;
				}
			}

			// ComputeCommonNeighbour(iBegin, iEnd, pEdgeValue);
			// ComputeExclusiveNeighbour(iBegin, iEnd, pEdgeValue);

			int degu = starU.size() - 1;
			int degv = starV.size() - 1;
			int c = no_common_neighbor;
			double numerator = (double) c; // common neighbor co 2 thang phia trong.
			double denominator = (double) (degu + degv + 2 - c);
			double dis = 1.0 - numerator / denominator;
			pEdgeValue.distance = dis;

			// We need to find the sumWeight of a star graph whatever the edge
			// weight is.
			double dDistance = pEdgeValue.distance;
			// System.out.println(dDistance);
			m_cGraph.UpdateEdge(iBegin, iEnd, dDistance, m_iCurrentStep);
			m_cGraph.AddVertexWeight(iBegin, dDistance, m_iCurrentStep);
			m_cGraph.AddVertexWeight(iEnd, dDistance, m_iCurrentStep);

			/**
			 * This variable is for testing only. It must be equal to |E| of
			 * reduced graph.
			 */
			cntCheckSumWeight += 1;
			

		}
		logSingle.println("Done finding commont neighbors and exclusive neighbors ");
		logSingle.flush();
		Assert.assertTrue(cntCheckSumWeight == this.cntEdges);

		if (Settings.DEBUG) {
			PrintWriter distance_init_out = new PrintWriter(new File("distance_init_out"));

			for (Entry<String, EdgeValue_v2> iter : pEdges.entrySet()) {
				EdgeValue_v2 pEdgeValue = iter.getValue();
				double distance = pEdgeValue.distance;

				String[] parts = iter.getKey().split("\\s+");
				int iBegin = Integer.parseInt(parts[0]);
				int iEnd = Integer.parseInt(parts[1]);

				distance_init_out.println(String.format("%d %d %f", iBegin, iEnd, distance));

			}

			distance_init_out.close();
		}

	}


	/**
	 * 
	 * @param sliding_window
	 * @param miu_sliding_window
	 * @param lambda
	 * @param logJobMaster
	 */
	public JaccardInit(String graphFile, int num_vertices,
			int num_edges, double lambda) {

		this.graphFile = graphFile;
		Settings.lambda = lambda;
		this.cntVertices = num_vertices;
		this.cntEdges = num_edges;
		m_iCurrentStep = 0;

	}

	/**
	 * Run Attractor single machine
	 * 
	 * @param strFileName
	 * @throws Exception
	 */
	public void Execute() throws Exception {

		long tic = System.currentTimeMillis();
		String[] a = graphFile.split("/");
		String graphName = a[a.length - 1];
		logSingle = new PrintWriter("log_single_attractor_full_" + graphName + ".log");

		SetupGraph(this.graphFile);
		InitializeGraph();
		long toc = System.currentTimeMillis();

		logSingle.println("Running time of single machine Jaccard Distance Init Attractor is: " + (toc - tic) / 1000.0);
		logSingle.close();

		if (Settings.DEBUG) {
			//Settings.logEachIteration.close();
		}

	}
	

}
