package no_memory_jaccard;


public class jaccard_starting_point {
	public static void main(String[] args) throws Exception {
		String graphfile = args[0];
		int no_vertices = Integer.parseInt(args[1]);
		int no_edges = Integer.parseInt(args[2]);
		double lambda_ = Double.parseDouble(args[3]);
		JaccardInit singleAttractor = new JaccardInit(graphfile, no_vertices, no_edges, lambda_);
		singleAttractor.Execute();
	}
}
