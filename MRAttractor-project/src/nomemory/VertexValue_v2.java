package nomemory;


import java.util.ArrayList;
import java.util.Set;
import java.util.TreeSet;
public class VertexValue_v2 {
	 public ArrayList<Integer> pNeighbours;
	 double[] aWeightSum;
	 
	 public VertexValue_v2() {
		// TODO Auto-generated constructor stub
		 /**TreeSet is preferred over HashSet since it is memory-efficient. HashSet uses contiguous array which is
		  * not good in terms of memory. 
		  * Iterations over as linked-list is always slower than an array (for sure) but we have no choice keke*/
		 this.pNeighbours = new ArrayList<Integer>();
		 this.aWeightSum = new double[Settings.STEP_LENGTH];
		 this.aWeightSum[0] = 0;
		 this.aWeightSum[1] = 0;
	}
	 
}
