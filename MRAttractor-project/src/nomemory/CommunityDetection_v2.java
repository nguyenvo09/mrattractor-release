package nomemory;

import java.io.BufferedReader;
import java.io.File;
import java.io.FileNotFoundException;
import java.io.FileReader;
import java.io.IOException;
import java.io.PrintWriter;
import java.util.ArrayList;
import java.util.Collections;
import java.util.LinkedList;
import java.util.Queue;
import java.util.Set;
import java.util.HashSet;
import java.util.HashMap;
import java.util.Map.Entry;

import junit.framework.Assert;

import com.google.common.collect.Sets;

public class CommunityDetection_v2 {

	private Graph m_cGraph;

	private int m_iCurrentStep = 0;

	private int cntVertices = 0;
	private int cntEdges = 0;
	private int current_loops = 0;
	private PrintWriter logSingle;
	private String graphFile = "";

	/**
	 * Adding edges to the graph, build dictionary of edges and dictionary of
	 * vertices.
	 * 
	 * @param strFileName
	 * @throws NumberFormatException
	 * @throws IOException
	 */
	private void SetupGraph(String strFileName) throws NumberFormatException,
			IOException {

		m_cGraph = new Graph();

		BufferedReader reader = new BufferedReader(new FileReader(new File(
				strFileName)));

		String line = "";
		int i = 0;

		while ((line = reader.readLine()) != null) {
			String[] parts = line.split("\\s+");
			int iBegin = Integer.parseInt(parts[0]);
			int iEnd = Integer.parseInt(parts[1]);
			/** Distance of the edge!!! */
			double dWeight = 0;
			// System.out.println(dWeight);
			m_cGraph.AddEdge(iBegin, iEnd, dWeight);
		}
		Assert.assertTrue("No Edges: " + m_cGraph.m_dictEdges.size(), m_cGraph.m_dictEdges.size() == cntEdges);
		Assert.assertTrue("No vertices: " + m_cGraph.m_dictVertices.size(), m_cGraph.m_dictVertices.size() == cntVertices);

		for (Entry<Integer, VertexValue_v2> vertex : m_cGraph.m_dictVertices.entrySet()) {
			Collections.sort(vertex.getValue().pNeighbours);
			int x = 0;
			x+=1;
		}
		// File f = new File(strFileName);
		// f.delete();

	}

	/**
	 * Pre-compute the common neighbors and exclusive neighbors of every
	 * non-converged edges. Pre-compute sumWeight of every node in G(V,E)
	 * 
	 * @throws Exception
	 */
	private void InitializeGraph() throws Exception {
		HashMap<String, EdgeValue_v2> pEdges = m_cGraph.GetAllEdges();

		int cntCheckSumWeight = 0;

		for (Entry<String, EdgeValue_v2> iter : pEdges.entrySet()) {
			EdgeValue_v2 pEdgeValue = iter.getValue();

			String[] parts = iter.getKey().split("\\s+");
			int iBegin = Integer.parseInt(parts[0]);
			int iEnd = Integer.parseInt(parts[1]);

			ArrayList<Integer> starU = m_cGraph.m_dictVertices.get(iBegin).pNeighbours;
			ArrayList<Integer> starV = m_cGraph.m_dictVertices.get(iEnd).pNeighbours;

			int i = 0;
			int j = 0;
			int m = starU.size();
			int n = starV.size();

			int no_common_neighbor = 0;

			while (i < m && j < n) {
				int a = starU.get(i);
				int b = starV.get(j);
				if (a == b) {
					no_common_neighbor += 1;
					i += 1;
					j += 1;
				} else if (a > b) {
					j += 1;
				} else if (a < b) {
					i += 1;
				}
			}

			// ComputeCommonNeighbour(iBegin, iEnd, pEdgeValue);
			// ComputeExclusiveNeighbour(iBegin, iEnd, pEdgeValue);

			int degu = starU.size() - 1;
			int degv = starV.size() - 1;
			int c = no_common_neighbor;
			double numerator = (double) c; // common neighbor co 2 thang phia trong.
			double denominator = (double) (degu + degv + 2 - c);
			double dis = 1.0 - numerator / denominator;
			pEdgeValue.distance = dis;

			// We need to find the sumWeight of a star graph whatever the edge
			// weight is.
			double dDistance = pEdgeValue.distance;
			// System.out.println(dDistance);
			m_cGraph.UpdateEdge(iBegin, iEnd, dDistance, m_iCurrentStep);
			m_cGraph.AddVertexWeight(iBegin, dDistance, m_iCurrentStep);
			m_cGraph.AddVertexWeight(iEnd, dDistance, m_iCurrentStep);

			/**
			 * This variable is for testing only. It must be equal to |E| of
			 * reduced graph.
			 */
			cntCheckSumWeight += 1;
			

		}
		logSingle.println("Done finding commont neighbors and exclusive neighbors");
		logSingle.flush();
		Assert.assertTrue(cntCheckSumWeight == this.cntEdges);

		if (Settings.DEBUG) {
			PrintWriter distance_init_out = new PrintWriter(new File("distance_init_out"));

			for (Entry<String, EdgeValue_v2> iter : pEdges.entrySet()) {
				EdgeValue_v2 pEdgeValue = iter.getValue();
				double distance = pEdgeValue.distance;

				String[] parts = iter.getKey().split("\\s+");
				int iBegin = Integer.parseInt(parts[0]);
				int iEnd = Integer.parseInt(parts[1]);

				distance_init_out.println(String.format("%d %d %f", iBegin, iEnd, distance));

			}

			distance_init_out.close();
		}

	}

	

	/**
	 * Dynamic Interaction
	 * 
	 * @throws Exception
	 */
	private void DynamicInteraction() throws Exception {
		boolean bContinue = true;

		HashMap<String, EdgeValue_v2> pEdges = m_cGraph.GetAllEdges();

		// cout << "Dynamic Iteraction Start" << endl;
		// const clock_t begin_time = clock();

		if (Settings.DEBUG) {
			Settings.logEachIteration = new PrintWriter("log_distance_each_iteration");
		}

		int cntLoop = this.current_loops;
		int loopSingle = 0;
		while (bContinue) {
			bContinue = false;
			int iNextStep = Helper.NextStep(m_iCurrentStep);
			System.out.println("Single Machine Current Loop: " + (cntLoop + 1));
			long tic = System.currentTimeMillis();
			int iConvergeNumber = 0;

			if (Settings.DEBUG) {
				Settings.logEachIteration.println("Loop: " + (cntLoop + 1));
			}

			for (Entry<String, EdgeValue_v2> iter : pEdges.entrySet()) {
				EdgeValue_v2 pEdgeValue = iter.getValue();
				String[] parts = iter.getKey().split("\\s+");
				int iBegin = Integer.parseInt(parts[0]);
				int iEnd = Integer.parseInt(parts[1]);
				double _ddDI = 0, _ddEI = 0, _ddCI = 0;
				if (pEdgeValue.aDistance[m_iCurrentStep] > 0 && pEdgeValue.aDistance[m_iCurrentStep] < 1) {
					// 2604 2601

					ArrayList<Integer> starU = m_cGraph.m_dictVertices.get(iBegin).pNeighbours;
					ArrayList<Integer> starV = m_cGraph.m_dictVertices.get(iEnd).pNeighbours;

					int i = 0;
					int j = 0;
					int m = starU.size();
					int n = starV.size();
					
					double dDI = ComputeDI(iBegin, iEnd, pEdgeValue);
					double dCI = 0;
					double dEI = 0;
					

					while (i < m && j < n) {
						int a = starU.get(i);
						int b = starV.get(j);
						if (a == b) {
							int common = starU.get(i);
							dCI += computeCI2(iBegin, iEnd, common);
							
							i += 1;
							j += 1;
						} else if (a > b) {
							dEI += computeEI2(iEnd, iBegin, starV.get(j));
							j += 1;
							
						} else if (a < b) {
							dEI += computeEI2(iBegin, iEnd, starU.get(i));
							i += 1;
						}
					}
					while(i < m){
						dEI += computeEI2(iBegin, iEnd, starU.get(i));
						i += 1;
					}
					while(j<n){
						dEI += computeEI2(iEnd, iBegin, starV.get(j));
						j += 1;
					}

					double delta = dDI + dCI + dEI;
					_ddDI = dDI;
					_ddEI = dEI;
					_ddCI = dCI;
					if (delta > Settings.PRECISE || delta < -Settings.PRECISE) {
						
						double newDistance = m_cGraph.Distance(iBegin, iEnd, m_iCurrentStep) + delta;

						if (newDistance > 1 - Settings.PRECISE) {
							newDistance = 1;
						} else if (newDistance < Settings.PRECISE) {
							newDistance = 0;
						}

						m_cGraph.UpdateEdge(iBegin, iEnd, newDistance, iNextStep);
						m_cGraph.AddVertexWeight(iBegin, newDistance, iNextStep);
						m_cGraph.AddVertexWeight(iEnd, newDistance, iNextStep);
						bContinue = true;
					}
				} else {
					pEdgeValue.aDistance[iNextStep] = pEdgeValue.aDistance[m_iCurrentStep];
					double newDistance = pEdgeValue.aDistance[m_iCurrentStep];
					m_cGraph.AddVertexWeight(iBegin, newDistance, iNextStep);
					m_cGraph.AddVertexWeight(iEnd, newDistance, iNextStep);

					iConvergeNumber++;
				}
				if (Settings.DEBUG) {
					String edge = iter.getKey();
					String test = String.format("%s ,oldDis: %.8f, newDis: %.8f, DI: %.8f, EI: %.8f, CI: %.8f", edge, 
									m_cGraph.Distance(iBegin, iEnd, m_iCurrentStep), 
									m_cGraph.Distance(iBegin, iEnd, iNextStep),
									_ddDI, _ddEI, _ddCI);
					Settings.logEachIteration.println(test);
					// logSingle.println(test);
				}
			}

			long toc = System.currentTimeMillis();

			cntLoop += 1;
			loopSingle += 1;
			logSingle.println("Current Iteration of single machine: " + cntLoop + " Running Time: " + (toc - tic) / 1000.0);
			logSingle.flush();
			m_cGraph.ClearVertexWeight(m_iCurrentStep);
			m_iCurrentStep = Helper.UpdateStep(m_iCurrentStep);
		}

		// update cnt loop
		logSingle.println("#Loops of single machine: " + (loopSingle - 1));

	}

	

	

	

	private double ComputeDI(int iBegin, int iEnd, EdgeValue_v2 pEdgeValue)
			throws Exception {
		return -Math.sin(1 - pEdgeValue.aDistance[m_iCurrentStep])
				* (1 / (double) (m_cGraph.GetVertexNeighbours(iBegin).size() - 1) + 1 / (double) (m_cGraph
						.GetVertexNeighbours(iEnd).size() - 1));
	}


	private double computeCI2(int iBegin, int iEnd, int common) throws Exception {
		double dCI = 0;

		int iSharedVertex = common;

		// avoid re-computation.
		if (iBegin == iSharedVertex || iEnd == iSharedVertex) {
			return 0;
		}

		double dBegin = m_cGraph.Distance(iBegin, iSharedVertex, m_iCurrentStep);
		double dEnd = m_cGraph.Distance(iEnd, iSharedVertex, m_iCurrentStep);

		dCI += Math.sin(1 - dBegin) * (1 - dEnd) / (m_cGraph.GetVertexNeighbours(iBegin).size() - 1)
				+ Math.sin(1 - dEnd) * (1 - dBegin) / (m_cGraph.GetVertexNeighbours(iEnd).size() - 1);

		return -dCI;
	}

	
	private double computeEI2(int iBegin, int iEnd, int exclusiveNeighborOfiBegin) throws Exception{
		double dEI = 0;
		
		dEI += ComputePartialEI2(iBegin, iEnd, exclusiveNeighborOfiBegin);
		
		return -dEI;
	}
	
	private double ComputePartialEI2(int iTarget, int iTargetNeighbour, int targetEN) throws Exception {

		double dDistance = 0;
		Integer iter = targetEN;
		dDistance += Math.sin(1 - m_cGraph.Distance(iter, iTarget, m_iCurrentStep))
				* ComputeInfluence(iTargetNeighbour, iter, iTarget)
				/ (m_cGraph.GetVertexNeighbours(iTarget).size() - 1);
		
		return dDistance;
	}
	

	private double ComputeInfluence(int iTargetNeighbour, int iENVertex, int iTarget) throws Exception {
		double dDistance = 1 - ComputeVirtualDistance(iTargetNeighbour,
				iENVertex, iTarget);

		if (dDistance >= Settings.lambda)
			return dDistance;

		return dDistance - Settings.lambda;
	}


	private double ComputeVirtualDistance(int iBegin, int iEnd, int iTarget)
			throws Exception {
		//int iTempBegin = iBegin;
		//int iTempEnd = iEnd;

		//String edgeKey = Graph.RefineEdgeKey(iTempBegin, iTempEnd);

		double dNumerator = 0;
		ArrayList<Integer> pBeginNeighbours = m_cGraph.GetVertexNeighbours(iBegin);
		ArrayList<Integer> pEndNeighbours = m_cGraph.GetVertexNeighbours(iEnd);
		
		int i=0;
		int j=0;
		int m = pBeginNeighbours.size();
		int n = pEndNeighbours.size();
		
		while(i<m && j<n){
			int a = pBeginNeighbours.get(i);
			int b = pEndNeighbours.get(j);
			if(a == b){
				Integer iter = pBeginNeighbours.get(i);
				double dBegin = m_cGraph.Distance(iBegin, iter, m_iCurrentStep);
				double dEnd = m_cGraph.Distance(iEnd, iter, m_iCurrentStep);
				dNumerator += (1 - dBegin) + (1 - dEnd);
				i+=1;
				j+=1;
			}else if(a > b){
				j+=1;
			}else{
				i+=1;
			}
		}
		

		double dDenominator = m_cGraph.GetVertexWeightSum(iBegin, m_iCurrentStep) + m_cGraph.GetVertexWeightSum(iEnd, m_iCurrentStep);

		double dDistance = 1 - dNumerator / dDenominator;

		return dDistance;
	}

	/**
	 * 
	 * @param sliding_window
	 * @param miu_sliding_window
	 * @param lambda
	 * @param logJobMaster
	 */
	public CommunityDetection_v2(String graphFile, int num_vertices,
			int num_edges, double lambda) {

		this.graphFile = graphFile;
		Settings.lambda = lambda;
		this.cntVertices = num_vertices;
		this.cntEdges = num_edges;
		m_iCurrentStep = 0;

	}

	/**
	 * Run Attractor single machine
	 * 
	 * @param strFileName
	 * @throws Exception
	 */
	public void Execute() throws Exception {

		long tic = System.currentTimeMillis();
		String[] a = graphFile.split("/");
		String graphName = a[a.length - 1];
		logSingle = new PrintWriter("log_single_attractor_full_" + graphName + ".log");

		SetupGraph(this.graphFile);
		InitializeGraph();
		// UpdateSlidingWindow(merged_sliding_windows_file);
		DynamicInteraction();
		long toc = System.currentTimeMillis();

		logSingle.println("Running time of single machine Attractor is: " + (toc - tic) / 1000.0);
		logSingle.close();

		if (Settings.DEBUG) {
			Settings.logEachIteration.close();
		}

	}

}
