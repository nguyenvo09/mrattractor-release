package nomemory;


import junit.framework.Assert;

public class EdgeKey_v2 {
	int iBegin;
	int iEnd;
	public EdgeKey_v2(int iBegin, int iEnd) {
		// TODO Auto-generated constructor stub
		Assert.assertTrue(iBegin > iEnd);
		this.iBegin = iBegin;
		this.iEnd = iEnd;
	}
	@Override
	public String toString() {
		// TODO Auto-generated method stub
		return iBegin + " " + iEnd;
	}
	public int hashCode() {
		String r = this.toString();
		return r.hashCode();
	};
}
