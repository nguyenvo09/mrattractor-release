package nomemory;

public class StartingPoint {
	public static void main(String[] args) throws Exception {
				
		String graphfile = args[0];
		int no_vertices = Integer.parseInt(args[1]);
		int no_edges = Integer.parseInt(args[2]);
		double lambda_ = Double.parseDouble(args[3]);
		CommunityDetection_v2 singleAttractor = new CommunityDetection_v2(graphfile, no_vertices, no_edges, lambda_);
		singleAttractor.Execute();
	}
}
