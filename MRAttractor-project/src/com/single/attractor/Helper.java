package com.single.attractor;

public class Helper {

	public static int ComputeNextStep(int iCurrentStep) {
		return iCurrentStep == 0 ? 1 : 0;
	}

	public static int UpdateStep(int iCurrentStep) {
		iCurrentStep = ComputeNextStep(iCurrentStep);
		return iCurrentStep;
	}

	public static int NextStep(int iCurrentStep) {
		return ComputeNextStep(iCurrentStep);
	}

}
