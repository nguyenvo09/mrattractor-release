package com.single.attractor;

public class Settings {
	public static int STEP_LENGTH = 2;
	public static double PRECISE = 0.0000001;
	public static int EDGE_ENDPOINT_NUMBER = 2;
	public static int BEGIN_POINT = 0;
	public static int END_POINT = 1;
	public static double lambda = 0.5;
	
	public static int SLIDINNG_WINDOW_SIZE = -1;
	public static int LIMIT_SIZE_DICT_VIRTUAL_EDGES = -1;
	public static double DEFAULT_SUPPORT_SLIDING_WINDOW = -0.7;
	
}
