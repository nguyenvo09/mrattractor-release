package com.single.attractor;

import java.io.BufferedReader;
import java.io.File;
import java.io.FileNotFoundException;
import java.io.FileReader;
import java.io.IOException;
import java.io.PrintWriter;
import java.util.ArrayList;
import java.util.LinkedList;
import java.util.Queue;
import java.util.Set;
import java.util.HashSet;
import java.util.HashMap;
import java.util.Map.Entry;

import junit.framework.Assert;

import com.google.common.collect.Sets;
import com.mrattractor.MasterMR;

public class CommunityDetection {

	private Graph m_cGraph;

	private int m_iCurrentStep = 0;

	HashMap<String, Double> m_dictVirtualEdgeTempResult;
	HashMap<Integer, Integer> m_dictInteration;

	private int no_vertices_reduced_graph = 0;
	private int no_edges_reduced_graph = 0;
	private int current_loops = 0;
	private int no_vertices_original = 0;
	private String outputfile="";
	private PrintWriter logSingle;
	private PrintWriter logMaster;
	/**
	 * Adding edges to the graph, build dictionary of edges and dictionary of vertices.
	 * @param strFileName
	 * @throws NumberFormatException
	 * @throws IOException
	 */
	private void SetupGraph(String strFileName) throws NumberFormatException, IOException {

		m_cGraph = new Graph();
		m_dictVirtualEdgeTempResult = new HashMap<String, Double>();
		m_dictInteration = new HashMap<Integer, Integer>();
		BufferedReader reader = new BufferedReader(new FileReader(new File(strFileName)));

		String line = "";
		int i = 0;

		while ((line = reader.readLine()) != null) {
			String[] parts = line.split("\\s+");
			int iBegin = Integer.parseInt(parts[0]);
			int iEnd = Integer.parseInt(parts[1]);
			/**Distance of the edge!!!*/
			double dWeight = Double.parseDouble(parts[2]);
			//System.out.println(dWeight);
			m_cGraph.AddEdge(iBegin, iEnd, dWeight);
			no_edges_reduced_graph += 1;
		}
		Assert.assertTrue(no_edges_reduced_graph == m_cGraph.m_dictEdges.size());
		no_vertices_reduced_graph = m_cGraph.m_dictVertices.size();
		
		if(MasterMR.DEBUG){
			try{
				
				int[] dirty = new int[no_vertices_original];
				reader = new BufferedReader(new FileReader(new File(strFileName)));
				line = "";
				while((line = reader.readLine()) != null){
					String[] args = line.split("\\s+");
					int u = Integer.parseInt(args[0]);
					int v = Integer.parseInt(args[1]);
					double dis = Double.parseDouble(args[2]);
					if(dis < 1 && dis > 0){
						u -= 1;
						v -= 1;
						dirty[u] = 1;
						dirty[v] = 1;
					}

					
				}
				reader = new BufferedReader(new FileReader(new File(strFileName)));
				line = "";
				while((line = reader.readLine()) != null){
					String[] args = line.split("\\s+");
					int u = Integer.parseInt(args[0]);
					int v = Integer.parseInt(args[1]);
					u -= 1;
					v -= 1;
					if(dirty[u] == 1 || dirty[v] == 1){
						//If one of the endpoints of an edge is dirty, we need to keep this edge, whatever its distance!!!!
						if(dirty[u] == 0){
							dirty[u] = 2;
						}
						if(dirty[v] == 0){
							dirty[v] = 2;
						} 
					}
				}
				
				reader = new BufferedReader(new FileReader(new File("MrAttractor/degfile.txt")));
				line = "";
				HashMap<Integer, Integer> mapDeg = new HashMap<Integer, Integer>();
				while((line = reader.readLine()) != null){
					String[] args = line.split("\\s+");
					int vertex = Integer.parseInt(args[0]);
					int deg = Integer.parseInt(args[1]);
					mapDeg.put(vertex, deg);
				}
				for(Entry<Integer, VertexValue> entry: m_cGraph.m_dictVertices.entrySet()){
					int vertex = entry.getKey();
					int currentDeg = entry.getValue().pNeighbours.size() - 1;
			
					if(dirty[vertex-1] != 0)
						Assert.assertTrue("Something wrong in degree", mapDeg.get(vertex) == currentDeg);
				}
			}catch(Exception e){
				e.printStackTrace();
			}
			
		}
//		File f = new File(strFileName);
//		f.delete();
		
	}
	/**
	 * Pre-compute the common neighbors and exclusive neighbors of every non-converged edges.
	 * Pre-compute sumWeight of every node in G(V,E)
	 * @throws Exception
	 */
	private void InitializeGraph() throws Exception {
		HashMap<String, EdgeValue> pEdges = m_cGraph.GetAllEdges();
		
		int cntCheckSumWeight = 0;
		
		
		for (Entry<String, EdgeValue> iter : pEdges.entrySet()) {
			EdgeValue pEdgeValue = iter.getValue();
			
			
			String[] parts = iter.getKey().split("\\s+");
			int iBegin = Integer.parseInt(parts[0]);
			int iEnd = Integer.parseInt(parts[1]);
			if(iBegin == 2604 && iEnd == 2601){
				int x = 0;
				x+=1;
			}
			//We need to find the sumWeight of a star graph whatever the edge weight is.
			double dDistance = pEdgeValue.distance;
//			System.out.println(dDistance);
			m_cGraph.UpdateEdge(iBegin, iEnd, dDistance, m_iCurrentStep);
			m_cGraph.AddVertexWeight(iBegin, dDistance, m_iCurrentStep);
			m_cGraph.AddVertexWeight(iEnd, dDistance, m_iCurrentStep);
			
			/**This variable is for testing only. It must be equal to |E| of reduced graph.*/
			cntCheckSumWeight += 1; 
			if(pEdgeValue.distance < 1 && pEdgeValue.distance > 0){
				//We only consider non-converged edges to avoid out-of-memory issue.
				ComputeCommonNeighbour(iBegin, iEnd, pEdgeValue);
				ComputeExclusiveNeighbour(iBegin, iEnd, pEdgeValue);
			}
			
			
			
		}
		logMaster.println("Done finding commont neighbors and exclusive neighbors");
		logMaster.flush();
		Assert.assertTrue(cntCheckSumWeight == this.no_edges_reduced_graph);
	}
	/**Updating sliding window from MR version.
	 * @throws IOException */
	private void UpdateSlidingWindow(String merged_sliding_windows_file) throws IOException{
		if (merged_sliding_windows_file.equalsIgnoreCase("")){
			return;
		}
		
		BufferedReader reader = new BufferedReader(new FileReader(new File(merged_sliding_windows_file)));
		String line = "";
		int no_loops_MR = Math.min(this.current_loops + 1, Settings.SLIDINNG_WINDOW_SIZE);
		
		int[] status = new int[no_loops_MR];
		while((line =reader.readLine()) != null){
			String[] args = line.split("\\s+");
			int u = Integer.parseInt(args[0]);
			int v = Integer.parseInt(args[1]);
			String edgeKey = Graph.RefineEdgeKey(u, v);
			if(m_cGraph.m_dictEdges.containsKey(edgeKey)){
				EdgeValue vl = m_cGraph.m_dictEdges.get(edgeKey);
				for(int j=3; j<args.length; j+=1){
					int s = Integer.parseInt(args[j]);
					Assert.assertTrue(s==0 || s == 1);
					status[j-3] = s;
				}
				vl.SetSlidingWindow(no_loops_MR, status);
			}
		}
		File f = new File(merged_sliding_windows_file);
		f.delete();
		
		
	}
	
	/**
	 * Dynamic Interaction
	 * @throws Exception
	 */
	private void DynamicInteraction() throws Exception {
		boolean bContinue = true;

		HashMap<String, EdgeValue> pEdges = m_cGraph.GetAllEdges();

		// cout << "Dynamic Iteraction Start" << endl;
		// const clock_t begin_time = clock();
		
		int i = 0;
		int cntLoop = this.current_loops;
		int loopSingle = 0;
		while (bContinue) {
			bContinue = false;
			int iNextStep = Helper.NextStep(m_iCurrentStep);
			System.out.println("Single Machine Current Loop: " + (cntLoop + 1));
			long tic = System.currentTimeMillis();
			int iConvergeNumber = 0;
			if(MasterMR.DEBUG){
				String filename = String.format("MrAttractor/LoopPhase3_%s/test/test_DI_CI_EI_%s-r-00000", cntLoop+1, cntLoop+1);
				File file = new File(filename);
				file.getParentFile().mkdirs();

				logSingle = new PrintWriter(filename);
			}
			
			for (Entry<String, EdgeValue> iter : pEdges.entrySet()) {
				EdgeValue pEdgeValue = iter.getValue();
				String[] parts = iter.getKey().split("\\s+");
				int iBegin = Integer.parseInt(parts[0]);
				int iEnd = Integer.parseInt(parts[1]);
				double _ddDI = 0, _ddEI = 0, _ddCI = 0;
				if (pEdgeValue.aDistance[m_iCurrentStep] > 0 && pEdgeValue.aDistance[m_iCurrentStep] < 1) {
					//2604 2601
					if(iBegin == 2604 && iEnd == 2601){
						int x = 0;
						x+=1;
					}
					double dDI = ComputeDI(iBegin, iEnd, pEdgeValue);
					// const clock_t cibegin_time = clock();
					double dCI = ComputeCI(iBegin, iEnd, pEdgeValue);
					// const clock_t eiBegin_time = clock();
					// ciTime += eiBegin_time - cibegin_time;
					double dEI = ComputeEI(iBegin, iEnd, pEdgeValue);
					// eiTime += clock() - eiBegin_time;
					double delta = dDI + dCI + dEI;
					_ddDI = dDI; _ddEI = dEI; _ddCI = dCI;
					if (delta > Settings.PRECISE || delta < -Settings.PRECISE) {
						// add delta to delta window of edge
						// No Sliding Window here!!!!
						if(Settings.SLIDINNG_WINDOW_SIZE > 0){
							//System.out.println("Using Sliding Window Single Machine");
							delta = pEdgeValue.addNewDelta2Window(delta);
							//pEdgeValue.addNewDelta2Window(delta);
						}
						double newDistance = m_cGraph.Distance(iBegin, iEnd, m_iCurrentStep) + delta;

						if (newDistance > 1 - Settings.PRECISE) {
							newDistance = 1;
						} else if (newDistance < Settings.PRECISE) {
							newDistance = 0;
						}

						m_cGraph.UpdateEdge(iBegin, iEnd, newDistance, iNextStep);
						m_cGraph.AddVertexWeight(iBegin, newDistance, iNextStep);
						m_cGraph.AddVertexWeight(iEnd, newDistance, iNextStep);
						bContinue = true;
					}
				} else {
					pEdgeValue.aDistance[iNextStep] = pEdgeValue.aDistance[m_iCurrentStep];
					double newDistance = pEdgeValue.aDistance[m_iCurrentStep];
					m_cGraph.AddVertexWeight(iBegin, newDistance, iNextStep);
					m_cGraph.AddVertexWeight(iEnd, newDistance, iNextStep);

					iConvergeNumber++;
				}
				if(MasterMR.DEBUG){
					String edge = iter.getKey();
					String test = String.format("%s ,oldDis: %.8f, newDis: %.8f, DI: %.8f, EI: %.8f, CI: %.8f", edge, 
							m_cGraph.Distance(iBegin, iEnd, m_iCurrentStep), 
							m_cGraph.Distance(iBegin, iEnd, iNextStep), 
							_ddDI, _ddEI, _ddCI);

					logSingle.println(test);
				}
			}
			if(MasterMR.DEBUG){
				logSingle.close();
			}
			long toc = System.currentTimeMillis();
			
			cntLoop += 1;
			loopSingle += 1;
			logMaster.println("Current Iteration of single machine: " + cntLoop + " Running Time: " + (toc-tic)/1000.0);
			logMaster.flush();
			m_cGraph.ClearVertexWeight(m_iCurrentStep);
			m_dictVirtualEdgeTempResult.clear();
			m_dictInteration.put(i, iConvergeNumber);
			m_iCurrentStep = Helper.UpdateStep(m_iCurrentStep);
		}
		
		//update cnt loop
		logMaster.println("#Loops of single machine: " + (loopSingle-1) + " #Loops single+MR is: " + cntLoop);
		PrintWriter writer = new PrintWriter(new File(this.outputfile));
		for (Entry<String, EdgeValue> iter : pEdges.entrySet()) {
			EdgeValue pEdgeValue = iter.getValue();
			String[] parts = iter.getKey().split("\\s+");
			int iBegin = Integer.parseInt(parts[0]);
			int iEnd = Integer.parseInt(parts[1]);
			String s = String.format("%s %s %.8f G", iBegin, iEnd, pEdgeValue.aDistance[m_iCurrentStep]);
			writer.println(s);
		}
		writer.close();
	}

	private void SetUnion(Set<Integer> left, Set<Integer> right, Set<Integer> dest) {
		Sets.union(left, right).copyInto(dest);
	}

	private void SetDifference(Set<Integer> left, Set<Integer> right, Set<Integer> dest) {

		Sets.difference(left, right).copyInto(dest);
		
	}

	private void SetIntersection(Set<Integer> left, Set<Integer> right, Set<Integer> dest) {

		Sets.intersection(left, right).copyInto(dest);
	}

	private double ComputeDI(int iBegin, int iEnd, EdgeValue pEdgeValue) throws Exception {
		return -Math.sin(1 - pEdgeValue.aDistance[m_iCurrentStep]) * 
				(1 / (double) (m_cGraph.GetVertexNeighbours(iBegin).size() - 1) 
				+ 1 / (double) (m_cGraph.GetVertexNeighbours(iEnd).size() - 1));
	}

	private double ComputeCI(int iBegin, int iEnd, EdgeValue pEdgeValue) throws Exception {
		
		
		if(iBegin == 2604 && iEnd == 2601){
			int x = 0;
			x+=1;
		}
		double dCI = 0;

		for (Integer iter : pEdgeValue.pCommonNeighbours) {
			int iSharedVertex = iter;
			
			//avoid re-computation.
			if (iBegin == iSharedVertex || iEnd == iSharedVertex) {
				continue;
			}

			double dBegin = m_cGraph.Distance(iBegin, iSharedVertex, m_iCurrentStep);
			double dEnd = m_cGraph.Distance(iEnd, iSharedVertex, m_iCurrentStep);

			dCI +=    Math.sin(1 - dBegin) * (1 - dEnd) / (m_cGraph.GetVertexNeighbours(iBegin).size() - 1)
					+ Math.sin(1 - dEnd) * (1 - dBegin) / (m_cGraph.GetVertexNeighbours(iEnd).size() - 1);

		}

		return -dCI;
	}

	private double ComputeEI(int iBegin, int iEnd, EdgeValue pEdgeValue) throws Exception {
		double dEI = 0;

		dEI += ComputePartialEI(iBegin, iEnd, pEdgeValue.pExclusiveNeighbours.get(Settings.BEGIN_POINT))
				+ ComputePartialEI(iEnd, iBegin, pEdgeValue.pExclusiveNeighbours.get(Settings.END_POINT));

		return -dEI;
	}

	private double ComputePartialEI(int iTarget, int iTargetNeighbour, Set<Integer> targetEN) throws Exception {

		double dDistance = 0;

		for (Integer iter : targetEN) {
			dDistance += Math.sin(1 - m_cGraph.Distance(iter, iTarget, m_iCurrentStep))
					* ComputeInfluence(iTargetNeighbour, iter, iTarget)
					/ (m_cGraph.GetVertexNeighbours(iTarget).size() - 1);
		}

		return dDistance;
	}

	private double ComputeInfluence(int iTargetNeighbour, int iENVertex, int iTarget)
			throws Exception {
		double dDistance = 1 - ComputeVirtualDistance(iTargetNeighbour, iENVertex, iTarget);

		if (dDistance >= Settings.lambda)
			return dDistance;

		return dDistance - Settings.lambda;
	}

	private void ComputeExclusiveNeighbour(int iBegin, int iEnd, EdgeValue pEdgeValue) throws Exception {
		SetDifference(m_cGraph.GetVertexNeighbours(iBegin), pEdgeValue.pCommonNeighbours, pEdgeValue.pExclusiveNeighbours.get(Settings.BEGIN_POINT));
		SetDifference(m_cGraph.GetVertexNeighbours(iEnd), pEdgeValue.pCommonNeighbours, pEdgeValue.pExclusiveNeighbours.get(Settings.END_POINT));
	}
	
	private void ComputeCommonNeighbour(int iBegin, int iEnd, EdgeValue pEdgeValue) throws Exception {
		SetIntersection(m_cGraph.GetVertexNeighbours(iBegin), m_cGraph.GetVertexNeighbours(iEnd), pEdgeValue.pCommonNeighbours);
	}
	/**
	 * This is faster implemention of finding common neighbors and exclusive neighbros
	 * Only applicable for TreeSet!!!
	 * @param iBegin
	 * @param iEnd
	 * @param pEdgeValue
	 * @throws Exception
	 */
	private void ComputeCommonAndExclusiveNeighbour(int iBegin, int iEnd, EdgeValue pEdgeValue) throws Exception{
		Set<Integer> set1 = m_cGraph.GetVertexNeighbours(iBegin);
		Set<Integer> set2 = m_cGraph.GetVertexNeighbours(iEnd);
		if(MasterMR.DEBUG){
			Integer prev = -1;
			for(Integer e : set1){
				Assert.assertTrue(e > prev);
				prev = e;
			}
			prev = -1;
			for(Integer e : set2){
				Assert.assertTrue(e > prev);
				prev = e;
			}
		}
		
		
	}

	private double ComputeVirtualDistance(int iBegin, int iEnd, int iTarget)
			throws Exception {
		int iTempBegin = iBegin;
		int iTempEnd = iEnd;

		String edgeKey = Graph.RefineEdgeKey(iTempBegin, iTempEnd);

		
		if (m_dictVirtualEdgeTempResult.containsKey(edgeKey)) { 
			return m_dictVirtualEdgeTempResult.get(edgeKey); 
		}
		 

		double dNumerator = 0;
		Set<Integer> pBeginNeighbours = m_cGraph.GetVertexNeighbours(iBegin);
		Set<Integer> pEndNeighbours = m_cGraph.GetVertexNeighbours(iEnd);

		Set<Integer> setCommonNeighbours = new HashSet<Integer>();

		SetIntersection(pBeginNeighbours, pEndNeighbours, setCommonNeighbours);

		for (Integer iter : setCommonNeighbours) {
			double dBegin = m_cGraph.Distance(iBegin, iter, m_iCurrentStep);
			double dEnd = m_cGraph.Distance(iEnd, iter, m_iCurrentStep);
			dNumerator += (1 - dBegin) + (1 - dEnd);
		}

		double dDenominator = m_cGraph.GetVertexWeightSum(iBegin, m_iCurrentStep)
							+ m_cGraph.GetVertexWeightSum(iEnd, m_iCurrentStep);

		double dDistance = 1 - dNumerator / dDenominator;
		
		if(m_dictVirtualEdgeTempResult.size() < Settings.LIMIT_SIZE_DICT_VIRTUAL_EDGES){
			m_dictVirtualEdgeTempResult.put(edgeKey, dDistance);
		}
		return dDistance;
	}
	/**
	 * 
	 * @param sliding_window
	 * @param miu_sliding_window
	 * @param lambda
	 * @param logJobMaster
	 */
	public CommunityDetection(int sliding_window, 
			double threshold_sliding_windows, 
			double lambda, 
			int cache_size_single_Attractor, 
			int currentLoop,
			int originalVertices,
			String outFileName,
			PrintWriter logJobMaster) {
		
		Settings.SLIDINNG_WINDOW_SIZE = sliding_window;
		Settings.DEFAULT_SUPPORT_SLIDING_WINDOW = threshold_sliding_windows;
		Settings.lambda = lambda;
		Settings.LIMIT_SIZE_DICT_VIRTUAL_EDGES = cache_size_single_Attractor;
		this.current_loops = currentLoop;
		this.no_vertices_original = originalVertices;
		m_iCurrentStep = 0;
		this.logMaster = logJobMaster;
		this.outputfile = outFileName;
	}
	/**
	 * Run Attractor single machine
	 * @param strFileName
	 * @throws Exception
	 */
	public void Execute(String strFileName, String merged_sliding_windows_file) throws Exception {
		SetupGraph(strFileName);
		InitializeGraph();
		UpdateSlidingWindow(merged_sliding_windows_file);
		DynamicInteraction();
	}

}
