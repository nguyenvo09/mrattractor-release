package com.single.attractor;

import java.util.ArrayList;
import java.util.BitSet;
import java.util.List;
import java.util.Set;
import java.util.TreeSet;

import junit.framework.Assert;

public class EdgeValue {
	
	private BitSet bDeltaWindow;
	private int iNewestDeltaIndex = 0;

	double distance;
	double[] aDistance;
	Set<Integer> pCommonNeighbours;
	List<Set<Integer>> pExclusiveNeighbours;

	public EdgeValue(double disuv) {
		this.aDistance = new double[Settings.STEP_LENGTH];
		for (int i = 0; i < Settings.STEP_LENGTH; ++i) {
			this.aDistance[i] = 0;
		}
		Assert.assertTrue(disuv >= 0 && disuv <= 1);
		this.distance = disuv;
		/**TreeSet is preferred our case because it uses Red-Black Tree implementation which is efficient in terms on memory
		 * We do not modify the set during Dynamic Interaction so we don't need HashSet for performance */
		this.pCommonNeighbours = new TreeSet<Integer>();
		this.pExclusiveNeighbours = new ArrayList<Set<Integer>>();
		this.pExclusiveNeighbours.add(new TreeSet<Integer>());
		this.pExclusiveNeighbours.add(new TreeSet<Integer>());

	}
	/**Checking sliding windows */
	public double addNewDelta2Window(double dDelta) {
		if (bDeltaWindow == null) {
			bDeltaWindow = new BitSet(32);
		}
		//System.out.println(iNewestDeltaIndex);
		if (dDelta < 0) {
			bDeltaWindow.clear(iNewestDeltaIndex % Settings.SLIDINNG_WINDOW_SIZE);
			// bDeltaWindow->reset(iNewestDeltaIndex % iWindowSize);
		} else {

			bDeltaWindow.set(iNewestDeltaIndex % Settings.SLIDINNG_WINDOW_SIZE);
		}

		int iSumSameSign = 0;
		if (iNewestDeltaIndex >= Settings.SLIDINNG_WINDOW_SIZE - 1) {
			if (bDeltaWindow.get(iNewestDeltaIndex % Settings.SLIDINNG_WINDOW_SIZE)) {
				// iSumSameSign = bDeltaWindow->count();
				iSumSameSign = bDeltaWindow.cardinality();
				if (iSumSameSign > Settings.DEFAULT_SUPPORT_SLIDING_WINDOW * Settings.SLIDINNG_WINDOW_SIZE) {
					dDelta = 2;
				}
			} else {
				iSumSameSign = Settings.SLIDINNG_WINDOW_SIZE - bDeltaWindow.cardinality();
				if (iSumSameSign > Settings.DEFAULT_SUPPORT_SLIDING_WINDOW * Settings.SLIDINNG_WINDOW_SIZE) {
					dDelta = -2;
				}
			}

		}
		iNewestDeltaIndex++;
		return dDelta;
	}
	
	/** Updating the slidingWindow from previous MR version. This is used to guarantee the correctness of this algorithm.
	 * @param currentLoop : the number of loops of MR version
	 * @param status : The sliding window status from iteration 0 to current iteration. */
	public void SetSlidingWindow(int currentLoop, int[] status){
		if (bDeltaWindow == null) {
			bDeltaWindow = new BitSet(32);
		}
		Assert.assertTrue(currentLoop == status.length);
		Assert.assertTrue(status.length <= Settings.SLIDINNG_WINDOW_SIZE);
		for(int i=0; i<status.length; i+=1){
			Assert.assertTrue(status[i] == 0 || status[i] == 1);
			if(status[i] == 1){
				bDeltaWindow.set(i);
			}
		}
		iNewestDeltaIndex = status.length;
		
	}
	void initWindowSize(int iSize) {
		Settings.SLIDINNG_WINDOW_SIZE = iSize;
	}
}
