package com.single.attractor;

import java.util.HashMap;
import java.util.Map.Entry;
import java.util.HashSet;
import java.util.Set;

public class Graph {
	HashMap<String, EdgeValue> m_dictEdges;
	HashMap<Integer, VertexValue> m_dictVertices;

	int BEGIN_POINT = 0;
	int END_POINT = 0;

	public boolean AddEdge(int iBegin, int iEnd, double dWeight) {
		String edgeKey = RefineEdgeKey(iBegin, iEnd);

		if (m_dictEdges.containsKey(edgeKey) == true) {
			return false;
		}
		//if(dWeight > 0 && dWeight < 1){
		m_dictEdges.put(edgeKey, new EdgeValue(dWeight));
		//}
		AddVertex(iBegin, iEnd);
		AddVertex(iEnd, iBegin);

		return true;
	}

	public void UpdateEdge(int iBegin, int iEnd, double dNewDistance, int iStep) throws Exception {
		String edgeKey = RefineEdgeKey(iBegin, iEnd);

		if (m_dictEdges.containsKey(edgeKey) == false) {
			throw new Exception("No Such Edges");
		}

		m_dictEdges.get(edgeKey).aDistance[iStep] = dNewDistance;
	}

	public double Distance(int iBegin, int iEnd, int iStep) throws Exception {
		if (iBegin == iEnd)
			return 0;

		String edgeKey = RefineEdgeKey(iBegin, iEnd);

		if (m_dictEdges.containsKey(edgeKey) == false) {
			throw new Exception("No edge");
		}

		return m_dictEdges.get(edgeKey).aDistance[iStep];
	}

	public double Weight(int iBegin, int iEnd) {
		if (iBegin == iEnd)
			return 0.0;

		String edgeKey = RefineEdgeKey(iBegin, iEnd);

		if (m_dictEdges.containsKey(edgeKey) == false) {
			return 0.0;
		}

		return m_dictEdges.get(edgeKey).distance;
	}

	public double GetVertexWeightSum(int iVertexId, int iStep) throws Exception {
		if (m_dictVertices.containsKey(iVertexId) == false) {
			throw new Exception("Vertex is not exist.");
		}

		return m_dictVertices.get(iVertexId).aWeightSum[iStep];
	}

	public void AddVertexWeight(int iVertexId, double dDistance, int iStep) throws Exception {
		if (m_dictVertices.containsKey(iVertexId) == false) {
			throw new Exception("Vertex is not exist.");
		}

		m_dictVertices.get(iVertexId).aWeightSum[iStep] += 1 - dDistance;
	}

	public void ClearVertexWeight(int iStep) {

		for (Entry<Integer, VertexValue> iter : m_dictVertices.entrySet()) {
			iter.getValue().aWeightSum[iStep] = 0;
		}

		// for (map<int, VertexValue*>::iterator iter = m_dictVertices.begin();
		// iter != m_dictVertices.end(); iter++)
		// {
		// iter->second->aWeightSum[iStep] = 0;
		// }
	}

	public HashMap<String, EdgeValue> GetAllEdges() {
		return m_dictEdges;
	}

	public Set<Integer> GetVertexNeighbours(int iVertexId) throws Exception {
		if (m_dictVertices.containsKey(iVertexId) == false) {
			
			throw new Exception("No such an iVertexId.");
			
		}

		VertexValue vertexValue = m_dictVertices.get(iVertexId);
		return vertexValue.pNeighbours;
	}

	// Graph::~Graph()
	// {
	// ClearVertices();
	// ClearEdges();
	// }
	
	public Graph() {
		// TODO Auto-generated constructor stub
		m_dictEdges = new HashMap<String, EdgeValue>();
		m_dictVertices = new HashMap<Integer, VertexValue>();
	}
	
	public void AddVertex(int iBegin, int iEnd) {
		if (m_dictVertices.containsKey(iBegin) == false) {
			VertexValue vl = new VertexValue();
			m_dictVertices.put(iBegin, vl);
			vl.pNeighbours.add(iBegin);
		}

		m_dictVertices.get(iBegin).pNeighbours.add(iEnd);
	}

	public static String RefineEdgeKey(int iBegin, int iEnd) {
		if (iBegin > iEnd) {
			return iBegin + " " + iEnd;
		}
		return iEnd + " " + iBegin;
	}

	

}
