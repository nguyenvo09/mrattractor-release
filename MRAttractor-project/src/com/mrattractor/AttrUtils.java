package com.mrattractor;


import java.io.BufferedReader;
import java.io.IOException;
import java.io.InputStreamReader;
import java.util.HashMap;

import org.apache.hadoop.conf.Configuration;
import org.apache.hadoop.fs.FileSystem;
import org.apache.hadoop.fs.Path;


public class AttrUtils {
	public static HashMap<Integer, Integer> readDegMap(Configuration conf, String key) throws IOException {
		
		try {
			String name = conf.get(key);
			Path pt = new Path(name);
			FileSystem fs = FileSystem.get(conf);
			BufferedReader br = new BufferedReader(new InputStreamReader(fs.open(pt)));
			
			HashMap<Integer, Integer> map = new HashMap<Integer, Integer>();
			String line = "";
			while ((line = br.readLine()) != null) {
				String[] args = line.split("\\s+");
				int deg = Integer.parseInt(args[1]);
				int u = Integer.parseInt(args[0]);
				map.put(u, deg);
			}
			return map;
		} catch (Exception e) {
			e.printStackTrace();
		} finally {
			
		}
		return null;
	}
	
	public static HashMap<String, Integer> readDegMap(Configuration conf) throws IOException {
		String name = conf.get("deg_file");
		Path pt = new Path(name);
		FileSystem fs = FileSystem.get(conf);
		BufferedReader br = new BufferedReader(new InputStreamReader(fs.open(pt)));
		
		HashMap<String, Integer> map = new HashMap<String, Integer>();
		try {
			String line = "";
			while ((line = br.readLine()) != null) {
				String[] args = line.split("\\s+");
				int deg = Integer.parseInt(args[1]);
				
				map.put(args[0], deg);
			}

		} catch (Exception e) {
			e.printStackTrace();
		} finally {
			br.close();
		}
		return map;
	}
	public static String GenKey(String u, String v) {
		int uu = Integer.parseInt(u);
		int vv = Integer.parseInt(v);
		String key = uu + " " + vv;
		if (uu < vv) {
			key = vv + " " + uu;
		}
		return key;
	}
	
	/* Return the map of edges including weight, loaded into memory of each node */
	public static HashMap<String, Double> getMapEdge(Configuration conf)
			throws IOException {
		String name = conf.get("edge_file");
		Path pt = new Path(name);
		FileSystem fs = FileSystem.get(conf);
		BufferedReader br = new BufferedReader(new InputStreamReader(fs.open(pt)));
		HashMap<String, Double> map = new HashMap<String, Double>();
		try {
			String line = "";
			while ((line = br.readLine()) != null) {
				String[] args = line.split("\\s+");
				String key = GenKey(args[0], args[1]);
				map.put(key, Double.parseDouble(args[2]));
//				System.out.println(key + " " + args[2]);
			}

		} catch(Exception e){
			e.printStackTrace();
//			System.out.println("error");
		}
		
		return map;
	}
}
