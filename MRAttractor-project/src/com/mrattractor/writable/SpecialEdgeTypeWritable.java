package com.mrattractor.writable;

import java.io.DataInput;
import java.io.DataOutput;
import java.io.IOException;
import java.util.ArrayList;
import java.util.BitSet;

import junit.framework.Assert;

import org.apache.commons.lang.NotImplementedException;
import org.apache.hadoop.io.Writable;
import org.apache.hadoop.io.WritableComparable;

/**
 * A compound type of edge (not really happy with this)
 * 
 * @author nguyenvo
 *
 */
public class SpecialEdgeTypeWritable implements WritableComparable<Writable> {

	/**
	 * Details please see {@link Settings} 
	 */
	public char type;

	public int center;
	public int target;
	public double weight;
	
	/**for star graph of only {@value SpecialEdgeTypeWritable#center}*/
	public int no_triplet_graphs;
	public ArrayList<TripleWritable> triplet_graphs;

	/**for sliding windows only*/
	public int noBits;
	public BitSet sliding;
	
	
	public SpecialEdgeTypeWritable() {
		// TODO Auto-generated constructor stub
	}

	public void init(char _type, int _center, int _target, double _weight,
			int _no_triplet_graphs, ArrayList<TripleWritable> _triples, int _noBits, BitSet _sliding) {
		this.type = _type;
		switch (this.type) {
		
		case Settings.EDGE_TYPE: 
		{
			this.center = _center;
			this.target = _target;
			this.weight = _weight;
			break;
		}
		case Settings.STAR_GRAPH: 
		{
			this.center = _center;
			this.no_triplet_graphs = _no_triplet_graphs;
			this.triplet_graphs = _triples;
			Assert.assertTrue(this.triplet_graphs != null);
			Assert.assertTrue(this.triplet_graphs.size() == no_triplet_graphs);
			break;
		}
		case Settings.C_TYPE:
		{
			this.center = _center;
			this.target = _target;
			this.weight = _weight;
			break;
		}
		case Settings.D_TYPE:
		{
			this.center = _center;
			this.target = _target;
			this.weight = _weight;
			break;
		}
		case Settings.E_TYPE:
		{
			this.center = _center;
			this.target = _target;
			this.weight = _weight;
			break;
		}
		case Settings.INTERACTION_TYPE:
		{
			this.center = _center;
			this.target = _target;
			this.weight = _weight;
			break;
		}
		case Settings.SLIDING:
		{
			this.center = _center;
			this.target = _target;
			this.noBits = _noBits;
			this.sliding = _sliding;
			break;
		}
		default:
			break;
		}

	}

	@Override
	public void readFields(DataInput inp) throws IOException {
		// TODO Auto-generated method stub
		type = inp.readChar();
		switch (type) {
		case Settings.EDGE_TYPE: 
		{
			center = inp.readInt();
			target = inp.readInt();
			weight = inp.readDouble();
			break;
		}
		case Settings.STAR_GRAPH: 
		{
			center = inp.readInt();
			no_triplet_graphs = inp.readInt();
			triplet_graphs = new ArrayList<TripleWritable>();
			for (int i = 0; i < no_triplet_graphs; i += 1) {
				TripleWritable e = new TripleWritable();
				e.readFields(inp);
				triplet_graphs.add(e);
			}
			break;
		}
		case Settings.D_TYPE: 
		{
			center = inp.readInt();
			target = inp.readInt();
			weight = inp.readDouble();
			break;
		}
		case Settings.C_TYPE: 
		{
			center = inp.readInt();
			target = inp.readInt();
			weight = inp.readDouble();
			break;
		}
		case Settings.E_TYPE: 
		{
			center = inp.readInt();
			target = inp.readInt();
			weight = inp.readDouble();
			break;
		}
		case Settings.INTERACTION_TYPE: 
		{
			center = inp.readInt();
			target = inp.readInt();
			weight = inp.readDouble();
			break;
		}
		case Settings.SLIDING:
		{
			center = inp.readInt();
			target = inp.readInt();
			noBits = inp.readInt();
			sliding = new BitSet(noBits);
			for(int i=0; i<noBits; i+=1){
				boolean vl = inp.readBoolean();
				if(vl == true){
					sliding.set(i);
				}
			}
			break;
		}
		default:
			break;
		}
	}

	@Override
	public void write(DataOutput out) throws IOException {
		// TODO Auto-generated method stub
		out.writeChar(type);
		switch (type) {
		case Settings.EDGE_TYPE: 
		{
			out.writeInt(center);
			out.writeInt(target);
			out.writeDouble(weight);
			break;
		}
		case Settings.STAR_GRAPH: 
		{
			out.writeInt(center);
			out.writeInt(no_triplet_graphs);
			for (int i = 0; i < no_triplet_graphs; i += 1) {
				TripleWritable e = triplet_graphs.get(i);
				e.write(out);
			}
			break;
		}
		case Settings.INTERACTION_TYPE: 
		{
			out.writeInt(center);
			out.writeInt(target);
			out.writeDouble(weight);
			break;
		}
		case Settings.E_TYPE: 
		{
			out.writeInt(center);
			out.writeInt(target);
			out.writeDouble(weight);
			break;
		}
		case Settings.D_TYPE: 
		{
			out.writeInt(center);
			out.writeInt(target);
			out.writeDouble(weight);
			break;
		}
		case Settings.C_TYPE: 
		{
			out.writeInt(center);
			out.writeInt(target);
			out.writeDouble(weight);
			break;
		}
		case Settings.SLIDING:
		{
			out.writeInt(center);
			out.writeInt(target);
			out.writeInt(noBits);
			for(int i=0; i<noBits; i+=1){
				boolean vl = sliding.get(i);
				out.writeBoolean(vl);
			}
			break;
		}
		default:
			break;
		}
	}

	@Override
	public int compareTo(Writable target) {
		// TODO Auto-generated method stub
		return 0;
	}

	@Override
	public int hashCode() {
		// TODO Auto-generated method stub
		return super.hashCode();
	}

	public String toStringForLocalMachine() throws Exception{
		switch (type) {
		case Settings.EDGE_TYPE:
		{
			return center + " " + target + " " + weight + " " + type;
		}
		case Settings.STAR_GRAPH:
		{
			throw new Exception("This is not type for single machine");
		}
		case Settings.D_TYPE:
		{
			throw new Exception("This is not type for single machine");
		}
		case Settings.C_TYPE:
		{
			throw new Exception("This is not type for single machine");
		}
		case Settings.E_TYPE:
		{
			throw new Exception("This is not type for single machine");
		}
		case Settings.INTERACTION_TYPE:
		{
			throw new Exception("This is not type for single machine");
		}
		case Settings.SLIDING:
		{
			String s = "";
			for(int i=0; i<noBits; i+=1){
				if(sliding.get(i)){
					s += "1 ";
				}else{
					s += "0 ";
				}
			}
			return center + " " + target + " " + type + " " + s;
			//throw new NotImplementedException();
		}
		default:
			break;
		}
		return super.toString();
	}
	
	@Override
	public String toString() {
		// TODO Auto-generated method stub
		switch (type) {
		case Settings.EDGE_TYPE:
		{
			return type + " " + center + " " + target + " " + weight;
		}
		case Settings.STAR_GRAPH:
		{
			StringBuilder sb = new StringBuilder();
			for(TripleWritable trip: this.triplet_graphs){
				sb.append(trip.toString() + ",");
			}
			String vl = sb.toString();
			return type + " " + center + " " + vl;
			//throw new NotImplementedException("Chua implement here");
			//break;
		}
		case Settings.D_TYPE:
		{
			return type + " " + center + " " + target + " " + weight;
		}
		case Settings.C_TYPE:
		{
			return type + " " + center + " " + target + " " + weight;
		}
		case Settings.E_TYPE:
		{
			return type + " " + center + " " + target + " " + weight;
		}
		case Settings.INTERACTION_TYPE:
		{
			return type + " " + center + " " + target + " " + weight;
		}
		case Settings.SLIDING:
		{
			String s = "";
			for(int i=0; i<noBits; i+=1){
				if(sliding.get(i)){
					s += "1 ";
				}else{
					s += "0 ";
				}
			}
			return type + " " + center + " " + target + " " + s;
			//throw new NotImplementedException();
		}
		default:
			break;
		}
		return super.toString();
	}

}
