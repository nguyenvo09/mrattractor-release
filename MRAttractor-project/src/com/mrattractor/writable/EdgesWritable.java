package com.mrattractor.writable;

import java.io.DataInput;
import java.io.DataOutput;
import java.io.IOException;
import java.util.ArrayList;

import junit.framework.Assert;

import org.apache.hadoop.io.WritableComparable;


public class EdgesWritable implements WritableComparable<EdgesWritable>{

	public ArrayList<PairWritable> edges;
	public int no_edges = 0;
	public EdgesWritable() {}
	
	public void set(ArrayList<PairWritable> _edges){
		this.edges = _edges;
		this.no_edges = _edges.size();
	}
	
	@Override
	public void readFields(DataInput in) throws IOException {
		// TODO Auto-generated method stub
		edges = new ArrayList<PairWritable>();
		no_edges = in.readInt();
		edges = new ArrayList<PairWritable>();
		for(int i=0; i<no_edges; i++){
			PairWritable p = new PairWritable();
			p.readFields(in);
			edges.add(p);
		}
		
	}

	@Override
	public void write(DataOutput out) throws IOException {
		// TODO Auto-generated method stub
		out.writeInt(no_edges);
		
		for(int i=0; i<no_edges; i++){
			PairWritable p = edges.get(i);
			p.write(out);
		}
	}

	@Override
	public int compareTo(EdgesWritable target) {
		// TODO Auto-generated method stub
		return this.toString().compareToIgnoreCase(target.toString());		
	}
	@Override
	public String toString() {
		// TODO Auto-generated method stub
		StringBuilder sb = new StringBuilder();
		for(int i=0; i<no_edges; i++){
			PairWritable curr = edges.get(i);
			//String e = String.format("%d %d", curr.left, curr.right);
			String e = curr.left + " " + curr.right;
			sb.append(e+",");
		}
		return sb.toString();
	}
	@Override
	public int hashCode() {
		// TODO Auto-generated method stub
		return this.toString().hashCode();
	}
}
