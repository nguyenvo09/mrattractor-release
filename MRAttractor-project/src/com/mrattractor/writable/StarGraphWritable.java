

package com.mrattractor.writable;


import java.io.DataInput;
import java.io.DataOutput;
import java.io.IOException;
import java.util.ArrayList;

import junit.framework.Assert;

import org.apache.hadoop.io.Writable;

import com.mrattractor.MasterMR;


/**
 * ...
 * <P>
 * @author ben
 */
public class StarGraphWritable implements Writable{
	
	////////////////////////////////////
	// public fields
	////////////////////////////////////
	
	public int center; // @center of stargraph
	//In function readFields, we need this information to load writable objects
	public int degCenter; //degree of center node. we need to use this dude for loading from file!!!
	
	public ArrayList<NeighborWritable> neighbors;
	

	////////////////////////////////////
	// public methods
	////////////////////////////////////
	
	public StarGraphWritable() {};
	
	
	public void set(int center, int degcenter, ArrayList<NeighborWritable> neighbors){
		this.center = center;
		this.degCenter = degcenter;
		this.neighbors = neighbors;
		Assert.assertTrue("Mismatched Size", this.neighbors.size() == this.degCenter);
		
	}

	/*
	 * (non-Javadoc)
	 * @see org.apache.hadoop.io.Writable#readFields(java.io.DataInput)
	 */
	public void readFields(DataInput in) throws IOException {
		
		this.center = in.readInt();
		this.degCenter = in.readInt();
		this.neighbors = new ArrayList<NeighborWritable>();
		for(int i=0; i<this.degCenter; i++){
			NeighborWritable e = new NeighborWritable();
			e.readFields(in);
			this.neighbors.add(e);
		}
		
	}

	/*
	 * (non-Javadoc)
	 * @see org.apache.hadoop.io.Writable#write(java.io.DataOutput)
	 */
	public void write(DataOutput out) throws IOException {
		out.writeInt(center);
		out.writeInt(degCenter);
		for(int i=0; i<this.degCenter; i++){
			NeighborWritable e = this.neighbors.get(i);
			e.write(out);
		}
	}
	@Override
	public int hashCode() {
		// TODO Auto-generated method stub
		return super.hashCode();
	}
	@Override
	public String toString() {
		// TODO Auto-generated method stub
		StringBuilder sb = new StringBuilder();

		for(NeighborWritable e : neighbors){
			sb.append(","+e.toString());
		}
		String vl=sb.toString();
		return center + " " + degCenter + " " + vl;
		//return String.format("%s %s %s", center, degCenter, vl);
	}
}
