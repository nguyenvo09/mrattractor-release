

package com.mrattractor.writable;

import java.io.DataInput;
import java.io.DataOutput;
import java.io.IOException;

import org.apache.hadoop.io.WritableComparable;
import org.junit.Assert;

/**
 * Key type to save triples
 * <P>
 * 
 * @author ben
 */
public class JaccardEdgeWritable implements WritableComparable<JaccardEdgeWritable> {

	// //////////////////////////////////
	// public fields
	// //////////////////////////////////

	public int u;
	public int v;
//	public int degu;
//	public int degv;
	public double dis;

	// //////////////////////////////////
	// private fields
	// //////////////////////////////////

	private static final int PRIME = 1000003;

	// //////////////////////////////////
	// public methods
	// //////////////////////////////////
	
	public JaccardEdgeWritable(){
		
	}
	
	public void set(int u, int v, double dis){
		try {
			this.u = u;
			this.v = v;
//			this.degu = degu;
//			this.degv = degv;
			this.dis = dis;
		} catch (Exception e) {
			e.printStackTrace();
		}
	}
	
	/*
	 * (non-Javadoc)
	 * 
	 * @see org.apache.hadoop.io.Writable#readFields(java.io.DataInput)
	 */
	public void readFields(DataInput in) throws IOException {
		u = in.readInt();
//		degu = in.readInt();
		v = in.readInt();
//		degv = in.readInt();
		dis = in.readDouble();
	}

	/*
	 * (non-Javadoc)
	 * 
	 * @see org.apache.hadoop.io.Writable#write(java.io.DataOutput)
	 */
	public void write(DataOutput out) throws IOException {
		out.writeInt(u);
//		out.writeInt(degu);
		out.writeInt(v);
//		out.writeInt(degv);
		out.writeDouble(dis);
	}

	/*
	 * (non-Javadoc)
	 * 
	 * @see java.lang.Comparable#compareTo(java.lang.Object)
	 */
	public int compareTo(JaccardEdgeWritable target) {
		int cmp = u - target.u;
		if (cmp != 0)
			return cmp;
		cmp = v - target.v;
		return cmp;
		
	}

	/*
	 * (non-Javadoc)
	 * 
	 * @see java.lang.Object#hashCode()
	 */
	public int hashCode() {
//		String r = String.format("%s %s %s", left, mid, right);
//		return r.hashCode();
		return u * PRIME + v;
	}
	@Override
	public String toString() {
		// TODO Auto-generated method stub
		return u + " " + v + " " + dis + " " + "G";
		//return String.format("%d %d %.8f G", u, v, dis);
	}
}
