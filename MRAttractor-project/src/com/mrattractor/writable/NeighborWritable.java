package com.mrattractor.writable;

import java.io.DataInput;
import java.io.DataOutput;
import java.io.IOException;

import org.apache.hadoop.io.WritableComparable;

import junit.framework.Assert;


public class NeighborWritable implements WritableComparable<NeighborWritable> {
	public int lab = -1;
//	public int deg = -1;
	public double dis = -1;

	public NeighborWritable() {}
	
	public void set(int v_, double _dis){
//		deg = _deg;
		dis = _dis;
		lab = v_;
//		Assert.assertTrue(deg >= 0 && lab >= 0);
	}

	@Override
	public int compareTo(NeighborWritable o) {
		// TODO Auto-generated method stub
		if (lab > o.lab) {
			return 1;
		}
		if (lab == o.lab) {
			return 0;
		}
		return -1;
	}
	@Override
	public String toString() {
		// TODO Auto-generated method stub
		return lab + " " + dis;
		//return String.format("%s %.8f", lab, dis);
	}

	@Override
	public void readFields(DataInput in) throws IOException {
		lab = in.readInt();
//		deg = in.readInt();
		dis = in.readDouble();
	}

	@Override
	public void write(DataOutput out) throws IOException {
		out.writeInt(lab);
//		out.writeInt(deg);
		out.writeDouble(dis);
	}
}