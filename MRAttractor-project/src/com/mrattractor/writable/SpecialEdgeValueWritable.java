package com.mrattractor.writable;

import java.io.DataInput;
import java.io.DataOutput;
import java.io.IOException;
import java.util.ArrayList;
import java.util.BitSet;

import org.apache.hadoop.io.Writable;

public class SpecialEdgeValueWritable implements Writable{
	
	
	public char type;
	public double value;
	
	public int noBits;
	public BitSet sliding;
	
	public SpecialEdgeValueWritable() {
		// TODO Auto-generated constructor stub
	}
	
	public void init(char _type, double _value, int _noBits, BitSet _sliding){
		this.type = _type;
		this.value = _value;
		this.sliding = _sliding;
		this.noBits = _noBits;
	}
	
	@Override
	public void readFields(DataInput inp) throws IOException {
		// TODO Auto-generated method stub
		type = inp.readChar();
		switch (type) {
		case Settings.EDGE_TYPE: 
		{
			value = inp.readDouble();
			break;
		}
		case Settings.STAR_GRAPH: 
		{
			throw new ExceptionInInitializerError("Something wrong with this");
		}
		case Settings.D_TYPE: 
		{
			value = inp.readDouble();
			break;
		}
		case Settings.C_TYPE: 
		{
			value = inp.readDouble();
			break;
		}
		case Settings.E_TYPE: 
		{
			value = inp.readDouble();
			break;
		}
		case Settings.INTERACTION_TYPE: 
		{
			value = inp.readDouble();
			break;
		}
		case Settings.SLIDING:
		{
			noBits = inp.readInt();
			sliding = new BitSet(noBits);
			for(int i=0; i<noBits; i+=1){
				boolean vl = inp.readBoolean();
				if(vl == true){
					sliding.set(i);
				}
			}
			break;
		}
		default:
			break;
		}
	}

	@Override
	public void write(DataOutput out) throws IOException {
		// TODO Auto-generated method stub
		out.writeChar(type);
		switch (type) {
		case Settings.EDGE_TYPE: 
		{
			out.writeDouble(value);
			break;
		}
		case Settings.STAR_GRAPH: 
		{
			throw new ExceptionInInitializerError("There will be no value like this!!!");
//			break;
		}
		case Settings.INTERACTION_TYPE: 
		{
			out.writeDouble(value);
			break;
		}
		case Settings.E_TYPE: 
		{
			out.writeDouble(value);
			break;
		}
		case Settings.D_TYPE: 
		{
			out.writeDouble(value);
			break;
		}
		case Settings.C_TYPE: 
		{
			out.writeDouble(value);
			break;
		}
		case Settings.SLIDING:
		{
			out.writeInt(noBits);
			for(int i=0; i<noBits; i+=1){
				boolean vl = sliding.get(i);
				out.writeBoolean(vl);
			}
			break;
		}
		default:
			break;
		}
	}
	@Override
	public String toString() {
		// TODO Auto-generated method stub
		switch (type) {
		case Settings.EDGE_TYPE:
		{
			return type + " " + value;
		}
		case Settings.STAR_GRAPH:
		{			
			break;
		}
		case Settings.D_TYPE:
		{
			return type + " " + value;
		}
		case Settings.C_TYPE:
		{
			return type + " " + value;
		}
		case Settings.E_TYPE:
		{
			return type + " " + value;
		}
		case Settings.INTERACTION_TYPE:
		{
			return type + " " + value;
		}
		case Settings.SLIDING:
		{
			String s = "";
			for(int i=0; i<noBits; i+=1){
				if(sliding.get(i)){
					s += "1 ";
				}else{
					s += "0 ";
				}
			}
			return type + " " + s;
			//throw new NotImplementedException();
		}
		default:
			break;
		}
		
		return super.toString();
	}
	

}
