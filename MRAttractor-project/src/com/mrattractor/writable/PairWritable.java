

package com.mrattractor.writable;


import java.io.DataInput;
import java.io.DataOutput;
import java.io.IOException;

import org.apache.hadoop.io.WritableComparable;
import org.junit.Assert;

/**
 * Key type to save pairs
 * <P>
 * @author ben
 */
public class PairWritable implements WritableComparable<PairWritable>{

	////////////////////////////////////
	// public fields
	////////////////////////////////////
	
	public int left;
	public int right;
	
	////////////////////////////////////
	// private fields
	////////////////////////////////////
	
	private static final int PRIME = 1000003;
	
	////////////////////////////////////
	// public methods
	////////////////////////////////////
	
	public PairWritable(){}
	
	/**
	 * set fields
	 * @param left left element
	 * @param right	right element
	 */
	public void set(int left, int right){
		this.left = Math.max(left, right);
		this.right = Math.min(left, right);
	}
	public void set(String a, String b){
		int u = Integer.parseInt(a);
		int v = Integer.parseInt(b);
		this.left = Math.max(u, v);
		this.right = Math.min(u, v);
		Assert.assertTrue(this.left > this.right);
	}
	/*
	 * (non-Javadoc)
	 * @see org.apache.hadoop.io.Writable#readFields(java.io.DataInput)
	 */
	public void readFields(DataInput in) throws IOException {
		left = in.readInt();
		right = in.readInt();
	}

	/*
	 * (non-Javadoc)
	 * @see org.apache.hadoop.io.Writable#write(java.io.DataOutput)
	 */
	public void write(DataOutput out) throws IOException {
		out.writeInt(left);
		out.writeInt(right);
	}

	/*
	 * (non-Javadoc)
	 * @see java.lang.Comparable#compareTo(java.lang.Object)
	 */
	public int compareTo(PairWritable target) {
		int cmp = left - target.left;
		if(cmp != 0)
			return cmp;
		else
			return right - target.right;
	}
	
	/*
	 * (non-Javadoc)
	 * @see java.lang.Object#hashCode()
	 */
	public int hashCode() {
		String r = this.toString();
		return r.hashCode();
    }
	@Override
	public String toString() {
		// TODO Auto-generated method stub
		return left + " " + right;
		//return String.format("%s %s", left, right);
	}
	
}
