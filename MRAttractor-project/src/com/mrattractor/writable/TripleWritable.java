

package com.mrattractor.writable;

import java.io.DataInput;
import java.io.DataOutput;
import java.io.IOException;

import org.apache.hadoop.io.WritableComparable;
import org.junit.Assert;

/**
 * Key type to save triples
 * <P>
 * 
 * @author ben
 */
public class TripleWritable implements WritableComparable<TripleWritable> {

	// //////////////////////////////////
	// public fields
	// //////////////////////////////////

	public int left;
	public int mid;
	public int right;

	// //////////////////////////////////
	// private fields
	// //////////////////////////////////

	private static final int PRIME = 1000003;

	// //////////////////////////////////
	// public methods
	// //////////////////////////////////

	public TripleWritable() {

	}

	public void set(String subgraph_key) {
		try {
			String[] s = subgraph_key.split("\\s+");
			this.left = Integer.parseInt(s[0]);
			this.mid = Integer.parseInt(s[1]);
			this.right = Integer.parseInt(s[2]);
			Assert.assertTrue("Wrong subgraph", right > mid && mid > left);
		} catch (Exception e) {
			e.printStackTrace();
		}
	}

	public void set(int a, int b, int c) {
		if (a > c) {
			int t = a; a = c; c = t;
		}
		if (a > b) {
			int t = a; a = b; b = t;
		}
		if (b > c) {
			int t = b; b = c; c = t;
		}
		//c > b > a
		Assert.assertTrue("Wrong subgraph", c > b && b > a);
		this.left = a;
		this.mid = b;
		this.right = c;
	}

	/*
	 * (non-Javadoc)
	 * 
	 * @see org.apache.hadoop.io.Writable#readFields(java.io.DataInput)
	 */
	public void readFields(DataInput in) throws IOException {
		left = in.readInt();
		mid = in.readInt();
		right = in.readInt();
	}

	/*
	 * (non-Javadoc)
	 * 
	 * @see org.apache.hadoop.io.Writable#write(java.io.DataOutput)
	 */
	public void write(DataOutput out) throws IOException {
		out.writeInt(left);
		out.writeInt(mid);
		out.writeInt(right);
	}

	/*
	 * (non-Javadoc)
	 * 
	 * @see java.lang.Comparable#compareTo(java.lang.Object)
	 */
	public int compareTo(TripleWritable target) {
		int cmp = left - target.left;
		if (cmp != 0)
			return cmp;
		cmp = mid - target.mid;
		if (cmp != 0)
			return cmp;
		return right - target.right;
	}

	/*
	 * (non-Javadoc)
	 * 
	 * @see java.lang.Object#hashCode()
	 */
	public int hashCode() {
		//String r = String.format("%s %s %s", left, mid, right);
		//String r = this.toString();
		//return r.hashCode();
		return left * PRIME * PRIME + mid * PRIME + right * PRIME;
	}
	@Override
	public boolean equals(Object obj) {
		if ((obj instanceof TripleWritable)){
			TripleWritable target = ((TripleWritable) obj);
			if (left == target.left && right == target.right && mid == target.mid){
				return true;
			}else{
				return false;
			}
        } else {
            return false;
        }
	}
	@Override
	public String toString() {
		// TODO Auto-generated method stub
		return left + " " + mid + " " + right;
		// String.format is very slow
		//return String.format("%s %s %s", left, mid, right);
	}
}
