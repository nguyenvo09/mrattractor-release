package com.mrattractor.writable;

import java.io.DataInput;
import java.io.DataOutput;
import java.io.IOException;
import java.util.ArrayList;

import junit.framework.Assert;

import org.apache.hadoop.io.Writable;

public class StarGraphWithPartitionWritable implements Writable {

	// //////////////////////////////////
	// public fields
	// //////////////////////////////////

	public int center; // @center of stargraph 
	
	public int degCenter; // degree of center node. we need to use this dude for loading from file!!!
	public ArrayList<NeighborWritable> neighbors;
	
	public int no_tripleSubGraphs;
	public ArrayList<TripleWritable> tripleSubGraphs;

	// //////////////////////////////////
	// public methods
	// //////////////////////////////////

	public StarGraphWithPartitionWritable() {
	};

	public void set(int center, ArrayList<NeighborWritable> _neighbors, ArrayList<TripleWritable> _tripleSubGraphs) {
		this.center = center;
		
		//star graph
		if(_neighbors == null){
			this.degCenter = -1;
		}else{
			this.degCenter = _neighbors.size();
			this.neighbors = _neighbors;
		}
		
		//triplet partitions
		if(_tripleSubGraphs == null){
			this.no_tripleSubGraphs = -1;
		}else{
			this.no_tripleSubGraphs = _tripleSubGraphs.size();
			this.tripleSubGraphs = _tripleSubGraphs;
		}
		
		
		//Assert.assertTrue("Mismatched Size", this.neighbors.size() == this.degCenter);

	}

	/*
	 * (non-Javadoc)
	 * 
	 * @see org.apache.hadoop.io.Writable#readFields(java.io.DataInput)
	 */
	public void readFields(DataInput in) throws IOException {

		this.center = in.readInt();
		this.degCenter = in.readInt();
		this.no_tripleSubGraphs = in.readInt();
		if(this.degCenter > 0){
			this.neighbors = new ArrayList<NeighborWritable>();
			for (int i = 0; i < this.degCenter; i++) {
				NeighborWritable e = new NeighborWritable();
				e.readFields(in);
				this.neighbors.add(e);
			}
		}
		if(this.no_tripleSubGraphs > 0){
			this.tripleSubGraphs = new ArrayList<TripleWritable>();
			for(int i=0; i<this.no_tripleSubGraphs; i+=1){
				TripleWritable subgraph = new TripleWritable();
				subgraph.readFields(in);
				this.tripleSubGraphs.add(subgraph);
			}

		}
		
	}

	/*
	 * (non-Javadoc)
	 * 
	 * @see org.apache.hadoop.io.Writable#write(java.io.DataOutput)
	 */
	public void write(DataOutput out) throws IOException {
		out.writeInt(center);
		out.writeInt(degCenter);
		out.writeInt(no_tripleSubGraphs);
		
		if(degCenter != -1){
			for (int i = 0; i < this.degCenter; i++) {
				NeighborWritable e = this.neighbors.get(i);
				e.write(out);
			}
		}
		if(no_tripleSubGraphs != -1){
			for(int i=0; i<this.no_tripleSubGraphs; i+=1){
				TripleWritable e = this.tripleSubGraphs.get(i);
				e.write(out);
			}
		}
		
	}

	@Override
	public int hashCode() {
		// TODO Auto-generated method stub
		return super.hashCode();
	}

	@Override
	public String toString() {
		// TODO Auto-generated method stub
		StringBuilder sb = new StringBuilder();
		sb.append(center + " ");
		sb.append(degCenter + " ");
		sb.append(no_tripleSubGraphs + " ");
		if(degCenter > 0){
			for (NeighborWritable e : neighbors) {
				sb.append("," + e.toString());
			}
		}
		sb.append(";");
		if(no_tripleSubGraphs > 0){
			for(TripleWritable g : this.tripleSubGraphs){
				sb.append(g.toString() + ";");
			}
		}
		String vl = sb.toString();
		return vl;
		// return String.format("%s %s %s", center, degCenter, vl);
	}

	

}
