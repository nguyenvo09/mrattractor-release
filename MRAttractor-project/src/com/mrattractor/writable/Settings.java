package com.mrattractor.writable;

public class Settings {
	/**
	 * Edge in the graph
	 */
	public final static char EDGE_TYPE = 'G';
	
	/**
	 * Star graph of center node
	 */
	public final static char STAR_GRAPH = 'S';
	/**
	 * Edges with interaction values
	 */
	public final static char INTERACTION_TYPE = 'I';
	/**
	 * Only for DEBUG
	 */
	public final static char C_TYPE = 'C';
	/**
	 * Only for DEBUG
	 */
	public final static char D_TYPE = 'D';
	/**
	 * Only for DEBUG
	 */
	public final static char E_TYPE = 'E';
	
	/**
	 * Sliding window
	 */
	public final static char SLIDING = 'L';
}
