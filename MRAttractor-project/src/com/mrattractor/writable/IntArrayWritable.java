package com.mrattractor.writable;

import java.io.DataInput;
import java.io.DataOutput;
import java.io.IOException;
import java.util.ArrayList;

import org.apache.hadoop.io.Writable;
import org.junit.Assert;
/**
 * This class is for both degree and neighbors.
 * @author ben
 *
 */
public class IntArrayWritable implements Writable{
	
	public ArrayList<Integer> neighbors;
	public int noNeighbors;
	
	public IntArrayWritable() {
		// TODO Auto-generated constructor stub
	}
	
	
	public void set(int _noNeighbors, ArrayList<Integer> _neighbors) {
		this.noNeighbors = _noNeighbors;
		this.neighbors = _neighbors;
		if(this.neighbors != null){
			Assert.assertTrue(this.noNeighbors == this.neighbors.size());
		}
	}
	
	
	@Override
	public void readFields(DataInput inp) throws IOException {
		// TODO Auto-generated method stub
		//We only read star graph. we will never read <u> <deg(u)> with this function.
		this.noNeighbors = inp.readInt();
		neighbors = new ArrayList<Integer>();
		for(int i=0; i<this.noNeighbors; i+=1){
			neighbors.add(inp.readInt());
		}
	}

	@Override
	public void write(DataOutput out) throws IOException {
		if(this.neighbors == null){
			//This is only saving degree
			//out.writeInt(noNeighbors);
		}else{
			out.writeInt(this.noNeighbors);
			for(int i=0; i<this.noNeighbors; i+=1){
				out.writeInt(neighbors.get(i));
			}
		}
	}
	@Override
	public String toString() {
		// TODO Auto-generated method stub
		
		if(this.neighbors == null){
			return "" + noNeighbors;
		}else{
			StringBuilder sb = new StringBuilder();
			sb.append(noNeighbors + ":");
			for(int i=0; i<this.neighbors.size(); i+=1){
				Integer e = this.neighbors.get(i);
				sb.append("," + e);
			}
			return sb.toString();
		}
		
	}

}
