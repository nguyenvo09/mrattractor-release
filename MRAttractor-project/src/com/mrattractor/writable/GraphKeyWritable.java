package com.mrattractor.writable;

import java.io.DataInput;
import java.io.DataOutput;
import java.io.IOException;

import junit.framework.Assert;

import org.apache.hadoop.io.WritableComparable;


public class GraphKeyWritable implements WritableComparable<GraphKeyWritable>{

	public int[] parts;
	public int no_component;
	
	public GraphKeyWritable() {}
	
	public void set(String graphKey){
		String[] p = graphKey.split(",");
		no_component = p.length;
		//Only G_{ijk} or G_{ij} are acceptable!!!
		Assert.assertTrue("Only G_{ijk} or G_{ij} are acceptable", no_component == 2 || no_component == 3);
		parts = new int[p.length];
		int pre = -1;
		for(int i=0; i<p.length;i++){
			
			parts[i] = Integer.parseInt(p[i]);
			if(pre == -1){
				pre = parts[i];
			}else{
				Assert.assertTrue("Must be increasing!!", parts[i] > pre);
				pre = parts[i];
			}
		}
	}
	
	@Override
	public void readFields(DataInput in) throws IOException {
		// TODO Auto-generated method stub
		no_component = in.readInt();
		parts=new int[no_component];
		for(int i=0; i<no_component;i++){
			parts[i] = in.readInt();
		}
	}

	@Override
	public void write(DataOutput out) throws IOException {
		// TODO Auto-generated method stub
		out.writeInt(no_component);
		for(int i=0; i<no_component; i++){
			out.writeInt(parts[i]);
		}
	}

	@Override
	public int compareTo(GraphKeyWritable target) {
		// TODO Auto-generated method stub
		return this.toString().compareToIgnoreCase(target.toString());		
	}
	@Override
	public String toString() {
		// TODO Auto-generated method stub
		StringBuilder sb = new StringBuilder();
		for(int i=0; i<no_component-1; i++){
			sb.append(parts[i]+" ");
		}
		sb.append(parts[no_component-1]);
		return sb.toString();
	}
	@Override
	public int hashCode() {
		// TODO Auto-generated method stub
		return this.toString().hashCode();
	}
}
