

package com.mrattractor.writable;


import java.io.DataInput;
import java.io.DataOutput;
import java.io.IOException;

import org.apache.hadoop.io.FloatWritable;
import org.apache.hadoop.io.Text;
import org.apache.hadoop.io.Writable;
import org.apache.hadoop.io.WritableComparable;
import org.junit.Assert;

import com.mrattractor.MasterMR;

/**
 * This class is used to save output of Each DynamicDistance Looping!!!
 * <P>
 * @author ben
 */
public class EdgeValueWritable implements Writable{

	////////////////////////////////////
	// public fields
	////////////////////////////////////
	/**This does not mean edge weight!!!!*/
	public double edgeValue;
	public char additional;
	
	////////////////////////////////////
	// private fields
	////////////////////////////////////
	
	private static final int PRIME = 1000003;
	
	////////////////////////////////////
	// public methods
	////////////////////////////////////
	
	public EdgeValueWritable(){}
	
	/**
	 * set fields
	 * @param left left element
	 * @param right	right element
	 */
	public void set(double _edgeValue, char _additional){
		this.edgeValue = _edgeValue;
		this.additional = _additional;
	}
	
	/**
	 * (non-Javadoc)
	 * @see org.apache.hadoop.io.Writable#readFields(java.io.DataInput)
	 */
	public void readFields(DataInput in) throws IOException {
		this.edgeValue = in.readDouble();
		this.additional = in.readChar();
	}

	/**
	 * (non-Javadoc)
	 * @see org.apache.hadoop.io.Writable#write(java.io.DataOutput)
	 */
	public void write(DataOutput out) throws IOException {
		out.writeDouble(edgeValue);
		out.writeChar(additional);
	}

	
	@Override
	public int hashCode() {
		String r = this.toString();
		return r.hashCode();
    }
	@Override
	public String toString() {
		// TODO Auto-generated method stub
		return edgeValue + " " + additional;
		//return String.format("%.8f %s", edgeValue, additional);
	}
	
}
