package com.mrattractor;

import java.io.BufferedReader;
import java.io.IOException;
import java.io.InputStreamReader;
import java.io.PrintWriter;
import java.io.StringWriter;
import java.util.ArrayList;
import java.util.Collections;
import java.util.Dictionary;
import java.util.HashMap;
import java.util.List;
import java.util.PriorityQueue;
import java.util.Map.Entry;

import junit.framework.Assert;

import org.apache.commons.logging.Log;
import org.apache.commons.logging.LogFactory;
import org.apache.hadoop.conf.Configuration;
import org.apache.hadoop.conf.Configured;
import org.apache.hadoop.fs.FileSystem;
import org.apache.hadoop.fs.Path;
import org.apache.hadoop.io.DoubleWritable;
import org.apache.hadoop.io.FloatWritable;
import org.apache.hadoop.io.LongWritable;
import org.apache.hadoop.io.NullWritable;
import org.apache.hadoop.io.Text;
import org.apache.hadoop.mapred.Master;
import org.apache.hadoop.mapreduce.Job;
import org.apache.hadoop.mapreduce.MRJobConfig;
import org.apache.hadoop.mapreduce.Mapper;
import org.apache.hadoop.mapreduce.Partitioner;
import org.apache.hadoop.mapreduce.Reducer;
import org.apache.hadoop.mapreduce.TaskAttemptID;
import org.apache.hadoop.mapreduce.TaskID;
import org.apache.hadoop.mapreduce.lib.input.FileInputFormat;
import org.apache.hadoop.mapreduce.lib.input.SequenceFileInputFormat;
import org.apache.hadoop.mapreduce.lib.input.TextInputFormat;
import org.apache.hadoop.mapreduce.lib.output.FileOutputFormat;
import org.apache.hadoop.mapreduce.lib.output.MultipleOutputs;
import org.apache.hadoop.mapreduce.lib.output.SequenceFileOutputFormat;
import org.apache.hadoop.mapreduce.lib.output.TextOutputFormat;
import org.apache.hadoop.util.Tool;
import org.apache.hadoop.util.ToolRunner;
import org.apache.log4j.Logger;

import com.mrattractor.writable.EdgeValueWritable;
import com.mrattractor.writable.NeighborWritable;
import com.mrattractor.writable.PairWritable;
import com.mrattractor.writable.Settings;
import com.mrattractor.writable.SpecialEdgeTypeWritable;
import com.mrattractor.writable.StarGraphWithPartitionWritable;
import com.mrattractor.writable.StarGraphWritable;
import com.mrattractor.writable.TripleWritable;


public class LoopDynamicInteractionsFasterNoCache extends
		Configured implements Tool {

	static String jobname = "DynamicInteractionNoCaching";

	public static Log logMap = LogFactory.getLog(LoopDynamicInteractionsFasterNoCache.Map.class);
	public static Log logReduce = LogFactory.getLog(LoopDynamicInteractionsFasterNoCache.Reduce.class);

	public static Job globalJob;

	static class Adj implements Comparable<Adj> {
		int lab = 0;
		int deg = 0;
		double dis = 0;

		Adj(int v_, int _deg, double _dis) {
			deg = _deg;
			dis = _dis;
			lab = v_;
		}

		@Override
		public int compareTo(Adj o) {
			// TODO Auto-generated method stub
			if (lab > o.lab) {
				return 1;
			}
			if (lab == o.lab) {
				return 0;
			}
			return -1;
		}
	}

	
	private static boolean isInSet1(int pu, int a, int b, int c) {
		if (pu == a || pu == b || pu == c)
			return true;
		return false;
	}

	/**
	 * Assigning a node to a partition. O(1)
	 * 
	 * @param u
	 * @param no_partitions
	 * @return
	 */
	public static int node2hash(int u, int no_partitions) {
		return u % no_partitions;
	}

	// O(1)
	public static String genkey3num(int a, int b, int c) {
		if (a > c) {
			int t = a;
			a = c;
			c = t;
		}
		if (a > b) {
			int t = a;
			a = b;
			b = t;
		}
		if (b > c) {
			int t = b;
			b = c;
			c = t;
		}
		Assert.assertTrue("Something wrong!!! with sorting three numbers: " + a
				+ " " + b + " " + c, b > a && c > b);
		return String.format("%s,%s,%s", a, b, c);
	}

	/**
	 * Check in how many numbers are equal to each other. (a==b) , (b==c) and
	 * (c==a). If result==0, all are different. If result==1, 2 same, 1
	 * different. If result=3: all are same.
	 * 
	 * @param a
	 * @param b
	 * @param c
	 * @return
	 */
	public static int checkEqual(int a, int b, int c) {
		int x1 = (a == b) ? 1 : 0;
		int x2 = (b == c) ? 1 : 0;
		int x3 = (a == c) ? 1 : 0;
		int s = x1 + x2 + x3;
		Assert.assertTrue("There are only three situation s=0,1 or 3: here: "
				+ s, s == 0 || s == 1 || s == 3);
		return x1 + x2 + x3;
	}

	public static class Map extends Mapper<NullWritable, StarGraphWithPartitionWritable, TripleWritable, StarGraphWritable> {

		int p = 0;
		HashMap<Integer, Integer> mapDeg;
		private HashMap<String, Integer> dictLoadBalance;

		
		@Override
		protected void setup(Context context) throws IOException, InterruptedException {
			super.setup(context);
			p = context.getConfiguration().getInt("ro", 0);
			Configuration conf = context.getConfiguration();
			mapDeg = AttrUtils.readDegMap(conf, MasterMR.degreeFileKey);
//			if(MasterMR.ESTIMATION_WORK_LOAD){
//				dictLoadBalance = readBalacnedFile(conf.get(MasterMR.LOAD_BALANCED_HEADER), conf);
//			}
			
		}

		/**
		 * Mapping function. Input is a line of string stored in @value param.
		 * Format of input is: u du ,<v1 dv1 dis(u,v1)>,<v2 dv2 dis(u,v2)>
		 * 
		 * @Example each line input of map: 1002 3 ,340 9 0.60000000,318 8
		 *          0.55555556,320 7 0.50000000 .Node @1002 has three neighbors
		 *          they are node @340 deg(@340)=9, dis(@1002, @340)=0.6, node @318
		 *          and node @320. This algorithm is based on the idea that
		 *          neighbors of node u are assigned to different partitions.
		 * @Example: with p=20, node @340, @320 have same hash code (e.g.,
		 *           340%20=320%20=0). node @318 has hash code 18. Three
		 *           adjacent neighbors of node @1002 are divided into two
		 *           partitions (340 and 320) and (318 alone). The idea is that
		 *           neighbors of center node @u who have same hash code will
		 *           for sure belong to same set of subgraphs.
		 * @Example: Edge (1002, 340) and edge (1002, 320) will for sure belong
		 *           to similar set of subgraphs. Based on this property, we can
		 *           reduce complexity of mapping function of
		 *           O(|no_partition_of_neighbors
		 *           |*|no_partition_of_neighbors|*p^2).
		 * @Example: With above example: no_partition_of_neighbors=2 (e.g., 20
		 *           and 0). An edge (u,v) can be a main edges to several
		 *           subgraphs and also act as a "rear edge" in other subgraphs.
		 *           When play as a rear edge, (u,v) will belong to subgraphs
		 *           that contain "either" node u or node v. The reason is that
		 *           two endpoints of a rear edge is one is a main vertex of
		 *           subgraph and the other endpoint is rear vertex of that
		 *           subgraph.
		 * @param value
		 *            : is star graph. We just emit it @kaka
		 */
		@Override
		protected void map(NullWritable key, StarGraphWithPartitionWritable value, Context context) throws IOException, InterruptedException {

			try {
				// //////////////////////////////////////////////////////////////////////////////
				// Begining of your code
				// //////////////////////////////////////////////////////////////////////////////

				StarGraphWritable star_graph_writable = new StarGraphWritable();

				int center = value.center; // Integer.parseInt(first[0]);
				int degcenter = mapDeg.get(center); // Integer.parseInt(first[1]);
				
				star_graph_writable.set(center, degcenter, value.neighbors);
				
				
				for(TripleWritable trip : value.tripleSubGraphs){
					if(MasterMR.ESTIMATION_WORK_LOAD){
						if(!dictLoadBalance.containsKey(trip.toString())){
							//System.err.println("Hello work");
							return;
						}
					}
					context.write(trip, star_graph_writable);
				}
				
				// //////////////////////////////////////////////////////////////////////////////
				// End of mapping function //
				// //////////////////////////////////////////////////////////////////////////////

			} catch (Exception e) {
				e.printStackTrace();
				StringWriter sw = new StringWriter();
				PrintWriter pw = new PrintWriter(sw);
				e.printStackTrace(pw);
				logReduce.error(MasterMR.prefix_log + "" + sw.toString());
				globalJob.killJob();
			}

		}
		private HashMap<String, Integer> readBalacnedFile(String balanced_file, Configuration conf)  {

			try {
				Path pt = new Path(balanced_file);
				FileSystem fs = FileSystem.get(conf);
				BufferedReader br = new BufferedReader(new InputStreamReader(fs.open(pt)));

				dictLoadBalance = new HashMap<String, Integer>();
				String line = "";
				while ((line = br.readLine()) != null) {
					String[] args = line.split("\\s+");
					String graphKey = args[0] + " " + args[1] + " " + args[2];
					int reducer = Integer.parseInt(args[3]);
					
					dictLoadBalance.put(graphKey, reducer);
				}
				return dictLoadBalance;
			} catch (Exception e) {
				e.printStackTrace();
			} finally {

			}
			return null;
		}

	}

	/**
	 * Computing CI(u,v).
	 * 
	 * @param u
	 * @param v
	 * @param c
	 * @param du
	 * @param dv
	 * @param dis_u_v
	 * @param dis_u_c
	 * @param dis_v_c
	 * @param p
	 * @return
	 */
	private static double computeCI(int u, int v, int c, int du, int dv,
			double dis_u_v, double dis_u_c, double dis_v_c, int p) {
		Assert.assertTrue("This edge is already converged!!!", dis_u_v > 0
				&& dis_u_v < 1);
		double w1 = 1 - dis_u_c;
		double w2 = 1 - dis_v_c;
		double CI = -w2 * Math.sin(w1) / du - w1 * Math.sin(w2) / dv;
		int pu = node2hash(u, p);
		int pv = node2hash(v, p);
		int pc = node2hash(c, p);
		int repeatCount = 1;
		int res = checkEqual(pu, pv, pc);
		if (res == 3) {
			// Three vertices are in a same partition. => There are
			// (p-1)*(p-2)/2 subgraphs contain this triangle.
			repeatCount = (p - 1) * (p - 2) / 2;
		} else if (res == 1) {
			// Two vertices are in same partition, another is in other
			// partition.
			repeatCount = p - 2;
		} else if (res == 0) {
			// Three vertices are in three different partitions.
			repeatCount = 1;
		}

		CI = CI * 1.0 / repeatCount;
		return CI;
	}

	

	/**
	 * Compute exclusive effect of node @u on edge (@middle, @v). We have
	 * \wedge(u, middle, v). Now node u affect edge (middle, v). Equation:
	 * \rho(u,v) * sin(1 - dis(u, middle)) / deg(middle)
	 * 
	 * @param u
	 * @param v
	 * @param middle
	 * @param dis_u_middle
	 * @param dis_v_middle
	 * @param degmiddle
	 * @param p
	 * @param adjListDict
	 * @param dictSumWeight
	 * @param lambda
	 * @return
	 * @throws Exception
	 */
	private static double computeEIWithCache(int u, int v, int middle,
			double dis_u_middle, double dis_v_middle, int degmiddle, int p,
			HashMap<Integer, ArrayList<NeighborWritable>> adjListDict,
			HashMap<Integer, Double> dictSumWeight, double lambda,
			int[] graphKey) throws Exception {

		// /////////////////////////////////////////////////////////////////////////////////////
		// BEGIN OF MY CODE: Compute common weight of virtual edge (u,v).
		// ////////////////////////////////////////////////////////////////////////////////////
		// Assert.assertTrue("Two endpoints of virtual edge must be different",
		// u!=v);
		if (u == v) {
			return 0.0;
		}

		double vartheta_uv = 0;
		// String key_virtual = GenKey(u, v);
		// if (dictWedge.containsKey(key_virtual)) {
		//boolean isHitWedge = dictWedge.checkContainWedge(u, v);

		
		ArrayList<NeighborWritable> neighborsU = adjListDict.get(u);
		ArrayList<NeighborWritable> neighborsV = adjListDict.get(v);
		int i = 0, j = 0;
		int m = neighborsU.size();
		int n = neighborsV.size();
		double sum_common_weight = 0.0;
		int commonMainNode = 0;

		while (i < m && j < n) {
			NeighborWritable first = neighborsU.get(i), second = neighborsV
					.get(j);
			// We need to check a rear vertex:

			if (first.lab < second.lab) {

				i++;
			} else if (second.lab < first.lab) {

				j++;
			} else {
				sum_common_weight += 1 - first.dis + 1 - second.dis;
				i++;
				j++;
				if (isInSet1(node2hash(first.lab, p), graphKey[0],
						graphKey[1], graphKey[2])) {
					commonMainNode++;
				}
			}
		}

		vartheta_uv = sum_common_weight / (dictSumWeight.get(u) + dictSumWeight.get(v));

		// System.out.println(String.format("Similarity of virtual edge: %s %s %.8f",
		// u, v, vartheta_uv));
		double rho_uv = vartheta_uv; // if >= \lambda
		if (vartheta_uv < lambda) {
			rho_uv = vartheta_uv - lambda;
		}

		double EI = -rho_uv * Math.sin(1 - dis_u_middle) / degmiddle;

		int pu = node2hash(u, p);
		int pv = node2hash(v, p);
		int pc = node2hash(middle, p);
		int repeatCount = 1;
		int res = checkEqual(pu, pv, pc);
		if (res == 3) {
			// Three vertices are in a same partition. => There are
			// (p-1)*(p-2)/2 subgraphs contain this triangle.
			repeatCount = (p - 1) * (p - 2) / 2;
		} else if (res == 1) {
			// Two vertices are in same partition, another is in other
			// partition.
			repeatCount = p - 2;
		} else if (res == 0) {
			// Three vertices are in three different partitions.
			repeatCount = 1;
		}
		// We need to scale them down.
		EI /= repeatCount;

		return EI;

	}

	/**
	 * Compute DI of non-converged edge (u,v) with scale down.
	 * 
	 * @param u
	 * @param v
	 * @param p
	 *            : No partitions.
	 * @param duv
	 *            : current distance.
	 * @param degU
	 *            in original graph G(V, E)
	 * @param degV
	 * @return
	 */
	private static double computeDI(int u, int v, int p, double duv, int degU,
			int degV) {
		Assert.assertTrue("Number of partion must be >= 3", p >= 3);
		Assert.assertTrue(
				String.format("Distance of (%s, %s) must be in (0, 1)", u, v),
				duv < 1 && duv > 0);
		double DI = -Math.sin(1 - duv) / degU - Math.sin(1 - duv) / degV;
		if (node2hash(u, p) == node2hash(v, p)) {
			// There are (p-1)*(p-2) subgraphs G_{ijk} contains (u,v). i,j,k \in
			// [0,p-1].
			int scale = (p - 1) * (p - 2) / 2;
			DI /= scale;
		} else {
			// There are p-2 subgraphs G_{ijk} contains (u,v) . i,j,k \in
			// [0,p-1].
			int scale = p - 2;
			DI /= scale;
		}
		return DI;
	}

	/**
	 * Compute DI, CI, EI for edge (ulab, vlab) in subgraph @graphKey.
	 * 
	 * @param ulab
	 * @param vlab
	 * @param degU
	 *            : deg(ulab) in original graph G(V, E)
	 * @param degV
	 *            : deg(vlab) in original graph G(V, E)
	 * @param adjListMainDict
	 *            : Adjacent list of every vertex in subgraph G_{ijk}(V_{ijk},
	 *            E_{ijk})
	 * @param dictSumWeight
	 *            : Sum weight of all vertex in V_{ijk}
	 * @param p
	 *            : number of partitions.
	 * @param duv
	 *            : dis(ulab, vlab), distance of edge (ulab, vlab).
	 * @param graphKey
	 *            : Graph key (e.g., i,j,k)
	 * @param mout
	 *            : object to collect key-value pair.
	 * @throws Exception
	 */
	private static void UnionIntersection(
			int ulab,
			int vlab,
			int degU,
			int degV,
			HashMap<Integer, ArrayList<NeighborWritable>> adjListMainDict,
			HashMap<Integer, ArrayList<NeighborWritable>> adjListDictForExclusive,
			HashMap<Integer, Double> dictSumWeight, int p, double duv,
			String graphKey, int[] graphKeyPartitions, double lambda,
			MultipleOutputs<SpecialEdgeTypeWritable, NullWritable> mout) throws Exception {

		if (duv < 0 || duv > 1) {
			return; // Checking again to make sure.
		}

		List<NeighborWritable> neighborsU = adjListMainDict.get(ulab);
		List<NeighborWritable> neighborsV = adjListMainDict.get(vlab);
		int i = 0, j = 0;
		int m = neighborsU.size();
		int n = neighborsV.size();
		double sumCI = 0;
		double sumEI = 0;
		// Compute DI, remember to scale down.
		double DI = computeDI(ulab, vlab, p, duv, degU, degV);

		while (i < m && j < n) {
			NeighborWritable first = neighborsU.get(i), second = neighborsV
					.get(j);

			// We need to check a rear vertex:
			boolean main_edge_first = isInSet1(node2hash(first.lab, p),
					graphKeyPartitions[0], graphKeyPartitions[1],
					graphKeyPartitions[2]);
			boolean main_edge_second = isInSet1(node2hash(second.lab, p),
					graphKeyPartitions[0], graphKeyPartitions[1],
					graphKeyPartitions[2]);

			Assert.assertTrue("There is a rear edge in this case!!!",
					main_edge_first && main_edge_second);

			if (first.lab < second.lab) {

				sumEI += computeEIWithCache(first.lab, vlab, ulab, first.dis,
						duv, degU, p, adjListDictForExclusive, dictSumWeight,
						lambda, graphKeyPartitions);
				i++;
			} else if (second.lab < first.lab) {

				sumEI += computeEIWithCache(second.lab, ulab, vlab, second.dis,
						duv, degV, p, adjListDictForExclusive, dictSumWeight,
						lambda, graphKeyPartitions);
				j++;
			} else {

				int common_node = first.lab;
				sumCI += computeCI(ulab, vlab, common_node, degU, degV, duv,
						first.dis, second.dis, p);
				i++;
				j++;
			}
		}

		while (i < m) {
			// EI
			NeighborWritable first = neighborsU.get(i); // first u v
			sumEI += computeEIWithCache(first.lab, vlab, ulab, first.dis, duv,
					degU, p, adjListDictForExclusive, dictSumWeight, lambda,
					graphKeyPartitions);
			i++;
		}
		while (j < n) {
			// EI
			NeighborWritable second = neighborsV.get(j); // u v second
			sumEI += computeEIWithCache(second.lab, ulab, vlab, second.dis,
					duv, degV, p, adjListDictForExclusive, dictSumWeight,
					lambda, graphKeyPartitions);
			j++;
		}

		double delta_ulab_vlab = DI + sumCI + sumEI;
		// String edgeKey = GenKey(ulab, vlab);
		/**
		 * We will re-use edgeKey pairWritable to avoid allocation of large
		 * number of objects
		 */
		
		NullWritable _null =  NullWritable.get();
		
		SpecialEdgeTypeWritable spec = new SpecialEdgeTypeWritable();
		
		
		if (MasterMR.DEBUG) {
			// For debugging purposes only.
			spec.init(Settings.D_TYPE, ulab, vlab, DI, -1, null, -1, null);
			mout.write("attr", spec, _null, "debug/debug");

			spec.init(Settings.C_TYPE, ulab, vlab, sumCI, -1, null, -1, null);
			mout.write("attr", spec, _null, "debug/debug");
			
			spec.init(Settings.E_TYPE, ulab, vlab, sumEI, -1, null, -1, null);
			mout.write("attr", spec, _null, "debug/debug");
		}

		spec.init(Settings.INTERACTION_TYPE, ulab, vlab, delta_ulab_vlab, -1, null, -1, null);
		mout.write("attr", spec, _null, "delta_dis/delta_dis");

	}

	public static class Reduce extends Reducer<TripleWritable, StarGraphWritable, SpecialEdgeTypeWritable, NullWritable> {

		MultipleOutputs<SpecialEdgeTypeWritable, NullWritable> mout;
		// HashMap<String, Double> mapEdge = null;
		// HashMap<String, Double> mapSumW = null;
		double lambda = 0.5;
		int p = 0;

		TaskAttemptID tId;
		TaskID taskId;
		int reducerNumber = -1;
		// HashMap<Integer, HashMap<Integer, Double> > dictVirtualEdge = null;
		HashMap<Integer, Integer> mapDeg;

		@Override
		protected void setup(Context context) throws IOException, InterruptedException {
			mout = new MultipleOutputs<SpecialEdgeTypeWritable, NullWritable>(context);
			// mapEdge = getMapEdge(context.getConfiguration());
			// mapSumW = readSumW(context.getConfiguration());
			lambda = context.getConfiguration().getDouble("lambda", 0.5);
			p = context.getConfiguration().getInt("ro", 0);
			tId = context.getTaskAttemptID();
			taskId = tId.getTaskID();
			reducerNumber = taskId.getId();

			Configuration conf = context.getConfiguration();
			// conf.set("deg_file", MasterMR.degreeFile);
			mapDeg = AttrUtils.readDegMap(conf, MasterMR.degreeFileKey);
		}

		/**
		 * Each reducer will handle a subgraph G_{ijk}. This subgraph contains
		 * rear edges and main edges. In a subgraph G_{ijk} of main edges and
		 * rear edges: The rear vertex has hashcode not equal to i or j or k.
		 * The reason is that they are just additional edges emitted to compute
		 * exclusive interactions From above argument we can see that: (1) The
		 * edges with two main points equal to (i or j or k) are "main edge".
		 * (2) The edges with one main point and one rear point are rear-edge.
		 * (3) There is no edges with only rear-points.
		 */
		@Override
		protected void reduce(TripleWritable subgraph_key, Iterable<StarGraphWritable> star_graphs, Context context)
				throws IOException, InterruptedException {

			try {

				// /////////////////////////////////////////////////////////////////////////////////////////
				// /// Beginning of using adjacency list. OUR CODE IS HERE
				// /////////////////////////////////////////////////////////////////////////////////////////
				//TripleWritable trip = new TripleWritable();
				//trip.set(subgraph_key.toString());
				int[] components = { subgraph_key.left, subgraph_key.mid, subgraph_key.right };
				
				/**
				 * This dictionary will contains \textbf{original} adjacent
				 * neighbors of all main vertices in subgraph G_{ijk}
				 **/
				HashMap<Integer, ArrayList<NeighborWritable>> adjListDictForExclusive = new HashMap<Integer, ArrayList<NeighborWritable>>();

				/**
				 * This dictionary will only contain adjacent \textbf{main}
				 * neighbors of main vertices in subgrah G_{ijk}
				 **/
				HashMap<Integer, ArrayList<NeighborWritable>> adjListDictMain = new HashMap<Integer, ArrayList<NeighborWritable>>();

				/**
				 * List of edges, we will visit each main edge one time.
				 * Complexity is only O(|no_main_edges_in_G_{ijk}|)
				 **/
				ArrayList<Edge> listEdges = new ArrayList<Edge>();

				HashMap<Integer, Double> dictSumWeight = new HashMap<Integer, Double>();
				
//				if(MasterMR.DEBUG){
//					System.out.println("Triple subgraph: " + subgraph_key.toString());
//				}
				
				/** For skewness issue investigation only!!! */
				int main_edges = 0, rear_edges = 0, sumDegree = 0;
				while (star_graphs.iterator().hasNext()) {
					// String star_graph =
					// star_graphs.iterator().next().toString();
					StarGraphWritable star = star_graphs.iterator().next();
					// String[] star = star_graph.split(",");

					// Info of center node of star graph.
					// String[] first = star[0].split("\\s+");
					// Assert.assertTrue("Missing info in first element star graph, actual "
					// + first.length, first.length == 2);
					// Node @center must be a main vertex.
					// int center = Integer.parseInt(first[0]);
					int center = star.center;

					Assert.assertTrue(String.format("Node %s must be in subgraph %s with p=%s", center, subgraph_key, p),
							isInSet1(node2hash(center, p), components[0], components[1], components[2]));

					// int degcenter = Integer.parseInt(first[1]);
					int degcenter = star.degCenter;

					if (MasterMR.CheckDegree) {
						Assert.assertTrue(
								"Number of neighbors are not matched with center degree",
								star.neighbors.size() == degcenter);
					}
					sumDegree += degcenter;

					double sumWeight = 0.0;
					// Looping through neighbors of center, index starting at 1
					// instead of 0.
					// Complexity: O(sum degree of center of all star-graphs).
					// for (int i = 1; i < star.length; i++) {
					for (NeighborWritable neighbor_info : star.neighbors) {
						// String[] neighbor_info = star[i].split("\\s+");
						// int neighbor = Integer.parseInt(neighbor_info[0]);
						int neighbor = neighbor_info.lab;
						// degree of neighbor in original graph G(V, E).
						// int degNeighbor = neighbor_info.deg;

						// distance between @center and @neighbor
						double dis = neighbor_info.dis;

						// This is similarity. Be careful.
						sumWeight += (1.0 - dis);
						NeighborWritable adj = neighbor_info;

						/**
						 * Building the dictionary of main edges in subgraph
						 * G_{ijk}. Note that we must only add main vertices
						 **/
						if (isInSet1(node2hash(neighbor, p), components[0], components[1], components[2])) {
							/**
							 * This conditions to avoid duplicatedly adding
							 * edges.
							 */
							if (center > neighbor) {
								if (dis < 1 && dis > 0) {
									Edge e = new Edge(center, neighbor, dis);
									listEdges.add(e);
								}
								main_edges += 1;
//								if(MasterMR.DEBUG){
//									System.out.println("Main Edge: " + center + " " + neighbor);
//								}
							}

							if (adjListDictMain.containsKey(center)) {
								ArrayList<NeighborWritable> neighbors = adjListDictMain.get(center);
								neighbors.add(adj);
								adjListDictMain.put(center, neighbors);
							} else {
								ArrayList<NeighborWritable> neighbors = new ArrayList<NeighborWritable>();
								neighbors.add(adj);
								adjListDictMain.put(center, neighbors);

							}
							
						} else {
							rear_edges += 1;
//							if(MasterMR.DEBUG){
//								System.out.println("Rear Edge: " + center + " " + neighbor);
//							}
						}

						/**
						 * Building dicitonary of adjacent neighbors of very
						 * main vertex in subgraph G_{ijk} for computing
						 * exclusinve interactions.
						 **/
						if (adjListDictForExclusive.containsKey(center)) {
							ArrayList<NeighborWritable> neighbors = adjListDictForExclusive.get(center);
							neighbors.add(adj);
							adjListDictForExclusive.put(center, neighbors);
						} else {
							ArrayList<NeighborWritable> neighbors = new ArrayList<NeighborWritable>();
							neighbors.add(adj);
							adjListDictForExclusive.put(center, neighbors);

						}

					}
					Assert.assertTrue("Duplicated star graph in a subgraph!!!. Something wrong.", !dictSumWeight.containsKey(center));
					dictSumWeight.put(center, sumWeight);
				}

				/**
				 * * This initialization is weird!!!. But I want to save the
				 * number of new writable to improve speed
				 * https://blog.cloudera.
				 * com/blog/2009/12/7-tips-for-improving-mapreduce-performance/
				 */
				//PairWritable edgeKeyWritable = new PairWritable();
				/** Re-use wriable value */
				//EdgeValueWritable edgeValueWriable = new EdgeValueWritable();
				// O(|main_edges|)
				for (Edge e : listEdges) {
					// each edge is a main edge.

					Assert.assertTrue(
							"Something wrong here: not main vertex",
							isInSet1(node2hash(e.u, p), components[0],
									components[1], components[2]));
					Assert.assertTrue(
							"Something wrong here: not main vertex",
							isInSet1(node2hash(e.v, p), components[0],
									components[1], components[2]));

					if (e.dis < 1 && e.dis > 0) {
						int degU = mapDeg.get(e.u);
						int degV = mapDeg.get(e.v);
						UnionIntersection(e.u, e.v, degU, degV,
								adjListDictMain, adjListDictForExclusive,
								dictSumWeight, p, e.dis,
								subgraph_key.toString(), components, lambda,
								mout);
					}

				}

				// //////////////////////////////////////////////////////////////////////////////////////////////
				// / END OF YOUR CODE
				// //////////////////////////////////////////////////////////////////////////////////////////////
			} catch (Exception e) {
				e.printStackTrace();
				StringWriter sw = new StringWriter();
				PrintWriter pw = new PrintWriter(sw);
				e.printStackTrace(pw);
				logReduce.error(MasterMR.prefix_log + "" + sw.toString());
				globalJob.killJob();
			} catch (AssertionError e) {
				StringWriter sw = new StringWriter();
				PrintWriter pw = new PrintWriter(sw);
				e.printStackTrace(pw);
				logReduce.error(MasterMR.prefix_log + "" + sw.toString());
				globalJob.killJob();
			}

		}

		@Override
		protected void cleanup(Context context) throws IOException,
				InterruptedException {
			super.cleanup(context);
			mout.close();
		}
	}

	@Override
	public int run(String[] args) throws Exception {
		Assert.assertTrue("Number of arguments must be 10 "+args.length, args.length == 10);
		//int cacheSize = Integer.parseInt(args[5]);
		//Assert.assertTrue("Cache size must be larger than 0", cacheSize > 0);
		// TODO Auto-generated method stuborg.apache.hadoop.mapreduce.Job
		// System.out.println("Dynamic Interactions");
		Job job = Job.getInstance(getConf());
		job.setJarByClass(LoopDynamicInteractionsFasterNoCache.class);
		globalJob = job;
		job.setJobName(jobname);

		job.setMapOutputKeyClass(TripleWritable.class);
		job.setMapOutputValueClass(StarGraphWritable.class);

		// job.setNumMapTasks(5); // 5 mappers
//		if (MasterMR.ESTIMATION_WORK_LOAD) {
//			job.setPartitionerClass(KeyPartitioner.class);
//		}
		job.setOutputKeyClass(SpecialEdgeTypeWritable.class);
		job.setOutputValueClass(NullWritable.class);

		job.setMapperClass(Map.class);
		job.setReducerClass(Reduce.class);

		job.setInputFormatClass(SequenceFileInputFormat.class);
		job.setOutputFormatClass(SequenceFileOutputFormat.class);

		String input_files = args[0];
		String ouput_files = args[1];

		job.getConfiguration().setInt("ro", Integer.parseInt(args[2]));
		job.getConfiguration().setDouble("lambda", Double.parseDouble(args[3]));
		jobname = jobname + " Loop- " + args[4]; // For information about number
													// of loops currently.

		job.setJobName(jobname);

		/**
		 * This cache acts the purpose to store similarity of virtual edges who
		 * are frequently used.
		 */
		job.getConfiguration().setInt("cacheSize", Integer.parseInt(args[5]));
		job.getConfiguration().set(MRJobConfig.REDUCE_MEMORY_MB, args[6]);
		job.getConfiguration().set(MRJobConfig.MAP_MEMORY_MB, "3072");
		job.setNumReduceTasks(Integer.parseInt(args[7])); // 2 reducers
		job.getConfiguration().set(MasterMR.degreeFileKey, args[8]);
		job.getConfiguration().set(MasterMR.LOAD_BALANCED_HEADER, args[9]);
		FileSystem fs = FileSystem.get(job.getConfiguration());

		fs.delete(new Path(ouput_files), true);
		FileInputFormat.setInputPaths(job, new Path[] { new Path(input_files) });
		FileOutputFormat.setOutputPath(job, new Path(ouput_files));

		//MultipleOutputs.addNamedOutput(job, "attr", TextOutputFormat.class, Text.class, Text.class);
		MultipleOutputs.addNamedOutput(job, "attr", SequenceFileOutputFormat.class, SpecialEdgeTypeWritable.class, NullWritable.class);
		try {
			job.waitForCompletion(true);
		} catch (Exception e) {
			e.printStackTrace();
		}
		return 1;
	}

}
