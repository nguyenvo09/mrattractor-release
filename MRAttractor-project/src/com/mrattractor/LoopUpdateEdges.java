package com.mrattractor;

import java.io.BufferedReader;
import java.io.IOException;
import java.io.InputStreamReader;
import java.io.PrintWriter;
import java.io.StringWriter;
import java.util.ArrayList;
import java.util.BitSet;
import java.util.HashMap;
import java.util.List;

import junit.framework.Assert;

import org.apache.commons.logging.Log;
import org.apache.commons.logging.LogFactory;
import org.apache.hadoop.conf.Configuration;
import org.apache.hadoop.conf.Configured;
import org.apache.hadoop.fs.FileSystem;
import org.apache.hadoop.fs.Path;
import org.apache.hadoop.io.DoubleWritable;
import org.apache.hadoop.io.FloatWritable;
import org.apache.hadoop.io.LongWritable;
import org.apache.hadoop.io.NullWritable;
import org.apache.hadoop.io.Text;
import org.apache.hadoop.mapreduce.Job;
import org.apache.hadoop.mapreduce.Mapper;
import org.apache.hadoop.mapreduce.Reducer;
import org.apache.hadoop.mapreduce.lib.input.FileInputFormat;
import org.apache.hadoop.mapreduce.lib.input.SequenceFileInputFormat;
import org.apache.hadoop.mapreduce.lib.input.TextInputFormat;
import org.apache.hadoop.mapreduce.lib.output.FileOutputFormat;
import org.apache.hadoop.mapreduce.lib.output.MultipleOutputs;
import org.apache.hadoop.mapreduce.lib.output.SequenceFileOutputFormat;
import org.apache.hadoop.mapreduce.lib.output.TextOutputFormat;
import org.apache.hadoop.util.Tool;
import org.apache.hadoop.util.ToolRunner;

import com.mrattractor.writable.PairWritable;
import com.mrattractor.writable.Settings;
import com.mrattractor.writable.SpecialEdgeTypeWritable;
import com.mrattractor.writable.SpecialEdgeValueWritable;


/**
 * @author: ben
 */
public class LoopUpdateEdges extends Configured implements Tool {

	static String jobname = "Update Edge";
	public static Log logMap = LogFactory.getLog(LoopUpdateEdges.Map.class);
	public static Log logReduce = LogFactory.getLog(LoopUpdateEdges.Reduce.class);

	public static Job globalJob;
	

	/* Map function. Input: Line text, out: (key: Text, value: Text) */
	public static class Map extends Mapper<SpecialEdgeTypeWritable, NullWritable, PairWritable, SpecialEdgeValueWritable> {
		// HashMap<String, Pair> mapDeg = null;
		// HashMap<String, Double> mapEdge = null;
		@Override
		protected void setup(Context context)
				throws IOException, InterruptedException {
			super.setup(context);
		}

		@Override
		protected void map(SpecialEdgeTypeWritable key, NullWritable value, Context context) throws IOException, InterruptedException {
			try{
				Assert.assertTrue(key.type == Settings.C_TYPE || key.type == Settings.D_TYPE || key.type == Settings.E_TYPE 
						|| key.type == Settings.EDGE_TYPE || key.type == Settings.INTERACTION_TYPE || key.type == Settings.SLIDING);
//				String s = value.toString();
//				if(s.contains("D") || s.contains("E") || s.contains("C")){
//					int x=0;
//					x+=1;
//				}
//				String[] args = s.split("\\s+");
				/**
				 * There are 2 mandatory inputs and 3 optional inputs. 
				 * (1) Edges with distance before updated in format: u v dis(u,v) 
				 * (2) Delta of all non-converged edge (u,v). With converged-edge (u,v): there is no delta emitted. Format: u v delta(u,v)
				 * Optional inputs for debugging purposes only:
				 * (1) u v DI(u,v) "D"
				 * (2) u v CI(u,v) "C"
				 * (3) u v EI(u,v) "E"
				 */
				//Assert.assertTrue("Input must have length 4 or 3, original input: " + s, args.length == 3 || args.length == 4);
				//String e = GenKey(Integer.parseInt(args[0]), Integer.parseInt(args[1]));
//				String v = "";
//				int cnt = 0;
//				for (int i = 2; i < args.length; i++) {
//					v += args[i] + " ";
//					cnt+=1;
//				}
				//Assert.assertTrue("Output must have 2 or 1 values: "+v,  cnt == 1 || cnt == 2);
				PairWritable edge = new PairWritable();
				edge.set(key.center, key.target);
				
				SpecialEdgeValueWritable spec = new SpecialEdgeValueWritable();
				if(key.type == Settings.SLIDING){
					spec.init(key.type, key.weight, key.noBits, key.sliding);
				}else{
					spec.init(key.type, key.weight, -1, null);
				}
				context.write(edge, spec);
			}catch(Exception e){
				e.printStackTrace();
				StringWriter sw = new StringWriter();
				PrintWriter pw = new PrintWriter(sw);
				e.printStackTrace(pw);
				logMap.error(MasterMR.prefix_log + "" + sw.toString());
				globalJob.killJob();
				
			}catch(AssertionError e){
				e.printStackTrace();
				StringWriter sw = new StringWriter();
				PrintWriter pw = new PrintWriter(sw);
				e.printStackTrace(pw);
				logMap.error(MasterMR.prefix_log + "" + sw.toString());
				globalJob.killJob();
			}
		}
	}
	/**
	 * Update DeltaWindow, 
	 * @param delta
	 * @param cLoopRound: the index of current iteration. 
	 * Please be consistent. O is starting or 1 is statring
	 * @param windowSize: default 10
	 * @param deltaWindow: this bit set always has 
	 * @param threshold: from 0 to 1. 
	 * @return
	 * @throws InterruptedException 
	 * @throws IOException 
	 */
	private static double updateDeltaWindow(PairWritable key, 
			double delta, int cLoopRound, 
			int windowSize, BitSet deltaWindow, 
			double threshold, 
			MultipleOutputs mout) throws IOException, InterruptedException{
		
		//cLoopRound initialized to zero.
		int index = cLoopRound % windowSize;
		//neu ko co deltawindows. 
		if(delta < 0){
			deltaWindow.clear(index);
			//giam di ne. 
		}else{
			//tang len ne
			deltaWindow.set(index);
		}
		double returnedDelta = delta;
		if(cLoopRound >= (windowSize-1)){
			//We should write down the whole list bitset.
			/*
			String s = " S ";
			for(int i=0; i<windowSize; i++){
				if(deltaWindow.get(i))
					s += " " + 1;
				else
					s += " " + 0;
			}
			*/
			SpecialEdgeTypeWritable spec = new SpecialEdgeTypeWritable();
			spec.init(Settings.SLIDING, key.left, key.right, -1, -1, null, windowSize, deltaWindow);
			//mout.write("updateEdge", key, new Text(s), "sliding/sliding");
			mout.write("updateEdge", spec, NullWritable.get(), "sliding/sliding");
			//Windows is full now
			if(deltaWindow.get(index)){
				int iSameSize = deltaWindow.cardinality();
				if(iSameSize > threshold * windowSize){
					returnedDelta =  2;
				}
			}else{
				int iSameSize = windowSize - deltaWindow.cardinality();
				if(iSameSize > threshold * windowSize){
					returnedDelta =  -2;
				}
			}
		}else{
			//We should write from 0 to currentIndex of (bitset)
			/*
			String s = "S ";
			for(int i=0; i<cLoopRound+1; i++){
				if(deltaWindow.get(i))
					s += " " + 1;
				else
					s += " " + 0;
			}
			*/
			SpecialEdgeTypeWritable spec = new SpecialEdgeTypeWritable();
			spec.init(Settings.SLIDING, key.left, key.right, -1, -1, null, cLoopRound+1, deltaWindow);
			//mout.write("updateEdge", key, new Text(s), "sliding/sliding");
			mout.write("updateEdge", spec, NullWritable.get(), "sliding/sliding");
		}
		return returnedDelta;
//		return delta;
		
	}
	
	public static class Combiner extends Reducer<PairWritable, SpecialEdgeValueWritable, PairWritable, SpecialEdgeValueWritable> {
		@Override
		protected void setup(Context context) throws IOException, InterruptedException {
			// TODO Auto-generated method stub
			super.setup(context);
		}
		@Override
		protected void reduce(PairWritable key, Iterable<SpecialEdgeValueWritable> values, Context context)
				throws IOException, InterruptedException {
			

			double weight = 0;
			boolean containInteractions = false;
			while (values.iterator().hasNext()) {
				SpecialEdgeValueWritable s = values.iterator().next();
				if(s.type == Settings.INTERACTION_TYPE){
					weight += s.value;
					containInteractions = true;
				}else{
					context.write(key, s);
				}
			}
			if(containInteractions){
				SpecialEdgeValueWritable spec = new SpecialEdgeValueWritable();
				spec.init(Settings.INTERACTION_TYPE, weight, -1, null);
				context.write(key, spec);
			}
		}
		@Override
		protected void cleanup(Context context) throws IOException, InterruptedException {
			// TODO Auto-generated method stub
			super.cleanup(context);
		}
	}
	
	public static class Reduce extends Reducer<PairWritable, SpecialEdgeValueWritable, SpecialEdgeTypeWritable, NullWritable> {

		MultipleOutputs mout;
		// HashMap<String, Double> mapEdge = null;
//		HashMap<String, Pair> mapV = null;
		double PRECISE = 0.0000001;
		String round = "";
		double threshold = -1;
		int windowSize = -1;
		int loopindex = 0;

		@Override
		protected void setup(Context context)
				throws IOException, InterruptedException {
			mout = new MultipleOutputs(context);
			round = context.getConfiguration().get("round");
			try{
				threshold = Double.parseDouble(context.getConfiguration().get("threshold"));
				windowSize = Integer.parseInt(context.getConfiguration().get("windowSize"));
				loopindex = Integer.parseInt(round);
			}catch(Exception e){
				e.printStackTrace();
			}
			// mapEdge = getMapEdge(context.getConfiguration());
			// mapV = getMapV(context.getConfiguration());
		}

		@Override
		protected void reduce(PairWritable key, Iterable<SpecialEdgeValueWritable> values, Context context)
				throws IOException, InterruptedException {

			try {
				//////////////////////////////////////////////////////////////////
				//// BEGIN OF YOUR CODE
				//////////////////////////////////////////////////////////////////
				int u = key.left;
				int v = key.right;

				boolean usingSlidingWindows = false;
				if(windowSize > 0){
					usingSlidingWindows = true;
				}
				
				double deltaDI = 0;
				double deltaEI = 0;
				double deltaCI = 0;
				double delta_t = 0;
//				int deltaCount=0;
//				String dis_t = "dis";
				double disuv=-1;
				boolean existDeltat = false;
				/**Only for delta window*/

				BitSet bDeltaWindow = new BitSet(32); //init a set of 32 bits. 
				
				while (values.iterator().hasNext()) {
					//String s = values.iterator().next().toString();
					SpecialEdgeValueWritable s = values.iterator().next();
					
					switch (s.type) {
					case Settings.EDGE_TYPE:
					{
						disuv = s.value;
						break;
					}
					case Settings.SLIDING:
					{
						/**init @bDeltaWindow*/
						for(int i=0; i<=s.noBits; i+=1){
							if(s.sliding.get(i)){
								bDeltaWindow.set(i);
							}
						}
						continue;
					}
					case Settings.D_TYPE:
					{
						deltaDI += s.value;
						break;
					}
					case Settings.C_TYPE:
					{
						deltaCI += s.value;
						break;
					}
					case Settings.E_TYPE:
					{
						deltaEI += s.value;
						break;
					}
					case Settings.INTERACTION_TYPE:
					{
						delta_t += s.value;
						existDeltat = true;
						break;
					}
					default:
						break;
					}
				}
				/////////////////////////////////////////////////////////////////////////////
				// CODE VALIDATION
				/////////////////////////////////////////////////////////////////////////////
				if(MasterMR.DEBUG){
					//If not DEBUG, deltaDI, deltaCI and deltaEI must be 0
				    Assert.assertTrue("Sum of DI, CI, EI must equal to delta_t", Math.abs(delta_t - (deltaDI+deltaCI+deltaEI)) <= 1e-5);
				}else{
					Assert.assertTrue("DI, CI, EI must be zero if not debugged.", Math.abs(deltaDI) < 1e-5 && Math.abs(deltaCI) < 1e-5 && Math.abs(deltaEI) < 1e-5);
				}
//				Assert.assertTrue("deg(u), deg(v) and dis(u,v) must be always initialized!!", degu != -1 && degv != -1 && !dis_t.equalsIgnoreCase("dis"));
//				Assert.assertTrue("Current distance of edge (u,v) is not initialized", dis_t.equalsIgnoreCase("dis"));
//				disuv = Double.parseDouble(dis_t);
				
				if(disuv < 0){
					return;
				}
				if(disuv < 1 && disuv > 0){
					Assert.assertTrue("With non-converged edge (%s,%s), there must be delta to update!!!", existDeltat == true);
					if(usingSlidingWindows){
						delta_t = updateDeltaWindow(key, delta_t, loopindex, windowSize, bDeltaWindow, threshold, mout);
					}
				}else{
					Assert.assertTrue("With converged edge (%s, %s), there is no delta emitted!!!", existDeltat == false);
				}
				
				Double d_t_1 = disuv + delta_t;
//				Double d_t_1 =  delta_t;
				if(d_t_1 > 1){
					d_t_1 = 1.0;
				}
				if(d_t_1 < 0){
					d_t_1 = 0.0;
				}
				
				if (MasterMR.DEBUG) {
					String test = String.format(",oldDis: %.8f, newDis: %.8f, DI: %.8f, EI: %.8f, CI: %.8f", disuv, d_t_1, deltaDI, deltaEI, deltaCI);
					mout.write("testing", key, new Text(test), "test/test_DI_CI_EI_" + round);
					
				}
				
				if(d_t_1 < 1 && d_t_1 > 0){
					mout.write("signal", "Flag", new Text(""), "flag" + "/" + "flag");
				}
				//String c = String.format("%.8f G", d_t_1);
				SpecialEdgeTypeWritable spec = new SpecialEdgeTypeWritable();
				spec.init(Settings.EDGE_TYPE, key.left, key.right, d_t_1, -1, null, -1, null);
				mout.write("updateEdge", spec, NullWritable.get(), "edges" + "/" + "edges");
				/////////////////////////////////////////////////////////////////////////////////////////////
				// END OF YOUR CODE
				/////////////////////////////////////////////////////////////////////////////////////////////
				
			} catch (Exception e) {
				e.printStackTrace();
				StringWriter sw = new StringWriter();
				PrintWriter pw = new PrintWriter(sw);
				e.printStackTrace(pw);
				logReduce.error(MasterMR.prefix_log + "" + sw.toString());
				globalJob.killJob();
			} catch (AssertionError e) {
				// TODO: handle exception
				e.printStackTrace();
				StringWriter sw = new StringWriter();
				PrintWriter pw = new PrintWriter(sw);
				e.printStackTrace(pw);
				logReduce.error(MasterMR.prefix_log + "" + sw.toString());
				globalJob.killJob();
			}

		}

		@Override
		protected void cleanup(Context context) throws IOException, InterruptedException {
			super.cleanup(context);
			mout.close();
		}
	}

	@Override
	public int run(String[] args) throws Exception {
		// TODO Auto-generated method stub
		Job job = Job.getInstance(getConf());
		globalJob = job;
		job.setJarByClass(LoopUpdateEdges.class);

		job.setMapOutputKeyClass(PairWritable.class);
		job.setMapOutputValueClass(SpecialEdgeValueWritable.class);

		job.setOutputKeyClass(SpecialEdgeTypeWritable.class);
		job.setOutputValueClass(NullWritable.class);

		job.setMapperClass(Map.class);
		job.setReducerClass(Reduce.class);
		job.setCombinerClass(Combiner.class);
		
		job.setInputFormatClass(SequenceFileInputFormat.class);
		job.setOutputFormatClass(SequenceFileOutputFormat.class);
		job.setJobName(jobname);
		/**
		 * There are three input folder.
		 * (1) The edges u v degu degv dis(u,v) before updated in this current loop. 
		 * (2) Folder of delta values of every edge (u,v). With already converged edge, there is no key emitted in @LoopDynamicInteractions.
		 * (3) [@optional] Folder of DI, CI, EI of every edge (u,v). 
		 * Path a separated by ; 
		 * Example: \path\to\current_edges;\path\to\delta_value;\path\to\DI_CI_EI
		 */
		String[] input_files = args[0].split(";"); 
		String ouput_files = args[1];

		// mapedgefile = args[2];

		job.getConfiguration().set("round", args[2]);
		job.getConfiguration().set("threshold", args[3]);
		job.getConfiguration().set("windowSize", args[4]);
		
		FileSystem fs = FileSystem.get(job.getConfiguration());
		job.setNumReduceTasks(20);

		fs.delete(new Path(ouput_files), true);

		if(MasterMR.DEBUG){
			Assert.assertTrue("In case of DEBUG, there must be 4 input folder", input_files.length == 4);
			FileInputFormat.setInputPaths(job, new Path[] { 
					new Path(input_files[0]), 
					new Path(input_files[1]), 
					new Path(input_files[2]), 
					new Path(input_files[3])});
		}else{
			Assert.assertTrue("In release case, there must be two input folders. "
					+ "One for non-updated edges. "
					+ "One for delta value. "
					+ "One for sliding windows (may be empty)", input_files.length == 3);
			FileInputFormat.setInputPaths(job, new Path[] { 
					new Path(input_files[0]), 
					new Path(input_files[1]), 
					new Path(input_files[2]) });
		}
		
		job.getConfiguration().setLong(FileInputFormat.SPLIT_MAXSIZE, 5143265*14);
		FileOutputFormat.setOutputPath(job, new Path(ouput_files));

		//MultipleOutputs.addNamedOutput(job, "updateEdge", TextOutputFormat.class, Text.class, Text.class);
		MultipleOutputs.addNamedOutput(job, "updateEdge", SequenceFileOutputFormat.class, SpecialEdgeTypeWritable.class, NullWritable.class);
		MultipleOutputs.addNamedOutput(job, "testing", TextOutputFormat.class, Text.class, Text.class);
		MultipleOutputs.addNamedOutput(job, "signal", TextOutputFormat.class, Text.class, Text.class);

		try {
			job.waitForCompletion(true);
		} catch (Exception e) {
			e.printStackTrace();
		}
		return 1;
	}

}
