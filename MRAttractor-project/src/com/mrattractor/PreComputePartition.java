package com.mrattractor;

import java.io.IOException;
import java.io.PrintWriter;
import java.io.StringWriter;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.Map.Entry;

import junit.framework.Assert;

import org.apache.hadoop.conf.Configured;
import org.apache.hadoop.fs.FileSystem;
import org.apache.hadoop.fs.Path;
import org.apache.hadoop.io.IntWritable;
import org.apache.hadoop.io.LongWritable;
import org.apache.hadoop.io.NullWritable;
import org.apache.hadoop.io.Text;
import org.apache.hadoop.mapreduce.Job;
import org.apache.hadoop.mapreduce.Mapper;
import org.apache.hadoop.mapreduce.Reducer;
import org.apache.hadoop.mapreduce.lib.input.FileInputFormat;
import org.apache.hadoop.mapreduce.lib.input.SequenceFileInputFormat;
import org.apache.hadoop.mapreduce.lib.input.TextInputFormat;
import org.apache.hadoop.mapreduce.lib.output.FileOutputFormat;
import org.apache.hadoop.mapreduce.lib.output.MultipleOutputs;
import org.apache.hadoop.mapreduce.lib.output.SequenceFileOutputFormat;
import org.apache.hadoop.mapreduce.lib.output.TextOutputFormat;
import org.apache.hadoop.util.Tool;
import org.apache.log4j.Logger;

import com.mrattractor.writable.JaccardEdgeWritable;
import com.mrattractor.writable.SpecialEdgeTypeWritable;
import com.mrattractor.writable.StarGraphWithPartitionWritable;
import com.mrattractor.writable.TripleWritable;
/**
 * Pre-compute partitions of star graphs. A star graph has a center and list of neighbors. We need to find G_{ijk} that a star graph
 * belongs to so that we don't need to re-partition the star graph times to times. 
 * @author ben
 *
 */
public class PreComputePartition extends Configured implements Tool{
	
	static String jobname = "PreComputePartition";

	public static Job globalJob;
	
	public static int node2hash(int u, int no_partitions){
		return u%no_partitions;
	}
	
	/**Mapping function*/
	public static class Map extends Mapper<SpecialEdgeTypeWritable, NullWritable, IntWritable, TripleWritable> {
		//Number of disjoint partitions of this graph. default 20
		int p = 0; 
		Logger logMap = Logger.getLogger(Map.class);

		@Override
		protected void setup(Context context) throws IOException, InterruptedException {
			super.setup(context);
			p = context.getConfiguration().getInt("no_partitions", 0);
		}

		@Override
		protected void map(SpecialEdgeTypeWritable edge, NullWritable value, Context context) throws IOException, InterruptedException {
			try {
				/**
				 * Each line is an edge: u v 
				 */
//				String[] args  = value.toString().split("\\s+");
//				int u = Integer.parseInt(args[0]);
//				int v = Integer.parseInt(args[1]);
				Assert.assertTrue("The input must be an edge", edge.type == 'G');
				int u = edge.center;
				int v = edge.target;
				int hashU = node2hash(u, p);
				int hashV = node2hash(v, p);
				IntWritable key = new IntWritable();
				TripleWritable triple_graph = new TripleWritable();
				if (hashU == hashV) {
					for (int a = 0; a < p; a++) {
						for (int b = a + 1; b < p; b++) {
							if (a == hashV || b == hashV)
								continue;
							
							triple_graph.set(a, b, hashV);
							key.set(u);
							context.write(key, triple_graph);
							key.set(v);
							context.write(key, triple_graph);
						}
					}
				} else {
					for (int a = 0; a < p; a++) {
						if (a != hashU && a != hashV) {
							triple_graph.set(a, hashU, hashV);
							key.set(u);
							context.write(key, triple_graph);
							key.set(v);
							context.write(key, triple_graph);
						}
					}
				}
				
				
			} catch (Exception e) {
				StringWriter sw = new StringWriter();
				PrintWriter pw = new PrintWriter(sw);
				e.printStackTrace(pw);
				logMap.error(MasterMR.prefix_log + "" + sw.toString());
				e.printStackTrace();
				globalJob.killJob();
			}
		}
		@Override
		protected void cleanup(Context context)
				throws IOException, InterruptedException {
			// TODO Auto-generated method stub
			super.cleanup(context);
		}
	}
	/**Values are 3-partition graphs G_{ijk}*/
	public static class CombineGraphPartition extends Reducer<IntWritable, TripleWritable, IntWritable, TripleWritable> {
		int p = 0; 
		Logger logCombiner = Logger.getLogger(CombineGraphPartition.class);
		@Override
		protected void setup(Context context)
				throws IOException, InterruptedException {
			// TODO Auto-generated method stub
			super.setup(context);
			p = context.getConfiguration().getInt("no_partitions", 0);
		}
		@Override
		protected void reduce(IntWritable vertexKey, Iterable<TripleWritable> values, Context context)
				throws IOException, InterruptedException {
			// TODO Auto-generated method stub
			try{
				HashMap<String, Integer> mapTriplets = new HashMap<String, Integer>();
				while(values.iterator().hasNext()){
					TripleWritable adj = values.iterator().next();
					String key = adj.toString();
					//we will emit at inside this loop (may be it can be faster)
					if(!mapTriplets.containsKey(key)){
						//when hashmap does not contain this G_{ijk}
						context.write(vertexKey, adj);
						mapTriplets.put(key, 1);
					}
					
				}
				Assert.assertTrue(mapTriplets.size() <= p*p);
				
			}catch(Exception e){
				StringWriter sw = new StringWriter();
				PrintWriter pw = new PrintWriter(sw);
				e.printStackTrace(pw);
				logCombiner.error(MasterMR.prefix_log + "" + sw.toString());
				e.printStackTrace();
				globalJob.killJob();
			}
			
		}
		@Override
		protected void cleanup(Context context)
				throws IOException, InterruptedException {
			super.cleanup(context);
		}
	}
	
	/**Values are 3-partition graphs G_{ijk}*/
	public static class Reduce extends Reducer<IntWritable, TripleWritable, SpecialEdgeTypeWritable, NullWritable> {
		
		int p = 0; 
		Logger logReduce = Logger.getLogger(Reduce.class);
		MultipleOutputs<SpecialEdgeTypeWritable, NullWritable> mout;

		@Override
		protected void setup(Context context)
				throws IOException, InterruptedException {
			// TODO Auto-generated method stub
			
			p = context.getConfiguration().getInt("no_partitions", 0);
			mout = new MultipleOutputs<SpecialEdgeTypeWritable, NullWritable>(context);
		}
		@Override
		protected void reduce(IntWritable vertexKey, Iterable<TripleWritable> values, Context context)
				throws IOException, InterruptedException {
			// TODO Auto-generated method stub
			try{
				if(vertexKey.get() == 3710){
					int x = 0;
					x+=1;
				}
				
				
				ArrayList<TripleWritable> triples = new ArrayList<TripleWritable>();
				
				HashMap<TripleWritable, Integer> mapTriplets = new HashMap<TripleWritable, Integer>();
				while(values.iterator().hasNext()){
					TripleWritable adj = values.iterator().next();
					TripleWritable newadj = new TripleWritable();
					newadj.set(adj.left, adj.mid, adj.right);
					mapTriplets.put(newadj, 1);
					
				}
				
				Assert.assertTrue("The number of distinct Triplets should be smaller than p*p", mapTriplets.size() <= p*p);
				for(Entry<TripleWritable, Integer> entry: mapTriplets.entrySet()){
					//Assert.assertTrue(entry.getKey().split("\\s+").length == 3);
					triples.add(entry.getKey()); 
				}
				
				SpecialEdgeTypeWritable special = new SpecialEdgeTypeWritable();
				special.init('S', vertexKey.get(), -1, -1, triples.size(), triples, -1, null);
				
				//StarGraphWithPartitionWritable star = new StarGraphWithPartitionWritable();
				//star.set(vertexKey.get(), null, triples);
				
				mout.write("graph", special, NullWritable.get(), "partitions"); 
			}catch(Exception e){
				StringWriter sw = new StringWriter();
				PrintWriter pw = new PrintWriter(sw);
				e.printStackTrace(pw);
				logReduce.error(MasterMR.prefix_log + "" + sw.toString());
				e.printStackTrace();
				globalJob.killJob();
			}
			
		}
		@Override
		protected void cleanup(Context context) throws IOException, InterruptedException {
			//super.cleanup(context);
			mout.close();
		}
	}
	
	@Override
	public int run(String[] args) throws Exception {
		// TODO Auto-generated method stub
		Job job = Job.getInstance(getConf());
		globalJob = job;
		job.setJarByClass(PreComputePartition.class);

		job.setJobName(jobname);
		job.setMapOutputKeyClass(IntWritable.class);
		job.setMapOutputValueClass(TripleWritable.class);

		job.setOutputKeyClass(SpecialEdgeTypeWritable.class);
		job.setOutputValueClass(NullWritable.class);

		job.setMapperClass(Map.class);
		job.setCombinerClass(CombineGraphPartition.class);
		job.setReducerClass(Reduce.class);

		job.setInputFormatClass(SequenceFileInputFormat.class);
		job.setOutputFormatClass(SequenceFileOutputFormat.class);
		String input_files = args[0];
		String ouput_files = args[1];
		//you should not do it manually like this!!!!
		job.getConfiguration().setLong(FileInputFormat.SPLIT_MAXSIZE, 5143265);
		job.getConfiguration().setInt("no_partitions", Integer.parseInt(args[2]));

		FileSystem fs = FileSystem.get(job.getConfiguration());
		fs.delete(new Path(ouput_files), true);
		FileInputFormat.setInputPaths(job, new Path[] {new Path(input_files)});
		FileOutputFormat.setOutputPath(job, new Path(ouput_files));

		MultipleOutputs.addNamedOutput(job, "graph", SequenceFileOutputFormat.class, SpecialEdgeTypeWritable.class, NullWritable.class);
		//MultipleOutputs.addNamedOutput(job, "graph", TextOutputFormat.class, Text.class, Text.class);
		try {
			job.waitForCompletion(true);
		} catch (Exception e) {
			e.printStackTrace();
		}
		return 1;
	}

}
