package com.mrattractor;

import java.io.BufferedReader;
import java.io.File;
import java.io.FileNotFoundException;
import java.io.FileReader;
import java.io.IOException;
import java.io.PrintWriter;
import java.util.ArrayList;
import java.util.Collections;
import java.util.LinkedList;
import java.util.PriorityQueue;
import java.util.Queue;

import junit.framework.Assert;
import no_memory_jaccard.JaccardInit;

import org.apache.commons.lang.NotImplementedException;
import org.apache.hadoop.conf.Configuration;
import org.apache.hadoop.fs.FileSystem;
import org.apache.hadoop.fs.FileUtil;
import org.apache.hadoop.fs.Path;
import org.apache.hadoop.io.NullWritable;
import org.apache.hadoop.io.SequenceFile;
import org.apache.hadoop.io.SequenceFile.Reader;
import org.apache.hadoop.io.SequenceFile.Writer;
import org.apache.hadoop.util.ToolRunner;

import com.mrattractor.writable.SpecialEdgeTypeWritable;
import com.single.attractor.CommunityDetection;

public class MasterMR {
	
	/**Testing values*/
	public static boolean DEBUG = false;
	
	/**Output file in local red.cs.usu.edu to see how balancing between reducers*/
	public static boolean TEST_BALANCING = false;
	
	/**True: if we don't want to re-computed Jaccard distance*/
	private static boolean FORFAST = false;
	
	/**No Longer used!!*/
	public static boolean FORDICT = false;
	
	/**Checking consistent of degree, @true : checking consistent degree when reducing the number of edges.*/
	public static boolean CheckDegree = false;
	
	/**Reducing the number of edges of the graph after each iteration*/
	public static boolean REDUCED_EDGE = true;
	
	/**Delete previous folder */
	public static boolean DELETE_STALE_INFO = true;
	
	/**Pre-computing partitions*/
	public static boolean PRE_COMPUTE_PARTITION = true; //true: 
	
	/***/
	public static boolean ESTIMATION_WORK_LOAD = false;
	
	public static boolean BACKUP_DATA = false;
	
//	public static boolean USE_BACKUP_DATA = false;
	
	public static int no_reduers_for_dynamic_interactions = 20; //20: 100%CPU=> failed, >=25: 120% CPU=> failed.
//	public static String degreeFile = "";
	public static String degreeFileKey = "deg_file";
	public static String cacheTypeMessage = "";
	public static String LOAD_BALANCED_HEADER = "LOAD_BALANCED_HEADER";
	PrintWriter logJob;
	
	double time_generating_star_graph = 0;
	double time_computing_dynamic_interactions = 0;
	double time_updating_edges = 0;
	double time_running_on_single_machine = 0;
	
	public static String prefix_log = "CaiGiDoKhongOn-";


	/** Complexity of each mapper: O(|no_partition_of_neighbors_of_center| * p)
	 * Fast initialization method, especially when p is large */

	
	private void genStarGraphsWithDistancePrePartition(String inputs, String output, String degfile)
			throws Exception {
		long tic = System.currentTimeMillis();
		System.out.println("Generating Star Graphs+Distance+PrePartition.");
		int res = ToolRunner.run(new Configuration(), new LoopGenStarGraphWithPrePartitions(),
				new String[] { inputs, output , degfile});
		long toc = System.currentTimeMillis();
		
		time_generating_star_graph += (toc - tic) / 1000.0;
		
	}
	
	
	private void dynamicInteractionFaster(String input, String output,
			String no_partitions, String lambda, String no_loops, String s_cacheSize, String MB_per_reducers, String no_reducers,
			String degfile,
			String hdfs_load_balanced_file)
			throws Exception {
		
		long tic = System.currentTimeMillis();
		System.out.println("Dynamic Interactions No Cache.");
		cacheTypeMessage = "Dynamic+Interactions+No+Cache";
//		int res = ToolRunner.run(new Configuration(),
//				new LoopDynamicInteractionsFaster(), new String[] { input, output,
//						no_partitions, lambda, no_loops });
		
		int res = ToolRunner.run(new Configuration(),
				new LoopDynamicInteractionsFasterNoCache(), new String[] { input,
			output, no_partitions, lambda, no_loops, s_cacheSize, MB_per_reducers, no_reducers, degfile, hdfs_load_balanced_file});
		
		long toc = System.currentTimeMillis();
		time_computing_dynamic_interactions += (toc - tic) / 1000.0;
	}
	
	
	private void precomputePartitions(String input, String output, String no_partitions) throws Exception {
		System.out.println("Pre-computing Partition of Graph.");
		int res = ToolRunner.run(new Configuration(), new PreComputePartition(),
				new String[] { input, output, no_partitions});
	}
	
	
	private void updateEdge(String input, String output, String no_loops, String windows_size, String miu) throws Exception {
		long tic = System.currentTimeMillis();
		System.out.println("Update Edges.");
		int res = ToolRunner.run(new Configuration(), new LoopUpdateEdges(),
				new String[] { input, output, no_loops, miu, windows_size});
		long toc = System.currentTimeMillis();
		time_updating_edges += (toc - tic) / 1000.0;
	}
	
	
	class Bucket implements Comparable<Bucket>{
		long totalWorkLoad = 0;
		ArrayList<String> subgraphs;
		public Bucket(SubGraphInfo element) {
			
			this.totalWorkLoad += element.workload;
			this.subgraphs = new ArrayList<String>();
			this.subgraphs.add(element.triple);
			
		}
		public void set(SubGraphInfo next){
			this.totalWorkLoad += next.workload;
			this.subgraphs.add(next.triple);
		}
		//smallest to smallest
		@Override
		public int compareTo(Bucket sub) {
			// TODO Auto-generated method stub
			if (totalWorkLoad > sub.totalWorkLoad) {
				return 1;
			}
			if (totalWorkLoad == sub.totalWorkLoad) {
				return 0;
			}
			return -1;
		}
	}
	/**
	 * Greedy algorithm to roughly balance workload. 
	 * Complexity O(N * logK) 
	 * with N is the number of triple subgraphs, 
	 * K is the number of reducers
	 * @author osboxes
	 *
	 */
	
	class SubGraphInfo implements Comparable<SubGraphInfo>{
		long workload;
		String triple;
		
		public SubGraphInfo(long _workLoad, String _triple) {
			// TODO Auto-generated constructor stub
			this.workload = _workLoad;
			this.triple = _triple;
		}
		//sort largest to smallest
		@Override
		public int compareTo(SubGraphInfo sub) {
			// TODO Auto-generated method stub
			if (workload > sub.workload) {
				return -1;
			}
			if (workload == sub.workload) {
				return 0;
			}
			return 1;
		}
		
	}
	/**We try to balance the workload for each reducer. The number of reducers are usually evenly divided by Hadoop framework.*/
	private void computeLoadBalancedGreedy(String workload_estimate_out, String graphname, 
			FileSystem hdfs, FileSystem localsystem, Configuration conf, String no_reducer_dynamic, String balancedResultFile) throws IOException{
		String out_est_load_file_merged = graphname + "_est_workload_merged.txt";
		MyUtil.mergeFiles(workload_estimate_out, out_est_load_file_merged, hdfs, localsystem, conf);
		BufferedReader reader = new BufferedReader(new FileReader(new File(out_est_load_file_merged)));
		int no_reducers = Integer.parseInt(no_reducer_dynamic);
		String line = "";
		//each work load is the load of a subgraph G_{ijk}
		ArrayList<SubGraphInfo> listWorkLoad = new ArrayList<SubGraphInfo>();
		while((line = reader.readLine()) != null){
			String[] args = line.split("\\s+");
			long workLoad = Long.parseLong(args[3]);
			String triple = args[0] + " " + args[1] + " " + args[2];
			SubGraphInfo ele = new SubGraphInfo(workLoad, triple);
			listWorkLoad.add(ele);
		}
		reader.close();
		File f = new File(out_est_load_file_merged);
		f.delete();
		int K = no_reducers;
		int N = listWorkLoad.size();
		if(K >= N){
			//when the number of reducers are large enough such that each reducer can handle one subgraph G_{ijk}
			PrintWriter writer = new PrintWriter(new File(balancedResultFile));
			for(int i=0; i<listWorkLoad.size(); i+=1){
				String subgraph = listWorkLoad.get(i).triple;
				//subgraph @subgraph will be handled by reducer name @i
				writer.println(subgraph + " " + i); 
			}
			writer.close();
			return;
		}else{
			//When we have few reducers, we want each reducer receive subgraphs such that complexity is roughly equal 
			// across reducers.
			//Sorting from largest to smallest
			
			Collections.sort(listWorkLoad);
			
			//Initialize K bucket in Heap
			PriorityQueue<Bucket> workLoadHeap = new PriorityQueue<Bucket>();
			for(int i=0; i<K; i+=1){
				SubGraphInfo sub = listWorkLoad.get(i);
				workLoadHeap.add(new Bucket(sub));
			}
			//Greedy approach 
			//http://stackoverflow.com/questions/6455703/fair-partitioning-of-set-s-into-k-partitions/6486812#6486812
			for(int i=K; i<N; i+=1){
				//Get the next one
				SubGraphInfo sub = listWorkLoad.get(i);
				//select the smallest bucket in Heap 
				Bucket topsmallest = workLoadHeap.poll();
				if(MasterMR.DEBUG){
					Bucket next = workLoadHeap.peek();
					Assert.assertTrue(next.totalWorkLoad >= topsmallest.totalWorkLoad);
				}
				topsmallest.set(sub);
				workLoadHeap.add(topsmallest);
				Assert.assertTrue(workLoadHeap.size() == K);
			}
			if(MasterMR.TEST_BALANCING){
				PrintWriter test_heap = new PrintWriter(new File("check_load_balancing"));
				Assert.assertTrue(K == workLoadHeap.size());
				test_heap.println("Number of reducers: " + K);
				for(Bucket b : workLoadHeap){
					test_heap.println(b.totalWorkLoad);
				}
				test_heap.close();
			}
			
			PrintWriter writer = new PrintWriter(new File(balancedResultFile));
			
			for(int i=0; i<K; i+=1){
				Bucket bucket = workLoadHeap.poll();
				for(String s : bucket.subgraphs){
					//These subgraphs will handlel by reducer @i.
					writer.println(s + " " + i);
				}
			}
			writer.close();
			
		}
		
		
		
	}
	
	private void initLogFile(String graphfile, int cacheType, String no_partition_dynamic_interaction, 
			String no_reducers) throws FileNotFoundException{
		String[] a = graphfile.split("/");
		StringBuilder bd = new StringBuilder();
		for(int kk = 0; kk<a.length; kk+=1){
			bd.append(a[kk] + "+");
		}
		String namefile = bd.toString();
		
		switch (cacheType) {
		case 0:{
			//dynamicInteraction
			cacheTypeMessage = "Dynamic+Interactions+No+Cache";
			break;
		}
		case 1:{
			cacheTypeMessage = "Dynamic+Interactions+With+Cache+Common+Nodes";
			break;
		}
		case 2:{
			cacheTypeMessage = "Dynamic+Interactions+With+Cache+Total+Degree";
			break;
		}
		case 3:{
			cacheTypeMessage = "Dynamic+Interactions+Cache+No+Priority+Queue";
			break;
			//throw new NotImplementedException("Not implemented here for case caching type is 3");
		}
		case 4:{
			cacheTypeMessage = "DynamicInteractions+SharedMemoryCache+NoPriorityQueue";
			break;
			//throw new NotImplementedException("Not implemented here for case caching type is 3");
		}
		default:
			throw new NotImplementedException("Not implemented for other cases!!!");
//			break;
		}
		
		logJob = new PrintWriter(new File(String.format("stat-MrAttractor-%s-%s-partions-%s-noReducers-%s.txt", namefile, cacheTypeMessage, no_partition_dynamic_interaction, no_reducers)));
	}
	/**
	 * Reduce the number of edges after each iteration. This will reduce a lot of effort for partitioning the graph
	 * This procedure will run in Master node
	 * @param N: The original number of vertices in the graph. 
	 * @param curr_edge_folder : current edge folder on HDFS after updating edges
	 * @param local_edge_file : This file is the result of merging all parts of edges in @curr_edge_folder
	 * @param hdfs : The HDFS system
	 * @param local : The local HDFS
	 * @param conf
	 * @param reduced_local_edge_file
	 * @param reduced_hdfs_edge_file
	 */
	private int[] reduceEdges(int N, 
			String curr_edge_folder, 
			FileSystem hdfs, 
			FileSystem local, 
			Configuration conf, 
			String reduced_local_edge_file,  
			String left_edges_file, 
			String prev_left_edges_file)
	{
		try{
			
			long tic = System.currentTimeMillis();
			
			//initializing all vertices as virgin, using boolean array to save memory.
			int[] dirty = new int[N];
			int non_converged_edges = 0;
			int converged_edges=0;
			String local_edge_folder = new File(curr_edge_folder).getName();
			
			File downloaded_folder = new File(local_edge_folder);

			MyUtil.copyFolder(curr_edge_folder, local_edge_folder, hdfs, local, conf);
			
			File[] listOfFiles = downloaded_folder.listFiles();
			
			ArrayList<SpecialEdgeTypeWritable> listEdges = new ArrayList<SpecialEdgeTypeWritable>();
			Configuration localConf = new Configuration();
			String hadoopLocalFS = "file:///";
			localConf.set(FileSystem.FS_DEFAULT_NAME_KEY, hadoopLocalFS);
		    for (int i = 0; i < listOfFiles.length; i++) {
		    	SpecialEdgeTypeWritable key = new SpecialEdgeTypeWritable();
			    NullWritable val = NullWritable.get();
			    SequenceFile.Reader reader;
		    	if (listOfFiles[i].isFile()) {
		    		File e = listOfFiles[i];
		    		if (e.isHidden()){
		    			continue;
		    		}
		    		String local_path = local_edge_folder + "/" + e.getName();
		    		
		    		reader = new SequenceFile.Reader(localConf, Reader.file(new Path(local_path)));
		    		while (reader.next(key, val)) {
		    			SpecialEdgeTypeWritable spec = new SpecialEdgeTypeWritable();
		    			spec.init(key.type, key.center, key.target, key.weight, -1, null, -1, null);
		    			listEdges.add(spec);
		 		    }
		 			reader.close();
		    	} 
		    }
		    FileUtil.fullyDelete(downloaded_folder);
		    for(SpecialEdgeTypeWritable edge : listEdges){
		       // System.err.println(key + "\t" + val);
		        int u = edge.center;
				int v = edge.target;
				double dis = edge.weight;
				if(dis < 1 && dis > 0){
					u -= 1;
					v -= 1;
					dirty[u] = 1;
					dirty[v] = 1;
					non_converged_edges += 1;
				}else{
					converged_edges += 1;
				}
		    }
		    for(SpecialEdgeTypeWritable edge : listEdges){
		    	int u = edge.center;
				int v = edge.target;
				u -= 1;
				v -= 1;
				if(dirty[u] == 1 || dirty[v] == 1){
					//If one of the endpoints of an edge is dirty, we need to keep this edge, whatever its distance!!!!
					if(dirty[u] == 0){
						dirty[u] = 2;
					}
					if(dirty[v] == 0){
						dirty[v] = 2;
					} 
				}
		    }
		    
			/**This variable is used for the number of edges that will be used in the next round.
			 * This @the_number_of_continued_to_used_edges is smaller than a threshold we will run SlidingAttractor on Single Machine*/
			int the_number_of_continued_to_used_edges = 0;
			
			/**Writer to HDFS*/
			SequenceFile.Writer writer = SequenceFile.createWriter(localConf,
		            Writer.file(new Path(reduced_local_edge_file)), Writer.keyClass(SpecialEdgeTypeWritable.class),
		            Writer.valueClass(NullWritable.class));
						
			PrintWriter left_edges_writer = new PrintWriter(new File(left_edges_file));
			for(SpecialEdgeTypeWritable edge : listEdges){
				int u = edge.center;
				int v = edge.target;
				
				u -= 1;
				v -= 1;
				
				if(dirty[u] == 1 || dirty[v] == 1 || dirty[u] == 2  || dirty[v] == 2){
					//writer.println(line);
					writer.append(edge, NullWritable.get());
					the_number_of_continued_to_used_edges += 1;
				}else{
					left_edges_writer.println(edge.toStringForLocalMachine());
				}
			}
			
			writer.close();
			
			if(!prev_left_edges_file.equalsIgnoreCase("")){
				BufferedReader reader1 = new BufferedReader(new FileReader(new File(prev_left_edges_file)));
				String line = "";
				while((line = reader1.readLine()) != null){
					left_edges_writer.println(line);
				}
				//ToDo Delete previous file
				File f = new File(prev_left_edges_file);
				f.delete();
				reader1.close();
			}

			left_edges_writer.close();
			System.out.println("#Converged edges: " + converged_edges + " #Edges of reduced graph: " + the_number_of_continued_to_used_edges + " #non-converged edges: " + non_converged_edges);
			logJob.println("#Converged edges: " + converged_edges + " #Edges of reduced graph: " + the_number_of_continued_to_used_edges + " #non-converged edges: " + non_converged_edges);
			logJob.flush();
			
			long toc = System.currentTimeMillis();
			time_updating_edges += (toc - tic) / 1000.0;
			
			return new int[]{converged_edges, non_converged_edges, the_number_of_continued_to_used_edges};
			
		}catch(Exception e){
			e.printStackTrace();
			System.exit(0);
			return null;
		}

	}
	private void mergeTwoFiles(String file1, String file2, String outputfile) throws IOException{
		PrintWriter writer = new PrintWriter(new File(outputfile));
		BufferedReader reader = new BufferedReader(new FileReader(new File(file1)));
		String line = "";
		while((line = reader.readLine()) != null){
			writer.println(line);
		}
		reader = new BufferedReader(new FileReader(new File(file2)));
		line = "";
		while((line = reader.readLine()) != null){
			writer.println(line);
		}
		File f1 = new File(file1);
		f1.delete();
		File f2 = new File(file2);
		f2.delete();
		writer.close();
	}
	
	private void validateFinalEdges(String file1, int no_original_edges) throws IOException{
		BufferedReader reader = new BufferedReader(new FileReader(new File(file1)));
		String line = "";
		int cnt = 0;
		while((line = reader.readLine()) != null){
			String[] args = line.split("\\s+");
			int u = Integer.parseInt(args[0]);
			int v = Integer.parseInt(args[1]);
			double dis = Double.parseDouble(args[2]);
			
			Assert.assertTrue(Math.abs(dis) < 1e-10 || Math.abs(dis - 1.0) < 1e-10);
			cnt += 1;
		}
		Assert.assertTrue(cnt == no_original_edges);
		reader.close();
	}
	
	/**
	 * 
	 * @param graphfile : Input undirected graph, each line is <u> <v> 
	 * @param outfolder : outfolder store in hdfs (Hue server)
	 * @param no_partitions : p number of partitions of a graph
	 * @param lambda : cohesive parameters always = 0.5 (to avoid criticized)
	 * @param s_cacheSize : Size of dictionary
	 * @param cacheType : Caching method 
	 * @param MB_per_reducers : Size in Mb for each containers (reducer)
	 * @param no_reducers : the number of reducers
	 * @param N : the number of vertices
	 * @param M : the number of edges of original graph.
	 * @param windows_size : Size of sliding windows
	 * @param miu : Percentage of 0 or 1 in sliding windows to force an edge converged
	 * @param threshold_used_edges : The number of edges that are small enough to run in single machine.
	 * @throws Exception
	 */
	public void master(String graphfile, String outfolder,
			String no_partitions, String lambda, String s_cacheSize, int cacheType,
			String MB_per_reducers, String no_reducers_unused, 
			String no_reducers_dynamic_interaction, 
			int N, int M,
			String windows_size, String miu,
			int threshold_used_edges,
			int cache_size_single_Attractor,
			String no_partition_dynamic_interaction)
			throws Exception {
		
		Configuration conf = new Configuration();
		FileSystem fs = FileSystem.get(conf);
		if(MasterMR.DELETE_STALE_INFO){
			fs.delete(new Path(outfolder), true);
		}
		FileSystem localFileSystem = FileSystem.getLocal(conf); 
		long tic = System.currentTimeMillis();
		
		String[] a = graphfile.split("/");
		String backup_folder = "backup_folder_" + a[a.length - 1];
		if(MasterMR.BACKUP_DATA){
			File f = new File(backup_folder);
			f.mkdir();
		}
		
		///////////////////////////////////////////////////////////////////////////////
		/// Log File
		//////////////////////////////////////////////////////////////////////////////
		initLogFile(graphfile, cacheType, no_partition_dynamic_interaction, no_reducers_dynamic_interaction);
		//File f1 = new File(graphfile);
		//String binary_graph_file_hdfs = f1.getParent() + "/binary_graph_file";
		//MyUtil.convertOriginalGraphInSequenceFiles(graphfile, fs, conf, localFileSystem, binary_graph_file_hdfs);
		
		
		////////////////////////////////////////////////////////////////////////////////////////////
		/// Ending Log File
		/////////////////////////////////////////////////////////////////////////////////////////////

	
		String prefix = outfolder;
		String degfile = prefix + "/degfile.txt";
		String outDistanceInit = prefix + "/phase2";
		String curr_edge_folder = outDistanceInit + "/edges";
		
		// //////////////////////////////////////////////////////////////////////////////////////////
		// Begin Distance Initialization.                                                          //
		// //////////////////////////////////////////////////////////////////////////////////////////
		fs.mkdirs(new Path(curr_edge_folder));
		MyUtil.computeJaccardDistanceInSingleMachine(graphfile, fs, conf, localFileSystem,
				curr_edge_folder + "/binary_graph_file_initilalized", N, M, Double.parseDouble(lambda), degfile);
		long toc = System.currentTimeMillis();
		long initialization_time = (toc - tic);
		
		logJob.println("Init Interaction Time " + initialization_time / (1000.0));
		logJob.flush();
		System.out.println("Running Time of Initialization: " + ((initialization_time) / (1000.0)));
		// //////////////////////////////////////////////////////////////////////////////////////////
		// End Distance Initialization.                                                            //
		// //////////////////////////////////////////////////////////////////////////////////////////
		
		
		/////////////////////////////////////////////////////////////////////////////////////////////
		//// PRE COMPUTING PARTITION OF GRAPHS  
		//////////////////////////////////////////////////////////////////////////////////////////////
		long tic_pre_partition = System.currentTimeMillis();
		String partitions_out = prefix + "/partitions";
		
		if(MasterMR.PRE_COMPUTE_PARTITION){
			precomputePartitions(curr_edge_folder, partitions_out, no_partition_dynamic_interaction); 
		}
		long toc_pre_partition = System.currentTimeMillis();
		long pre_compute_partition_time = (toc_pre_partition - tic_pre_partition);
		logJob.println("Running time of Pre-partition of all edges: (seconds)" + (pre_compute_partition_time/1000.0) + " p=" + no_partition_dynamic_interaction);
		logJob.flush();
		///////////////////////////////////////////////////////////////////////////////////////////////
		//  END PRE-COMPUTING PARTITIONING OF GRAPHS.
		///////////////////////////////////////////////////////////////////////////////////////////////
		
		boolean usingSlidingWindow = false;
		
		String currentSlidingWindowFolder = prefix + "/sliding_empty";
		fs.mkdirs(new Path(currentSlidingWindowFolder));
		if(Integer.parseInt(windows_size) > 0){
			usingSlidingWindow = true;
		}
		
		// //////////////////////////////////////////////////////////////////////////////////////////
		// Begin Looping of Dynamic Interactions.
		// //////////////////////////////////////////////////////////////////////////////////////////
		boolean Flag = true;
		int cntRound = 0;
		int cacheSize = Integer.parseInt(s_cacheSize);
		
		String prev_converged_edges_file = "";
		tic = System.currentTimeMillis();
		
		
		
		while (Flag) {
			System.out.println("Current Loop: " + (cntRound + 1));
			String out_star_graph_with_dist = prefix + "/LoopPhase1";
			
			long starting_iteration_tic = System.currentTimeMillis();
			
			/////////////////////////////////////////////////////////////////////////////////////////////
			//// ESTIMATION OF WORKLOAD  
			//////////////////////////////////////////////////////////////////////////////////////////////
			long tic_estimation = System.currentTimeMillis();
			//String workload_estimate_out = prefix + "/loadEstimated";
			String hdfs_load_balanced_file = prefix + "/loadEstimated/load_balanced_estimated.txt";
			
			long toc_estimation = System.currentTimeMillis();
			long findWorkLoadBalacingTime = (toc_estimation - tic_estimation) ;
			logJob.println("Running time of Estimation WorkLoad: (seconds) " + (findWorkLoadBalacingTime/ 1000.0) + " p=" + no_partition_dynamic_interaction);
			logJob.flush();
			///////////////////////////////////////////////////////////////////////////////////////////////
			//  END ESTIMATION OF WORKLOAD.
			///////////////////////////////////////////////////////////////////////////////////////////////
			
			/////////////////////////////////////////////////////////////////////////
			/// GENERATING STAR GRAPHS WITH PRE-PARTITIONS
			/////////////////////////////////////////////////////////////////////////
			
			String inputs_for_gen_star_graphs = curr_edge_folder + "," + partitions_out;
			//Injecting graph_partition inside star-graph for fast computation
			genStarGraphsWithDistancePrePartition(inputs_for_gen_star_graphs, out_star_graph_with_dist, degfile);
			
			//////////////////////////////////////////////////////////////////////////
			/// ENDING NEW GENERATING GRAPHS
			//////////////////////////////////////////////////////////////////////////
			
			String out_dynamic = prefix + "/LoopPhase2";
			String no_loops = cntRound + "";
			switch (cacheType) {
			case 0:{
				//dynamicInteraction
				
				dynamicInteractionFaster(out_star_graph_with_dist + "/star",
						out_dynamic, no_partition_dynamic_interaction, lambda, no_loops,
						s_cacheSize,  MB_per_reducers,  no_reducers_dynamic_interaction, degfile, hdfs_load_balanced_file);
				break;
			}
			
			default:
				throw new NotImplementedException("Not implemented for other cases!!!");
//				break;
			}
			
			String out_update_edge = prefix + "/LoopPhase3_" + no_loops;
			
			
			if (MasterMR.DEBUG) {
				updateEdge(String.format("%s;%s;%s;%s", curr_edge_folder, 
														out_dynamic + "/delta_dis", 
														out_dynamic + "/debug", 
														currentSlidingWindowFolder),
														out_update_edge, no_loops, windows_size, miu);
			} else {
				updateEdge(String.format("%s;%s;%s", out_dynamic + "/delta_dis", curr_edge_folder, currentSlidingWindowFolder), 
							out_update_edge, no_loops, windows_size, miu);
				//remove old folder to save disk
				int nloop = Integer.parseInt(no_loops);
				if(nloop != 0){
					//delete outfoder from the last dynamic interactions (nloop-1)
					String tt = prefix + "/LoopPhase3_" + (nloop-1);
					fs.delete(new Path(tt), true);
				}else{
					//remove the edges with jaccard distance to save memory
					//fs.delete(new Path(curr_edge_folder), true);
				}
				
			}
			if(usingSlidingWindow){
				currentSlidingWindowFolder = out_update_edge + "/sliding";
			}
			curr_edge_folder = out_update_edge + "/edges";
			
			////////////////////////////////////////////////////////////////////////////////////////
			/// REDUCING THE NUMBER OF EDGES
			////////////////////////////////////////////////////////////////////////////////////////
			
			if(MasterMR.REDUCED_EDGE){
				//String[] a = graphfile.split("/");
				String current_left_edges_file = "converged_edges_" + a[a.length - 1] + "_"+ cacheTypeMessage + "_" + (cntRound + 1) ;
				//String current_left_edges_file = "converged_edges_" + (cntRound + 1);
				/**binary format, on single machine*/ 
				String reduced_local_edge_file = String.format("local_edge_file_%s_reduced_local_%s.txt", a[a.length - 1], (cntRound + 1)); //single machine
				String reduced_edges_folder = out_update_edge + "/reduced_edges"; //on HDFS folder
				String reduced_hdfs_edge_file = reduced_edges_folder + "/reduced_edges.txt"; //HDFS file
				System.out.println("Reducing the number of edges @Loops: " + (cntRound + 1));
				int[] info = reduceEdges(N, curr_edge_folder, fs, localFileSystem, conf, 
						reduced_local_edge_file,  
						current_left_edges_file, 
						prev_converged_edges_file);
				
				int converged_edges = info[0];
				int non_converged_edges = info[1];
				int used_next_round_edges = info[2];
				
				/**the name of prev_converged_edges_file in text format, on single machine*/
				prev_converged_edges_file = current_left_edges_file;
				curr_edge_folder = reduced_edges_folder;
				
				
				////////////////////////////////////////////////////////////////////////////////////////////
				////
				////////////////////////////////////////////////////////////////////////////////////////////
				long ending_this_iteration_toc = System.currentTimeMillis();
				double length_each_iteration = (ending_this_iteration_toc - starting_iteration_tic) / 1000.0;
				logJob.println("Running time of " + (cntRound + 1) + " iteration: " + length_each_iteration);
				logJob.flush();
				////////////////////////////////////////////////////////////////////////////////////////////
				////
				////////////////////////////////////////////////////////////////////////////////////////////

				if(non_converged_edges <= threshold_used_edges){
					
					
					long tic_single_machine = System.currentTimeMillis();
					long dynamic_time_MR = (tic_single_machine - tic);
					logJob.println("Starting Entering Single Mode");
					logJob.println("No of Loops of MR: " + (cntRound + 1));

					
					
					logJob.println("Dynamic Interaction Time of MR version: " + dynamic_time_MR / (1000.0));
					
					logJob.flush();
					String outputSingleMachine = "output_edges_" + a[a.length - 1] + "single_machine";
					/**Call single machine Attractor. We just continue iterations on single machine*/
					
					CommunityDetection singleMachineAttractor = new CommunityDetection(Integer.parseInt(windows_size), 
																					   Double.parseDouble(miu), 
																					   Double.parseDouble(lambda),
																					   cache_size_single_Attractor,
																					   (cntRound), N, 
																					   outputSingleMachine,
																					   logJob);
																					   
				
					String reduced_local_edge_file_normal = reduced_local_edge_file + "_normal";
					MyUtil.convertBinaryFileToTextFile(reduced_local_edge_file, reduced_local_edge_file_normal, localFileSystem);
					long tic_singl_machine = System.currentTimeMillis();
					String merged_sliding_windows_file_normal = "";
					if(usingSlidingWindow){
						/**Download sliding window file*/
						String binary_sliding_windows_folder_local = "sliding_merged_" + a[a.length - 1] + "MR";
						
						MyUtil.copyFolder(currentSlidingWindowFolder, binary_sliding_windows_folder_local, fs, localFileSystem, conf);
						//mergeFiles(currentSlidingWindowFolder, merged_sliding_windows_file, fs, localFileSystem, conf);
						merged_sliding_windows_file_normal = "sliding_merged_" + a[a.length - 1] + "_normal";
						MyUtil.mergeAndConvertBinaryFiles(binary_sliding_windows_folder_local, merged_sliding_windows_file_normal);
						//merged_sliding_windows_file_normal = merged_sliding_windows_file + "_normal";
						//convertBinaryFileToTextFile(merged_sliding_windows_file, merged_sliding_windows_file_normal, conf, localFileSystem);
					}
					singleMachineAttractor.Execute(reduced_local_edge_file_normal, merged_sliding_windows_file_normal);
					String merged_all_converged_edges_from_single_and_MR = "mergedAllConvergedEdges_" + a[a.length - 1] + ".edges";
					mergeTwoFiles(prev_converged_edges_file, outputSingleMachine, merged_all_converged_edges_from_single_and_MR);
					prev_converged_edges_file = merged_all_converged_edges_from_single_and_MR;
					validateFinalEdges(merged_all_converged_edges_from_single_and_MR, M);
					long toc_single_machine = System.currentTimeMillis();
					
					time_running_on_single_machine += (toc_single_machine - tic_singl_machine) / 1000.0;
					Flag = false; //we already converge!!!!
				}else{
					//After reducing edge file locally, we will push it to HDFS.
					
					FileUtil.copy(new File(reduced_local_edge_file), fs, new Path(reduced_hdfs_edge_file), false, conf);
					if(MasterMR.DELETE_STALE_INFO){
						File f = new File(reduced_local_edge_file);
						f.delete();
					}
				}
				
			}
			////////////////////////////////////////////////////////////////////////////////////////
			/// ENDING REDUCING THE NUMBER OF EDGES
			////////////////////////////////////////////////////////////////////////////////////////
			
//			 if(true){
//			 break;
//			 }
			
			
			if (Flag == true && fs.exists(new Path(out_update_edge + "/flag"))) {
				Flag = true;
			} else {
				System.out.println("MrAttractor-Hoi-Tu-Roi!!!!! Oh yeah");
				System.out.println("No-of-Loops: " + (cntRound + 1));
				toc = System.currentTimeMillis();
				long dynamic_time = toc - tic;
				System.out.println("Total Running Time of Dynamic Interactions (single + MR) (seconds): " + (toc - tic) / (1000.0));

				// File fl = new File(graphfile);
				// String path_parent = fl.getParent();
				

				logJob.println("Finally, everything converges!!!!! Oh yeah");
				logJob.println("No of Loops: " + (cntRound + 1));

				
				logJob.println("Init Interaction Time (duplicated info)" + initialization_time / (1000.0));
				logJob.println("Dynamic Interaction Time (single + MR) (seconds) " + dynamic_time / (1000.0));
				logJob.println("Running time generating star graphs: " + time_generating_star_graph);
				logJob.println("Running time computing dynamic interactions: " + time_computing_dynamic_interactions);
				logJob.println("Running time updating edges: " + time_updating_edges);
				logJob.println("Running time on single machine: " + time_running_on_single_machine);
				
				tic = System.currentTimeMillis();
				//Finding connected component with Breath First search.
				
				
				/**
				 * The @prev_converged_edges_file is the file of all converged edges. We only need to based on this file
				 * to find community.
				 */
				String final_edge_file = prev_converged_edges_file;
				
				toc = System.currentTimeMillis();
				long copying_time = toc-tic;
				logJob.println("Copying Final Edge file: " + (copying_time)/1000.0);
				//Doing Breath First Search.
				tic = System.currentTimeMillis();
				String outfile = final_edge_file+".communities";
				BreadthFirstSearch(final_edge_file, N, outfile);
				
				toc = System.currentTimeMillis();
				long breath_first_search_time = (toc-tic);
				logJob.println("Breadth First Search Time: " + breath_first_search_time/ 1000.0 );
				long three_phases = initialization_time + dynamic_time + breath_first_search_time + copying_time + pre_compute_partition_time + findWorkLoadBalacingTime;
				logJob.println("Total Running Time (seconds): " + three_phases / (1000.0));
				logJob.close();

				break;
			}

			cntRound += 1;

		}

	}
	/**Do breath first search to find communities on the converged edges. The edge with distance =1 is discarded. 
	 * Only the edge with distance = 0 will be kept in the graph for bfs. Complexity: O(m+n)**/
	private void BreadthFirstSearch(String filename, int N, String outfile) throws IOException{
//		File fin = new File(filename);
		int E = 0; 
		int nEdgeDis1 = 0;
		BufferedReader reader = new BufferedReader(new FileReader(new File(filename)));
		String line = "";
		int[] visited = new int[N];
		int[] comms = new int[N];
		ArrayList<Integer>[] adjList = (ArrayList<Integer>[]) new ArrayList[N];
		for(int i=0; i<N; i+=1){
			adjList[i] = new ArrayList<Integer>();
		}
		/**Reading the final edge file */
		while((line = reader.readLine()) != null){
			String[] args = line.split("\\s+");
			int u = Integer.parseInt(args[0]);
			int v = Integer.parseInt(args[1]);
			int dis = -1;
			if(args[2].contains("0.")){
				dis = 0;
			}else if(args[2].contains("1.")){
				dis = 1;
			}
			Assert.assertTrue("Some thing wrong with implementation", dis != -1);
			Assert.assertTrue(u >= 1 && u<= N);
			Assert.assertTrue(v >= 1 && v <= N);
			E+=1;
			if (dis == 1){
				//This edge will be discarded.
				nEdgeDis1 += 1;
				continue;
			}
			u-=1; v-=1; //note to reduce this vertex.
			adjList[u].add(v);
			adjList[v].add(u);
		}
		/**Doing breadth first search */
		Queue<Integer> queue = new LinkedList<Integer>();
		int ID = 0;
		for(int i=0; i<N; i+=1){
			if(visited[i] == 0){
				//starting new bfs
				queue.clear();
				visited[i] = 1;
				queue.add(i);
				ID += 1; //Starting new community.
				comms[i] = ID;
				//These guys will belong to same community.
				while(!queue.isEmpty()){
					int curr = queue.poll();
					Assert.assertTrue(curr >= 0 && curr < N);
					visited[curr] = 1;
					for(Integer adj : adjList[curr]){
						if(visited[adj] == 1) continue;
						/** This line is important since it will help avoid adding too many objects in queue. */
						visited[adj] = 1; 
						comms[adj] = ID;
						queue.add(adj);
					}
				}
			}
		}
		PrintWriter out = new PrintWriter(new File(outfile));
		for(int i=0; i<N; i+=1){
			out.println((i+1) + " " + comms[i]);
		}
		out.close();
		
		
	}
	
	
	public static void main(String[] args) throws Exception {
		
		Runtime rt = Runtime.getRuntime();
		long totalMem = rt.totalMemory();
		long maxMem = rt.maxMemory();
		long freeMem = rt.freeMemory();
		double megs = 1048576.0;

		System.out.println ("Total Memory: " + totalMem + " (" + (totalMem/megs) + " MiB)");
		System.out.println ("Max Memory:   " + maxMem + " (" + (maxMem/megs) + " MiB)");
		System.out.println ("Free Memory:  " + freeMem + " (" + (freeMem/megs) + " MiB)");

		
		Assert.assertTrue("Numer of arguments must be >=16 while current input has: " + args.length + " arguments.", args.length >= 16);
		String graphfile = args[0]; // "testgraphs/connected_group_user_45_uw_modified.txt";
		// String graphfile = "testgraphs/sample.txt";
		String outfolder = args[1]; // "MrAttractor";
		String nopartitions = args[2]; // "20";

		String lambda = args[3]; // "0.5";
		String cacheSize = args[4];
		int cacheType = Integer.parseInt(args[5]);
		String MB_per_reducers = args[6];
		String no_reducers = args[7];
		int no_vertices = Integer.parseInt(args[8]);
		String windows_size = args[9];
		/**Percentage of 1 or 0 in a sliding windows to consider an edge converged*/
		String miu = args[10];
		Assert.assertTrue(
				"caching type value must be in {0, 1,2,3}. \n"
				+ "[0] means no caching at all \n ",
				cacheType == 0);
//		 nopartitions = "5";
		 
//		 cacheSize = "10";
		
		
		
//		miu = "0.7";
		/**The number of edges that are small enough to run on single machine Attractor*/
		int no_edges_to_run_single_machine = Integer.parseInt(args[11]);
		
		
		/**The maximum size of dictionary. This is to avoid out-of-memory of Master node*/
		int cache_size_single_Attractor = Integer.parseInt(args[12]);
		
		int no_edges = Integer.parseInt(args[13]);
		
		String no_reducers_dynamic_interaction = args[14];
		String no_partition_dynamic_interaction = args[15];
		
		if(MasterMR.DEBUG){
			/**Set no edges to run single machine to negative to check correctness*/
//			no_edges_to_run_single_machine = 1;
			/**Set windowSize to negative to check correctness!!!*/
//			windows_size = "-20";
//			/**Changing graph file*/
			
		}
		
		MasterMR t = new MasterMR();
		boolean use_backup_data = false;
		if(Integer.parseInt(args[16]) == 1){
			use_backup_data = true;
		}
		
		t.master(graphfile, outfolder, 
				nopartitions+"", lambda+"", 
				cacheSize+"", cacheType, 
				MB_per_reducers, no_reducers, no_reducers_dynamic_interaction, 
				no_vertices, no_edges,
				windows_size, miu, 
				no_edges_to_run_single_machine, 
				cache_size_single_Attractor,
				no_partition_dynamic_interaction);
		
		
		
		//t.temp_testing();
		
		/**
		 * When releasing Jar file:
		 * MasterMR.DEBUG = false
		 * MasterMR.DELETE_STALE_INFO = true //whether you want to delete your previous-files
		 */
	}

}
