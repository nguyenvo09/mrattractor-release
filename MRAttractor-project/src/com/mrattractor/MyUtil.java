package com.mrattractor;

import java.io.BufferedReader;
import java.io.File;
import java.io.FileReader;
import java.io.IOException;
import java.io.PrintWriter;
import java.util.HashMap;
import java.util.Map.Entry;

import no_memory_jaccard.JaccardInit;
import nomemory.EdgeValue_v2;
import nomemory.VertexValue_v2;

import org.apache.hadoop.conf.Configuration;
import org.apache.hadoop.fs.FileStatus;
import org.apache.hadoop.fs.FileSystem;
import org.apache.hadoop.fs.FileUtil;
import org.apache.hadoop.fs.Path;
import org.apache.hadoop.io.NullWritable;
import org.apache.hadoop.io.SequenceFile;
import org.apache.hadoop.io.SequenceFile.Reader;
import org.apache.hadoop.io.Writable;
import org.apache.hadoop.util.ReflectionUtils;
import org.junit.Assert;

import com.mrattractor.writable.Settings;
import com.mrattractor.writable.SpecialEdgeTypeWritable;

public class MyUtil {
	/**
	 * If we merge Sequence File using "hadoop fs -getmerge" or Java API, Hadoop simply concatenate all files,
	 * leading to problem of each sequence file. Therefore, I have to come up with a version of myself. 
	 * @param fromDirectory
	 * @param toFile
	 * @param keyClass
	 * @param valueClass
	 * @param hdfs
	 * @param conf
	 * @throws IOException
	 */
	public static <K, V> void mergeSequenceFilesToLocal(Path fromDirectory, Path toFile,
			Class<K> keyClass, Class<V> valueClass, FileSystem hdfs, Configuration conf) throws IOException {
		
		
		if (!hdfs.isDirectory(fromDirectory)) {
			throw new IllegalArgumentException("'" + fromDirectory.toString() + "' is not a directory");
		}
		/**Indicating local file system instead of HDFS*/
		String hadoopLocalFS = "file:///";
		conf.set(FileSystem.FS_DEFAULT_NAME_KEY, hadoopLocalFS);
		
		SequenceFile.Writer writer = SequenceFile.createWriter(conf,
				SequenceFile.Writer.file(toFile),
				SequenceFile.Writer.keyClass(keyClass),
				SequenceFile.Writer.valueClass(valueClass));

		for (FileStatus status : hdfs.listStatus(fromDirectory)) {
			if (status.isDirectory()) {
				System.out.println("Skip directory "+ status.getPath().getName());
				continue;
			}

			Path file = status.getPath();

			if (file.getName().startsWith("_")) {
				//There are files such "_SUCCESS"-named in jobs' ouput folders 
				System.out.println("Skip \"_\"-file '" + file.getName() + "'"); 													
				continue;
			}

			System.out.println("Merging '" + file.getName() + "'");
			//reading from remote!!!
			SequenceFile.Reader reader = new SequenceFile.Reader(new Configuration(), SequenceFile.Reader.file(file));
			Writable key = (Writable) ReflectionUtils.newInstance(reader.getKeyClass(), conf);
			Writable value = (Writable) ReflectionUtils.newInstance(reader.getValueClass(), conf);

			while (reader.next(key, value)) {
				writer.append(key, value);
			}

			reader.close();
		}

		writer.close();

	}
	/**
	 * Convert binary to normal text file for single machine processed
	 * @param binaryFile
	 * @param outputTextFile
	 * @throws Exception 
	 */
	public static void convertBinaryFileToTextFile(String binaryFile, String outputTextFile, FileSystem local) throws Exception{
		Configuration local_config = new Configuration();
		String hadoopLocalFS = "file:///";
		local_config.set(FileSystem.FS_DEFAULT_NAME_KEY, hadoopLocalFS); 
		SequenceFile.Reader reader = new SequenceFile.Reader(local_config, Reader.file(new Path(binaryFile)));
		
		SpecialEdgeTypeWritable key = new SpecialEdgeTypeWritable();
	    NullWritable val = NullWritable.get();
	    PrintWriter writer = new PrintWriter(new File(outputTextFile));
	    while (reader.next(key, val)) {
	    	writer.println(key.toStringForLocalMachine());
	    }
	    File f = new File(binaryFile);
	    f.delete();
	    writer.close();
	    reader.close();
	}
	public static void mergeFiles(String root_folder, String merged_file, FileSystem from, FileSystem to, Configuration conf) {
		try {
			to.delete(new Path(merged_file), false);
			FileUtil.copyMerge(from, new Path(root_folder), to, new Path(merged_file), false, conf, null);
		} catch (Exception e) {
			e.printStackTrace();
		}
	}
	/** @param hdfs_folder
	 * @param local_folder
	 * @param hdfs
	 * @param local
	 * @param conf */
	public static void copyFolder(String hdfs_folder, String local_folder, FileSystem hdfs, FileSystem local, Configuration conf){
		try {
			File f = new File(local_folder);
			FileUtil.fullyDelete(f);
			FileUtil.copy(hdfs, new Path(hdfs_folder), local, new Path(local_folder), false, conf);
		} catch (IOException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
	}
	public static void mergeAndConvertBinaryFiles(String binary_folder, String output_text_file_local) throws Exception{
		File folder_ = new File(binary_folder);
		Assert.assertTrue(folder_.isDirectory());
		
		Configuration local_config = new Configuration();
		String hadoopLocalFS = "file:///";
		local_config.set(FileSystem.FS_DEFAULT_NAME_KEY, hadoopLocalFS); 
		
		PrintWriter writer = new PrintWriter(new File(output_text_file_local));
		
		
		File downloaded_folder = new File(binary_folder);
		File[] listOfFiles = downloaded_folder.listFiles();
		for (int i = 0; i < listOfFiles.length; i++) {
			SpecialEdgeTypeWritable key = new SpecialEdgeTypeWritable();
		    NullWritable val = NullWritable.get();
		    SequenceFile.Reader reader;
	    	if (listOfFiles[i].isFile()) {
	    		File e = listOfFiles[i];
	    		if(e.isHidden()){
	    			continue;
	    		}
	    		String local_path = binary_folder + "/" + e.getName();
	    		
	    		reader = new SequenceFile.Reader(local_config, Reader.file(new Path(local_path)));
	    		while (reader.next(key, val)) {
	    			writer.println(key.toStringForLocalMachine());
	 		    }
	 			reader.close();
	    	} 
		}
		FileUtil.fullyDelete(downloaded_folder);
		writer.close();
		
	}
	
	public static void computeJaccardDistanceInSingleMachine(String hdfs_graph_file, FileSystem hdfs, Configuration conf, FileSystem local,
			String binary_graph_file_hdfs, int no_vertices, int no_edges, double lambda, String degfileOut) throws Exception{
		////copy file from HDFS
		File f = new File(hdfs_graph_file);
		String downloaded_graph_file = f.getName();
		FileUtil.copy(hdfs, new Path(hdfs_graph_file), local, new Path(downloaded_graph_file), false, conf);
		
		////using downloaded file for single machine computation on Master node.
		JaccardInit singleAttractor = new JaccardInit(downloaded_graph_file, no_vertices, no_edges, lambda);
		singleAttractor.Execute();
		
		/**Indicating local file system instead of HDFS*/
		Configuration config1 = new Configuration();
		String hadoopLocalFS = "file:///";
		config1.set(FileSystem.FS_DEFAULT_NAME_KEY, hadoopLocalFS);
		
		String local_binary_graph_file = "local_binary_graph_file_" + downloaded_graph_file; 
		
		SequenceFile.Writer writer = SequenceFile.createWriter(config1,
				SequenceFile.Writer.file(new Path(local_binary_graph_file)),
				SequenceFile.Writer.keyClass(SpecialEdgeTypeWritable.class),
				SequenceFile.Writer.valueClass(NullWritable.class));
		
		HashMap<String, EdgeValue_v2> pEdges = singleAttractor.m_cGraph.GetAllEdges();
		for (Entry<String, EdgeValue_v2> iter : pEdges.entrySet()) {
			EdgeValue_v2 pEdgeValue = iter.getValue();

			String[] parts = iter.getKey().split("\\s+");
			int iBegin = Integer.parseInt(parts[0]);
			int iEnd = Integer.parseInt(parts[1]);
			SpecialEdgeTypeWritable spec = new SpecialEdgeTypeWritable();
			spec.init(Settings.EDGE_TYPE, iBegin, iEnd, pEdgeValue.distance, -1, null, -1, null);
			writer.append(spec, NullWritable.get());
		}
		writer.close();
		
		//Pushing binary initialized jaccard distance to HDFS
		//copy and delete src file from local to hdfs
		FileUtil.copy(local, new Path(local_binary_graph_file), hdfs, new Path(binary_graph_file_hdfs), true, conf);
		
		File f1 = new File(downloaded_graph_file);
		f1.delete();
		
		PrintWriter writer2 = new PrintWriter("degfile");
		HashMap<Integer, VertexValue_v2> mapVertices = singleAttractor.m_cGraph.m_dictVertices;
		for(Entry<Integer, VertexValue_v2> entry: mapVertices.entrySet()){
			int deg = entry.getValue().pNeighbours.size() - 1;
			writer2.println(entry.getKey() + " " + deg);
		}
		writer2.close();
		
		FileUtil.copy(local, new Path("degfile"), hdfs, new Path(degfileOut), true, conf);
		f1 = new File("degfile");
		f1.delete();
	}
	
	/**
	 * Converting a text file on HDFS to binary file in local machine. Then I need to push that converted file from local -> HDFS
	 * @param hdfs_graph_file
	 * @param hdfs
	 * @param conf
	 * @param local
	 * @param binary_graph_file_hdfs
	 * @throws IOException
	 */
	public static void convertOriginalGraphInSequenceFiles(String hdfs_graph_file, FileSystem hdfs, Configuration conf, FileSystem local,
															String binary_graph_file_hdfs) throws IOException{
		File f = new File(hdfs_graph_file);
		String downloaded_graph_file = f.getName();
		FileUtil.copy(hdfs, new Path(hdfs_graph_file), local, new Path(downloaded_graph_file), false, conf);
		
		BufferedReader reader = new BufferedReader(new FileReader(new File(downloaded_graph_file)));
		
		/**Indicating local file system instead of HDFS*/
		Configuration config1 = new Configuration();
		String hadoopLocalFS = "file:///";
		config1.set(FileSystem.FS_DEFAULT_NAME_KEY, hadoopLocalFS);
		
		String local_binary_graph_file = "local_binary_graph_file_" + downloaded_graph_file; 
		
		SequenceFile.Writer writer = SequenceFile.createWriter(config1,
				SequenceFile.Writer.file(new Path(local_binary_graph_file)),
				SequenceFile.Writer.keyClass(SpecialEdgeTypeWritable.class),
				SequenceFile.Writer.valueClass(NullWritable.class));
		
		String line = "";
		while((line = reader.readLine()) != null){
			String[] args = line.split("\\s+");
			int u = Integer.parseInt(args[0]);
			int v = Integer.parseInt(args[1]);
			SpecialEdgeTypeWritable spec = new SpecialEdgeTypeWritable();
			spec.init(Settings.EDGE_TYPE, u, v, 0, -1, null, -1, null);
			writer.append(spec, NullWritable.get());
			
		}
		writer.close();
		reader.close();
		//copy and delete src file from local to hdfs
		FileUtil.copy(local, new Path(local_binary_graph_file), hdfs, new Path(binary_graph_file_hdfs), true, conf);
		
		File f1 = new File(downloaded_graph_file);
		f1.delete();
	}
	
	
}
