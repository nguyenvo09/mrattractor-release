package com.mrattractor;
import java.io.BufferedReader;
import java.io.IOException;
import java.io.InputStreamReader;
import java.io.PrintWriter;
import java.io.StringWriter;
import java.net.InetAddress;
import java.util.ArrayList;
import java.util.Collections;
import java.util.HashMap;
import java.util.List;
import java.util.Map.Entry;

import junit.framework.Assert;

import org.apache.commons.logging.Log;
import org.apache.commons.logging.LogFactory;
import org.apache.hadoop.conf.Configuration;
import org.apache.hadoop.conf.Configured;
import org.apache.hadoop.fs.FileSystem;
import org.apache.hadoop.fs.Path;
import org.apache.hadoop.io.IntWritable;
import org.apache.hadoop.io.LongWritable;
import org.apache.hadoop.io.NullWritable;
import org.apache.hadoop.io.Text;
import org.apache.hadoop.mapred.Master;
import org.apache.hadoop.mapreduce.Job;
import org.apache.hadoop.mapreduce.MRJobConfig;
import org.apache.hadoop.mapreduce.Mapper;
import org.apache.hadoop.mapreduce.Reducer;
import org.apache.hadoop.mapreduce.lib.input.FileInputFormat;
import org.apache.hadoop.mapreduce.lib.input.SequenceFileInputFormat;
import org.apache.hadoop.mapreduce.lib.input.TextInputFormat;
import org.apache.hadoop.mapreduce.lib.output.FileOutputFormat;
import org.apache.hadoop.mapreduce.lib.output.MultipleOutputs;
import org.apache.hadoop.mapreduce.lib.output.SequenceFileOutputFormat;
import org.apache.hadoop.mapreduce.lib.output.TextOutputFormat;
import org.apache.hadoop.util.Tool;
import org.apache.hadoop.util.ToolRunner;
import org.apache.log4j.Logger;

import com.mrattractor.writable.NeighborWritable;
import com.mrattractor.writable.Settings;
import com.mrattractor.writable.SpecialEdgeTypeWritable;
import com.mrattractor.writable.StarGraphWithPartitionWritable;
import com.mrattractor.writable.StarGraphWritable;
import com.mrattractor.writable.TripleWritable;



/**
 * Generate star graphs from edges + pre-computed partitions.
 * @author ben
 * 
 */
public class LoopGenStarGraphWithPrePartitions extends Configured implements Tool {

	static String jobname = "Generate Star Graphs With PreComputed-Partitions.";

	public static Job globalJob;
		
	//public static Log logMap = LogFactory.getLog(LoopGenStarGraph.Map.class);
	public static Logger logMap2 = Logger.getLogger(LoopGenStarGraphWithPrePartitions.Map.class);
	public static Logger logReduce2 = Logger.getLogger(LoopGenStarGraphWithPrePartitions.Reduce.class);
//	public static Log logReduce = LogFactory.getLog(LoopGenStarGraph.Reduce.class);

	/**
	 * Input: Each line is an undirected unweighted edge (u,v)
	 * @author osboxes
	 *
	 */
	public static class Map extends Mapper<SpecialEdgeTypeWritable, NullWritable, IntWritable, SpecialEdgeTypeWritable> {
		
		HashMap<Integer, Integer> mapDeg;
		@Override
		protected void setup(Context context) throws IOException, InterruptedException {
			super.setup(context);

		}

		@Override
		protected void map(SpecialEdgeTypeWritable lineID, NullWritable line, Context context)
				throws IOException, InterruptedException {
			
			try {
				// example: u v
				//String vl = line.toString();
				//String[] args = vl.split("\\s+");
				//Assert.assertTrue("Input must be an edge OR list partition", args.length == 4 || args.length == 2);
				//assert args.length == 4: "Input must have 4 elements @LoopGenStarGraph.java";
				
				if(lineID.type == 'S'){
					IntWritable key = new IntWritable();
					int center = lineID.center;
					key.set(center);
					//String res = vl.substring(0 + args[0].length());
					//res = res.trim();
					context.write(key, lineID);
				}else if(lineID.type == 'G'){
					//int u = Integer.parseInt(args[0]);
					//int v = Integer.parseInt(args[1]);
					//String dis = args[2];
					//...
					//int degu = mapDeg.get(u); // Integer.parseInt(args[3]);
					//int degv = mapDeg.get(v); // Integer.parseInt(args[4]);
					//NodeWritable nodeU = new NodeWritable();
					IntWritable key = new IntWritable();
					SpecialEdgeTypeWritable value = new SpecialEdgeTypeWritable();

					key.set(lineID.center);
					value.init('G', lineID.center, lineID.target, lineID.weight, -1, null, -1, null);
					context.write(key, value);
					
					//=========================
					key.set(lineID.target);
					value.init('G', lineID.target, lineID.center, lineID.weight, -1, null, -1, null);
					context.write(key, value);
					if(MasterMR.DEBUG){
						//System.out.println("Distance of edge: " + lineID.target + " " + lineID.center +": " + lineID.weight);
					}
				}
				
			} catch (Exception e) {
				StringWriter sw = new StringWriter();
				PrintWriter pw = new PrintWriter(sw);
				e.printStackTrace(pw);
				logMap2.error(MasterMR.prefix_log + "" + sw.toString());
				e.printStackTrace();
				globalJob.killJob();
			}catch (AssertionError e){
				StringWriter sw = new StringWriter();
				PrintWriter pw = new PrintWriter(sw);
				e.printStackTrace(pw);
				logMap2.error(MasterMR.prefix_log + "" + sw.toString());
				e.printStackTrace();
				globalJob.killJob();
			}

		}
	}
	
//	static class CustomizedAdj implements Comparable<CustomizedAdj>{
//		int lab;
//		String dis;
//		public CustomizedAdj(int _adjV, String _dis) {
//			// TODO Auto-generated constructor stub
//			lab = _adjV;
//			dis = _dis;
//		}
//		@Override
//		public int compareTo(CustomizedAdj o) {
//			// TODO Auto-generated method stub
//			if (lab > o.lab) {
//				return 1;
//			}
//			if (lab == o.lab) {
//				return 0;
//			}
//			return -1;
//		}
//		@Override
//		public String toString() {
//			// TODO Auto-generated method stub
//			return lab + " " + dis;
//		}
//	}
	
	public static class Reduce extends Reducer<IntWritable, SpecialEdgeTypeWritable, StarGraphWithPartitionWritable, NullWritable> {

		MultipleOutputs<StarGraphWithPartitionWritable, NullWritable> mout;
		int p = 20;
		HashMap<Integer, Integer> mapDeg;

		@Override
		protected void setup(Context context)
				throws IOException, InterruptedException {
			mout = new MultipleOutputs<StarGraphWithPartitionWritable, NullWritable>(context);
			
			Configuration conf = context.getConfiguration();
			mapDeg = AttrUtils.readDegMap(conf, MasterMR.degreeFileKey);
			//logReduce2.warn(MasterMR.prefix_log + " " + "Logger: Reducing phase setup of Loop Generate Star Graphs");
//			logReduce2.info(MasterMR.prefix_log + " " + "Testing Info Logger: Reducing phase setup of Loop Generate Star Graphs");
//			logReduce2.error(MasterMR.prefix_log + " " + "Testing Error Logger: Reducing phase setup of Loop Generate Star Graphs");
//			logReduce2.fatal(MasterMR.prefix_log + " " + "Testing Fatal Logger: Reducing phase setup of Loop Generate Star Graphs");
//			logReduce2.trace(MasterMR.prefix_log + " " + "Testing Trace Logger: Reducing phase setup of Loop Generate Star Graphs");		
		}
		/**
		 * We want each star graph has sorted increasingly based on vertex's label 
		 *  => This helps decrease computational time for each reducer in @LoopDynamicInteractions. 
		 *  We sorted each star graph in each reducer. 
		 *  The number of star graphs we need to sort is exactly same to |V| (e.g., number of vertices of original graph.)
		 */
		@Override
		protected void reduce(IntWritable key, Iterable<SpecialEdgeTypeWritable> values, Context context) throws IOException, InterruptedException {

			try{
			
				//just emit to compute degree value:
				int center = key.get(); //@u @degu 
				int degcnt = 0;
				int countInfoPartitions = 0;
				//String full_partitions = "";
				ArrayList<NeighborWritable> neighbors = new ArrayList<NeighborWritable>();
				ArrayList<TripleWritable> triples = new ArrayList<TripleWritable>();
				while (values.iterator().hasNext()) {
					//String value = values.iterator().next().toString(); // v
					SpecialEdgeTypeWritable value = values.iterator().next();
					Assert.assertTrue(center == value.center);
					if(value.type == Settings.STAR_GRAPH){
						//partitions information
						//value.triplet_graphs
						countInfoPartitions += 1;
						for(TripleWritable trip: value.triplet_graphs){
							TripleWritable newTrip = new TripleWritable();
							newTrip.set(trip.left, trip.mid, trip.right);
							triples.add(newTrip);
						}
						//full_partitions = value; //e.g. 1 3 6;1-4-6;1-2-6;2-5-6;3-5-6;0-4-6;2-3-6;0-1-6;3-4-6;4-5-6;0-5-6;2-4-6;1-5-6;0-3-6;0-2-6;
					}else if(value.type == Settings.EDGE_TYPE){
						//String[] args = value.split("\\s+");
						//int adjV = Integer.parseInt(args[0]);
						//String dis = args[1];
						NeighborWritable neb = new NeighborWritable();
						neb.set(value.target, value.weight);
						//CustomizedAdj adj = new CustomizedAdj(adjV, dis);
						neighbors.add(neb);
						degcnt += 1;
					}
					
				}
				Assert.assertTrue(countInfoPartitions == 1);
				int degCenter = mapDeg.get(center);
				
				////////////////////////////////////////////////////////////////////////////////////
				/// THEOREM: The star graph with the number of neighbors < true_degree of a @center is unnecessary since
				/// the neighbors are only for exclusive interactions. We can ignore this star graph
				////////////////////////////////////////////////////////////////////////////////////
				
				if(degcnt < degCenter){
//					if(MasterMR.DEBUG){
//						//System.out.println(">>>Useless star graph, don't care about it<<<<");
//					}
					return;
				}
				/////////////////////////////////////////////////////////////////////////////////////
				/// END THEOREM
				/////////////////////////////////////////////////////////////////////////////////////
				
				/**
				 * We don't need this information ANYMORE. Because We are going to reduce edges.
				 * */
				if(MasterMR.CheckDegree){
					Assert.assertTrue(String.format("degcnt: %d, while key.degnode: %d", degcnt, degCenter), degcnt == degCenter);
				}
				Collections.sort(neighbors);
				
				StarGraphWithPartitionWritable star = new StarGraphWithPartitionWritable();
				star.set(key.get(), neighbors, triples);
				mout.write("graph", NullWritable.get(), star, "star/star");
	
			}catch(Exception e){
				e.printStackTrace();
				StringWriter sw = new StringWriter();
				PrintWriter pw = new PrintWriter(sw);
				e.printStackTrace(pw);
				e.printStackTrace();
				logReduce2.error(MasterMR.prefix_log + "" + sw.toString());
				globalJob.killJob();
			}catch(AssertionError e){
				StringWriter sw = new StringWriter();
				PrintWriter pw = new PrintWriter(sw);
				e.printStackTrace(pw);
				e.printStackTrace();
				logReduce2.error(MasterMR.prefix_log + "" + sw.toString());
				globalJob.killJob();
			}
			
		}

		@Override
		protected void cleanup(Context context)
				throws IOException, InterruptedException {
			super.cleanup(context);
			mout.close();
		}
	}

	@Override
	public int run(String[] args) throws Exception {
		// TODO Auto-generated method stub
//		System.out.println("GenTriangles");
		Job job = Job.getInstance(getConf());
		globalJob = job;
		job.setJarByClass(LoopGenStarGraphWithPrePartitions.class);
		
		job.setJobName(jobname);
		job.setMapOutputKeyClass(IntWritable.class);
		job.setMapOutputValueClass(SpecialEdgeTypeWritable.class);

		job.setOutputKeyClass(NullWritable.class);
		job.setOutputValueClass(StarGraphWithPartitionWritable.class);

		job.setMapperClass(Map.class);
		job.setReducerClass(Reduce.class);
		job.setNumReduceTasks(40);

		job.setInputFormatClass(SequenceFileInputFormat.class);
		job.setOutputFormatClass(SequenceFileOutputFormat.class);

		String[] input_files = args[0].split(",");
		String ouput_files = args[1];
		job.getConfiguration().set(MasterMR.degreeFileKey, args[2]);
		job.setJobName(jobname);
		FileSystem fs = FileSystem.get(job.getConfiguration());

		fs.delete(new Path(ouput_files), true);
		FileInputFormat.setInputPaths(job, new Path[] { new Path(input_files[0]), new Path(input_files[1]) });
		FileOutputFormat.setOutputPath(job, new Path(ouput_files));

		//MultipleOutputs.addNamedOutput(job, "graph", TextOutputFormat.class, Text.class, Text.class);
		MultipleOutputs.addNamedOutput(job, "graph", SequenceFileOutputFormat.class, NullWritable.class, StarGraphWithPartitionWritable.class);
		try {
			job.waitForCompletion(true);
		} catch (Exception e) {
			e.printStackTrace();
		}
		return 1;
	}

}
