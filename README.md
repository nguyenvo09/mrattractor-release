# README #



### What is this repository for? ###

* This is the implementation of MRAttractor, our paper accepted at IEEE International Conference on Big Data 2017, acceptance rate 18%. 
* Conference website: http://cci.drexel.edu/bigdata/bigdata2017/AcceptedPapers.html 
* Datasets are available in datasets folder
* MasterMR.java is the starting point of MRAttractor. 
* Authors: Nguyen Vo, Kyumin Lee
* Contact: nkvo@wpi.edu or vknguyen09@gmail.com

### How to use? ###

* To run MasterMR.java you should provide it arguments in following format:
```
<hdfs_graph_file> <hdfs_outfolder> -1 <lambda> -1 0 <memory_each_reducer> -1 <no_vertices> <window_size> <tau> <gamma> <dict_size_single_machine> <noReducersDynamic> <no_partitions> 0
```

## where 

* <hdfs_graph_file> is graph file that contains this list of edges. (e.g., <u> <v> ). Note that, <u> and <v> must be >= 1 and <= no_vertices. This file is stored in HDFS.
* <hdfs_outfolder> is the output folder in HDFS
* <lambda> is cohesive parameter in range [0-1]. You should use it 0.5 
* <memory_each_reducer> : memory in MB for each reducer 
* <no_vertices> : The number of vertices of graph
* <window_size> : Size of sliding window. If it is negative, we will not use sliding window. 
* <tau> : is threshold that will force an edge (u,v) to converge
* <gamma> : the number of left non-converged edges that we should run the rest of algorithm on single machine.
* <dict_size_single_machine> : this is the maximum size of hashmap to store virtual edges in single machine.
* <noReducersDynamic> : the number of reducers of computing dynamic interactions. Note that, By default, Hadoop system sets the number of reducers to 20.  
* <no_partitions> : The number of partitions we want to partition our graph .
* Constant value 0 and -1 should be preserved.

For example: 
Example 1: 
```
testgraphs/sample2.txt MrAttractor -1 0.5 -1 0 100 -1 3710 -20 0.7 5000 15000000 16453 5 5 0
```
Example 2:
```
testgraphs/karate.txt MrAttractor -1 0.6 -1 0 100 -1 34 20 0.7 5000 15000000 78 5 5 0
```
